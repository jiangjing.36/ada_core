# __all__ = [
#     'dependencies',
#     'components',
#     'configs',
#     'profiles',
#     'processing',
#     'sequences',
# ]

import sys
import os.path

sys.path.append(os.path.dirname(os.path.abspath(__file__)))

from ada_core.dependencies.general import Watcher
from dotenv import load_dotenv

envwatcher = Watcher(  # create a watcher instance to find .env files
    os.getcwd(),
    '.env',
)

# load any environment variables specified into namespace
for path in envwatcher:
    if path.endswith('.env'):
        load_dotenv(path)
