"""
These items are manipulated by the robotarm.
"""
import warnings
from ..dependencies.arm import robotarm
from ..dependencies.chemistry import UnitFloat
from ..dependencies.coordinates import Location, PierceArray, Rotation
from ..items.basic import Item, ReactionVessel, Container
from ..profiles.needles import gauges
from .. import assets


class ProbeItem(Item):
    def __init__(self,
                 location,
                 height=None,
                 interaction_depth=None,
                 total_length=None,
                 top_inner_diameter=None,
                 bot_inner_diameter=None,
                 tray_rotation=0.,
                 asset=None,
                 **kwargs,
                 ):
        """
        A generic class which may interact with a probe on the RobotArm. This class has defined values necessary for
        interaction with the probe.

        :param Location or dict location: location of the needle (usually this is provided from a Tray class instance)
        :param float height: height above the resting point (in the Tray) the top of the needle sits (mm)
        :param float interaction_depth: depth from the top of the needle that the probe should travel to pickup the
            needle (mm). This is typically the measure from the top to the bottom of the inside of the needle.
        :param float total_length: The total length of the needle from tip to top(mm).
        :param float top_inner_diameter: The inner diameter at the top of the item (mm). This is used for location
            determination for TipRemover instances.
        :param float bot_inner_diameter: The inner diameter of the item at the bottom of the interaction depth (mm).
            This is used for location determination for TipRemover instances. If this is not specified,
            `top_inner_diameter` will be used.
        :param kwargs: Item kwargs
        """
        self._height = None
        self._total_length = None
        self._interaction_depth = None
        self._top_id = None
        self._bot_id = None
        self.top_inner_diameter = top_inner_diameter
        self.bot_inner_diameter = bot_inner_diameter
        self.total_length = total_length  # true length of the item
        self.height = height
        self.interaction_depth = interaction_depth

        location = Location(location)
        # offset to bottom of the item from storage location
        location.offset(z=-(self.total_length - self.height))

        # store tray rotation and offset
        self._tray_rotation = Rotation(z=tray_rotation)
        self._tray_rotation.z += 90.  # offset by 90 (default orientation is along x)

        Item.__init__(
            self,
            location=location,
            asset=asset,
            **kwargs,
        )

        self.contaminated = False  # whether the item has been contaminated

    def __repr__(self):
        return f"{self.__class__.__name__}({self.total_length})"

    def __str__(self):
        return f"{self.__class__.__name__} {self.total_length} total {self.length} effective"

    @property
    def height(self):
        """Height above the resting point of the item in the tray to the top of the item"""
        return self._height

    @height.setter
    def height(self, value):
        if value is None:
            raise ValueError('The height value must not be None')
        self._height = UnitFloat(value, 'm', 'm', 'm')

    @height.deleter
    def height(self):
        self.height = 0.

    @property
    def total_length(self):
        """Total length of the item (from top to bottom)"""
        return self._total_length

    @total_length.setter
    def total_length(self, value):
        if value is None:
            raise ValueError('The total_length value must not be None')
        self._total_length = UnitFloat(value, 'm', 'm', 'm')

    @total_length.deleter
    def total_length(self):
        self.total_length = 0.

    @property
    def interaction_depth(self):
        """Interaction depth from the top of the item to the bottom of where the probe will sit when the item is on
        the probe"""
        return self._interaction_depth

    @interaction_depth.setter
    def interaction_depth(self, value):
        if value is None:
            raise ValueError('The interaction_depth value must not be None')
        self._interaction_depth = UnitFloat(value, 'm', 'm', 'm')

    @interaction_depth.deleter
    def interaction_depth(self):
        self.interaction_depth = 0.

    @property
    def length(self):
        """Length of the item when it is installed on the probe"""
        return self.total_length - self._interaction_depth

    @length.setter
    def length(self, value):
        pass  # catch for Item init

    @property
    def top_inner_diameter(self):
        """The inner diameter of the item at the top"""
        return self._top_id

    @top_inner_diameter.setter
    def top_inner_diameter(self, value):
        if value is None:
            raise ValueError('The top_inner_diameter value must be specified')
        self._top_id = UnitFloat(value, 'm', 'm', 'm')

    @property
    def bot_inner_diameter(self):
        """The inner diameter of the item at the bottom of the interaction depth"""
        return self._bot_id

    @bot_inner_diameter.setter
    def bot_inner_diameter(self, value):
        if value is None:
            value = self.top_inner_diameter
        self._bot_id = UnitFloat(value, 'm', 'm', 'm')

    @bot_inner_diameter.deleter
    def bot_inner_diameter(self):
        self.bot_inner_diameter = None

    @property
    def location_interaction(self):
        """interaction location for probe"""
        out = self.location_bottom
        out.z = out.z + self.length  # offset for length
        return out

    @location_interaction.setter
    def location_interaction(self, location):
        location = Location(location)
        location.z = location.z - self.length
        self.location_bottom = location

    @property
    def location_top(self):
        """location of the top of the needle"""
        out = self.location_bottom
        out.z = out.z + self.total_length
        return out

    @location_top.setter
    def location_top(self, location):
        location = Location(location)
        location.z = location.z - self.total_length
        self.location_bottom = location

    def retrieve_item(self, height=None):
        """
        Retrieves the item from its location with the RobotArm probe.

        :param float height: optional safeheight to use
        """
        if robotarm.modules.probe.item is not None:
            raise ValueError(
                f'There is already something ({str(robotarm.modules.probe.item)} attached to the end of the '
                f'dispenser. '
            )
        # ensure height is sufficient
        if height is None or height < self.location_top.z:
            height = self.location_top.z
        robotarm.safeheight(
            height,
            target='probe',
        )
        # record previous gripper position
        previous_gripper_position = robotarm.axes_location()['gripper']
        robotarm.move_to_location(  # go to the stored location of that tip
            self.location_interaction,
            target='probe',
            armbias='shoulder middle',
            aligngripper=self._tray_rotation.z,  # aligns to the same axis as the tray
            reference_axis='+x',  # by default, trays are aligned to the X axis
        )
        robotarm.modules.probe.item = self  # save the new tip in the holder variable
        self.item_asset.attach('arm')
        robotarm.safeheight(  # move up to clear
            height,
            target='probe',
        )
        # return gripper to old position
        robotarm.move_axes({'gripper': previous_gripper_position})


class GripperItem(Item):
    def __init__(self,
                 location,
                 total_height=None,
                 interaction_depth=None,
                 tray_rotation=0.,
                 asset=None,
                 **kwargs,
                 ):
        """
        Defines a generic item that can be manipulated by the RobotArm gripper. This class contains all requisite
        attributes required for gripper pickup and placing.

        :param Location or dict location: location of the bottom of the item
        :param float total_height: total height of the item (from bottom to top, mm)
        :param float interaction_depth: the distance down from the top of the item which the gripper should go for
            pickup actions.
        :param kwargs: Item keyword arguments
        """
        self._total_height = None
        self._interaction_depth = None
        self.total_height = total_height
        self.interaction_depth = interaction_depth

        # store tray rotation and offset
        self._tray_rotation = Rotation(z=tray_rotation)
        self._tray_rotation.z -= 90.  # offset by 90 (default orientation is along x)

        Item.__init__(
            self,
            location=location,
            asset=asset,
            **kwargs,
        )

    def __repr__(self):
        return f"{self.__class__.__name__}({self.total_height})"

    def __str__(self):
        return f"{self.__class__.__name__} {self.total_height} total {self.length} effective"

    @property
    def total_height(self):
        """Total length of the item (from top to bottom)"""
        return self._total_height

    @total_height.setter
    def total_height(self, value):
        if value is None:
            raise ValueError('The total_length value must not be None')
        self._total_height = UnitFloat(value, 'm', 'm', 'm')

    @total_height.deleter
    def total_height(self):
        self.total_height = 0.

    @property
    def interaction_depth(self):
        """Interaction depth from the top of the item to the bottom of where the bottom of the gripper will interact
        with the item"""
        return self._interaction_depth

    @interaction_depth.setter
    def interaction_depth(self, value):
        if value is None:
            raise ValueError('The interaction_depth value must not be None')
        self._interaction_depth = UnitFloat(value, 'm', 'm', 'm')

    @interaction_depth.deleter
    def interaction_depth(self):
        self.interaction_depth = 0.

    @property
    def length(self):
        """Length of the item when it is in the robotarm gripper"""
        return self.total_height - self._interaction_depth

    @length.setter
    def length(self, value):
        pass  # catch for Item init

    @property
    def location_interaction(self):
        """interaction location for gripper"""
        out = self.location_bottom
        out.z = out.z + self.length  # offset for length
        return out

    @location_interaction.setter
    def location_interaction(self, location):
        location = Location(location)
        location.z = location.z - self.length
        self.location_bottom = location

    @property
    def location_top(self):
        """location of the top of the item"""
        out = self.location_bottom
        out.z = out.z + self.total_height
        return out

    @location_top.setter
    def location_top(self, location):
        location = Location(location)
        location.z = location.z - self.total_height
        self.location_bottom = location

    def _check_in_grip(self):
        """checks whether this vial instance is in the robot arm gripper"""
        return robotarm.gripper.ingrip is self

    def pick_up(self, height=None):
        """
        Moves the robot arm to the item pickup location and grips it in the arm gripper.

        :param height: if specified, this will be the safe height that the sequence will use
        """
        if robotarm.gripper.ingrip is not None:
            raise ValueError(f'There is already something in the robot arm gripper: {robotarm.gripper.ingrip}')
        robotarm.move_to_location(  # move to pick up location
            self.location_interaction
        )
        robotarm.gripper_on(self)  # turn on gripper and store self

        self.item_asset.attach('gripper')

        robotarm.safeheight(height)  # move to safeheight

    def move_to_location(self, location, height=None):
        """
        Moves the item to the specified location.

        :param Location or dict location: xyz location
        :param height: safeheight to use
        """
        if self._check_in_grip() is False:
            raise ValueError(f'{str(self)} is not in the arm gripper (in grip: {str(robotarm.gripper.ingrip)})')

        # move to safeheight, then location
        robotarm.safeheight(height)
        robotarm.move_to_location(
            location,
        )

    def place(self, location, height=None):
        """
        Moves the item to the specified location and releases it from the gripper.

        :param location: position dictionary with x, y, and z components
        :param height: if specified, this will be the safe height that the sequence will use
        """
        self.move_to_location(  # move vial to location
            location,
            height=height,
        )
        robotarm.gripper_off()  # uninstall from gripper attribute and turn off gripper

        self.item_asset.detach()


class Needle(ProbeItem):
    def __init__(self,
                 location,
                 name=None,
                 gauge=None,
                 diameter=None,
                 height=16.7,  # default height from resting point to top of needle
                 interaction_depth=9.52,  # default depth from top of needle to bottom of inside
                 total_length=None,
                 pierce_length=None,

                 top_inner_diameter=4.35,
                 bot_inner_diameter=3.9,
                 top_outer_diameter=5.75,

                 tray_rotation=0.,
                 cap=True,
                 asset=None,

                 # depreciated kwargs
                 length=None,
                 bd_length=None,
                 tipdiameter=5.,

                 **kwargs,
                 ):
        """
        A class defining a needle (most commonly a disposable BD needle)

        :param Location or dict location: location of the needle (usually this is provided from a Tray class instance)
        :param str name: convenience name
        :param float gauge: may be specified instead of diameter (retrieves diameter from a predefined dictionary)
            The outer diameter of the needle is used for calculating pierce arrays. The diameter may be specified by one
            of either diameter or gauge.
        :param float diameter: outer diameter of the needle (mm)
        :param float height: height above the resting point (in the Tray) the top of the needle sits (mm)
        :param float interaction_depth: depth from the top of the needle that the probe should travel to pickup the
            needle (mm). This is typically the measure from the top to the bottom of the inside of the needle.
        :param float total_length: The total length of the needle from tip to top(mm).
        :param float pierce_length:  the usable length of the needle (mm; from tip to the end of the metal area)

        :param float top_inner_diameter: The inner diameter at the top of the item (mm). This is used for location
            determination for TipRemover instances.
        :param float bot_inner_diameter: The inner diameter of the item at the bottom of the interaction depth (mm).
            This is used for location determination for TipRemover instances. If this is not specified,
            `top_inner_diameter` will be used.
        :param float top_outer_diameter: The outer diameter at the top of the item (mm). This is used for location
            determination for TipRemover instances.

        :param Rotation, float tray_rotation: Rotation of the parent tray. This will be used to align the gripper to
            that axis so as to avoid collisions with the tray on retrieval.
            If specified, the needle length is used to retrieve the true length from a predefined dictionary.
        :param bool cap: Whether or not the safety cap is still on the needle

        :param float length: length of the needle from the end of the dispenser probe to the tip of the needle (mm)
        :param float bd_length: BD needle length (mm; optional instead of length)

        In the Needle class, the core location is the resting point in the Tray.
        """
        # set outer diameter of needle (used for
        if 'probe' not in robotarm.modules:
            raise ValueError('There is no dispenser installed in the RobotArm instance. Use of a needle requires a '
                             'DispenserProbe instance to be installed as a RobotArm module. ')

        # catch for depreciated kwargs
        if bd_length is not None:  # convert to manually measured lengths
            warnings.warn('The bd_length and length kwargs have been depreciated, '
                          'set true_length and pierce_length instead', DeprecationWarning)
            if total_length is None:
                if bd_length == 40.:  # 40 mm BD needles
                    total_length = 56.
                elif bd_length == 50.:  # 50 mm BD needles
                    total_length = 68.4
                else:
                    raise ValueError(f'The bd_length {bd_length} is not profiled. Please profile the total_length, '
                                     f'height, and the interaction_depth parameters for this new needle. ')

        # set pierce length of the needle
        self._pierce_length = None
        self.pierce_length = pierce_length or total_length - height

        # set outer diameter of the needle
        self._od = None
        self.od = diameter or gauges[gauge]

        # build a dynamic needle asset based on the generic needle
        asset_info = asset or assets.needles.build_needle(
            sleeve_height=height,  # height of the interaction area
            diameter=float(top_inner_diameter) or 0,  # diameter of the interaction area
            needle_length=self.pierce_length or 0,  # pierce length of the needle
            tipdiameter=self.od,  # diameter of the tip
        )
        
        # set parameters for decapping
        self.cap = cap
        self.top_outer_diameter = top_outer_diameter

        # initialize ProbeItem base instance
        ProbeItem.__init__(
            self,
            location=location,
            name=name,
            height=height,
            interaction_depth=interaction_depth,
            total_length=total_length,
            top_inner_diameter=top_inner_diameter,
            bot_inner_diameter=bot_inner_diameter,
            asset=asset_info,
            tray_rotation=tray_rotation,
            **kwargs,
        )

    def __repr__(self):
        return f"{self.__class__.__name__}({self.od} od, {self.length} length)"

    def __str__(self):
        return f"{self.od} od, {self.length} length needle"

    @property
    def pierce_length(self):
        return self._pierce_length

    @pierce_length.setter
    def pierce_length(self, value):
        if value is None:
            self._pierce_length = None
        else:
            self._pierce_length = UnitFloat(value, 'm', 'm', 'm')

    @pierce_length.deleter
    def pierce_length(self):
        self.pierce_length = None

    @property
    def od(self):
        """outer diameter of the needle"""
        return self._od

    @od.setter
    def od(self, value):
        self._od = UnitFloat(value or 0., 'm', 'm', 'm')

    @od.deleter
    def od(self):
        self.od = 0.

    def retrieve_tip(self, height=None):
        """
        Retrieves the needle  and installs it on the end of the RobotArm probe.
        This method exists for legacy support. Use retrieve_item instead.

        :param float height: optional safeheight to use
        """
        self.retrieve_item(height)


class PipetteTip(ProbeItem, Container):
    def __init__(self,
                 location,
                 height=None,
                 interaction_depth=None,
                 total_length=None,
                 maximum_volume=None,
                 top_inner_diameter=7.17,
                 bot_inner_diameter=7.12,
                 tray_rotation=0.,
                 asset=None,

                 # depreciated kwargs
                 length=None,

                 **kwargs,
                 ):
        """
        Defines a pipette tip

        :param Location or dict location: location of the tip (usually retrieved from a tray object)
        :param float length: length of the tip (mm)
            This is measured from the end of the tip to the bottom of the dispenser probe on the robot arm when the pipette
            tip is installed properly on the probe.
        :param tray_rotation: Rotation of the parent tray. This will be used to align the gripper to that axis so as to
            avoid collisions with the tray on retrieval.
        :param float height: height from the resting point (in the Tray) to the top of the pipette tip (mm)
        :param float interaction_depth: depth from the top of the pipette tip that the probe should travel to pickup the
            pipette tip (mm).
        :param float total_length: The total length of the pipette tip from tip to top(mm).
        :param float top_inner_diameter: The inner diameter at the top of the item (mm). This is used for location
            determination for TipRemover instances.
        :param float bot_inner_diameter: The inner diameter of the item at the bottom of the interaction depth (mm).
            This is used for location determination for TipRemover instances. If this is not specified,
            `top_inner_diameter` will be used.
        :param float maximum_volume: volume that can be contained by the tip (mL)
        """
        if 'probe' not in robotarm.modules:
            raise ValueError('There is no probe installed in the RobotArm instance. Use of a pipette tip requires a '
                             'DispenserProbe instance to be installed as a RobotArm module. ')

        if length is not None:
            warnings.warn('The length keyword argument of PipetteTip has been depreciated, set total_length, '
                          'height, and interaction_depth instead', DeprecationWarning)
            total_length = total_length or length

        # build a dynamic asset
        asset_info = asset or assets.pipette_tips.build_pipette_tip(
            length=total_length,
            diameter=top_inner_diameter,
            interaction_height=interaction_depth,
        )

        ProbeItem.__init__(
            self,
            location=location,
            height=height,
            interaction_depth=interaction_depth,
            total_length=total_length,
            top_inner_diameter=top_inner_diameter,
            bot_inner_diameter=bot_inner_diameter,
            asset=asset_info,
            tray_rotation=tray_rotation,
            **kwargs
        )
        Container.__init__(
            self,
            maximum_volume=maximum_volume,
            **kwargs
        )

    def __repr__(self):
        return f"{self.__class__.__name__}({self.maximum_volume})"

    def __str__(self):
        return f"{self.maximum_volume} mL {self.total_length} pipette tip"

    def retrieve_tip(self, height=None):
        """
        Retrieves the pipette tip and installs it on the end of the RobotArm probe.
        This method exists for legacy support. Use retrieve_item instead.

        :param height: optional safeheight to use
        """
        self.retrieve_item(height)


class Vial(ReactionVessel, Container, GripperItem):
    def __init__(self,
                 *args,
                 location=None,
                 name=None,
                 total_height=None,
                 capped=True,
                 capheight=0.,
                 piercediameter=0.,
                 piercedepth=0.,
                 screwpitch=None,
                 rotations=None,
                 reaction=False,
                 capdiameter=None,
                 # crimp=False,
                 diameter=20,
                 # screw=True,
                 tray_rotation=0.,
                 asset=None,

                 # depreciated kwargs
                 height=None,

                 **kwargs,
                 ):
        """
        Defines a screw-capped vial

        :param Location or dict location: location dictionary of the base of the vial
        :param name: name for the vial (convenience)
        :param capped: whether the vial is capped (bool)
        :param capdiameter: diameter of the cap (mm)
        :param diameter: diameter of the vial (mm)
        :param total_height: height of the vial from the bottom to the top of the cap (mm)
        :param capheight: height of the cap (mm)
        :param piercediameter: diameter of the piercable area of the cap (mm)
        :param screwpitch: thread pitch of the vial (mm)
        :param rotations: number of rotations required to remove the cap
        :param piercedepth: how far from the bottom of a vial should a needle go to (mm)
        :param reaction: whether this Vial contains a reaction
        """
        self.capheight = capheight
        self._trueheight = height

        # build a dynamic asset based on the generic vial
        asset_info = asset or assets.vials.build_vial(
            diameter=diameter, height=height, capdiameter=capdiameter, capheight=capheight
        )

        if reaction is True:  # if there is to be a reaction in the vial, initialize ReactionVessel
            ReactionVessel.__init__(
                self,
                *args,
                length=height - capheight,
                **kwargs
            )
        else:  # otherwise this is a basic LocationContainer
            Container.__init__(
                self,
                *args,
                **kwargs,
            )

        # initialize gripper item
        GripperItem.__init__(
            self,
            location=location,
            total_height=total_height or height,
            interaction_depth=capheight,
            asset=asset_info,
            tray_rotation=tray_rotation,
            **kwargs,
        )
        self.name = name
        self.capped = capped
        # self.capdiameter = capdiameter
        # self.crimp = crimp
        # self.screw = screw
        # self.diameter = diameter
        self.piercedepth = piercedepth
        self.piercediameter = piercediameter
        self.screwpitch = screwpitch
        self.rotations = rotations

        self.cappedloc = None
        self.uncappedloc = None
        self.pa = None

    def __repr__(self):
        return f"{self.__class__.__name__}({self.name})"

    def __str__(self):
        return f"{self.__class__.__name__} {self.name}"

    @property
    def location_pierce(self):
        """pierce location for the vial (offset vertically from the base of the vial by the piercedepth parameter"""
        if self.pa is None:  # if array has not been generated, generate array
            self.generate_piercearray()
        ploc = self.pa.getloc()  # get pierce location relative to center of vial
        out = self.location_bottom  # get bottom location
        if is_installed('probe', Needle) is True:  # if a needle is installed:
            if robotarm.modules.probe.item.pierce_length is not None:  # and the pierce_length is defined
                length = robotarm.modules.probe.item.pierce_length
                # if pierce depth is deeper than the reach of the needle
                if length < self.location_top.z - out.z - self.piercedepth:
                    out = self.location_top  # get the top location
                    out.offset(ploc)  # offset the xy location
                    out.offset(z=-length)  # go down exactly as far as possible
                    return out

        out.offset(ploc)  # apply pierce location offset
        out.offset(z=self.piercedepth)
        return out

    @location_pierce.setter
    def location_pierce(self, location):
        location = Location(location)
        location.offset(z=-self.piercedepth)
        self.location_bottom = location

    def uncap(self):
        """
        Uncaps a screw capped vial. The function assumes that the vial is held securely against an uncapping motion.
        """
        if self._check_in_grip() is False:
            raise ValueError(f'{str(self)} is not in the gripper, call pickup first')
        self.cappedloc = robotarm.axes_location()
        # The symmetry is locked before move_axes due to a inversion of axes
        if robotarm._gripper.lock_symmetry is False:  # if the gripper symmetry is not locked, temporarily lock it
            robotarm._gripper.temp_lock = True  # enable temporary lock
        robotarm.move_axes(
            {
                'vert': robotarm._vert.mm_to_counts_absolute(
                    self.screwpitch * self.rotations
                ) + self.cappedloc['vert'],
                'gripper': self.cappedloc['gripper'] - robotarm._gripper.cpr * self.rotations
            },
            splitxyz='all',
        )
        if robotarm._gripper.temp_lock is True:  # if temporarily locked, unlock
            robotarm._gripper.temp_lock = False
        self.uncappedloc = robotarm.axes_location()
        del robotarm.modules.gripper.item  # remove item from gripper
        robotarm.gripper.ingrip = 'cap'  # specify cap is in grip

    def recap(self):
        """
        Recaps a screw capped vial. The function assumes that the vial is held securely and the gripper must have a
        'cap' ingrip.
        """
        if robotarm.gripper.ingrip != 'cap':
            raise ValueError('There is no cap in the gripper')
        robotarm.move_axes(  # move to uncapped location
            self.uncappedloc,
        )
        robotarm.move_axes(  # recap movement
            self.cappedloc,
            splitxyz=None,
        )
        robotarm.modules.gripper.item = self
        robotarm.gripper.ingrip = self

    def generate_piercearray(self, needle=None, od=0., **kwargs):
        """
        Generates an array of pierce locations for a piercable vial cap.

        :param Needle needle: a needle instance
        :param float od: outer diameter of the needle (default 0.)
        :param kwargs: keyword arguments for a PierceArray instance
        """
        if self.piercediameter is None:
            raise ValueError(f'The piercediameter attribute is not defined for this {self.__class__.__name__} instance.')
        if is_installed('probe', Needle) is True:  # if a needle is installed on the robotarm
            needle = robotarm.modules['probe'].item
        self.pa = PierceArray(
            needle=needle,
            piercediameter=self.piercediameter,
            od=od,
            **kwargs
        )

    def get_pierce_location(self, **kwargs):
        """
        Retrieves a location for needle piercing from the PierceArray instance. If a pierce array has not already been
        generated, a needle or PierceArray keyword arguments may be handed to this function to automatically generate
        a pierce array.

        :param kwargs: keyword arguments for a PierceArray instance
        :return: pierce location dictionary
        :rtype: dict
        """
        warnings.warn('The get_pierce_location method has been depreciated. Generate a pierce array using '
                      'generate_piercearray and access location_pierce directly', DeprecationWarning)
        if self.pa is None:  # if array has not been generated, generate array
            self.generate_piercearray(**kwargs)
        return self.location_pierce

    def get_location(self, loc='bottom'):
        """
        Returns either the top, bottom, or a pierce location of the vial.

        :param 'bottom', 'top', 'pickup', or 'pierce' loc: the location to retireve
        :return: location
        :rtype: Location
        """
        warnings.warn('The get_location method has been depreciated. Access location_bottom, _top, _interaction, '
                      'and _pierce directly', DeprecationWarning)
        if loc.lower() == 'bottom':
            return self.location_bottom
        elif loc.lower() == 'top':
            return self.location_top
        elif loc.lower() == 'pierce':
            return self.location_pierce
        elif loc.lower() == 'pickup':
            return self.location_interaction
        else:
            raise KeyError(f'The loc value "{loc}" is not recognized')


# todo make item checks look for item instance instead of an update_location attribute
def is_installed(module, cls):
    """
    Checks to see if class instance is installed on the robotarm module specified

    :param str, ArmModule module: module name or RobotArm module instance
    :param cls: class type
    :return: bool
    :rtype: bool
    """
    if type(module) == str:  # catch for string specification
        module = robotarm.modules[module]
    if module.item is None:  # if there is nothing installed
        return False
    return isinstance(module.item, cls)  # otherwise check type