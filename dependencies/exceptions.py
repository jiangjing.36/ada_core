class CriticalVolumeException(Exception):
    def __init__(self, msg=None):
        """
        Indicates volume is below the critical volume specified
        :param msg:
        """
        """custom exception to indicate that this tray is empty"""
        if msg is None:
            msg = 'The contained volume is below the critical volume specified. '
        super(CriticalVolumeException, self).__init__(msg)


class UnreachablePosition(Exception):
    def __init__(self, position, name=None, limits=None):
        """
        An error raised when a given position is unreachable by an actuator

        :param int, dict, Location position: position that was targeted
        :param list of int limits: range limits
        :param string name: name of the axis or object (recommended for error clarity)
        """
        if type(position) == dict:
            string = f'The position {", ".join(f"{key}: {position[key]:.1f}" for key in sorted(position))} ' \
                     f'is unreachable'
        else:
            string = f'The position {position} is unreachable'
        if name is not None:
            string += f' by {name}'
        if limits is not None:
            string += f'. The acceptable range is {limits[0]:g} - {limits[1]:g}.'
        super(UnreachablePosition, self).__init__(string)


class PositionMismatch(Exception):
    def __init__(self, name=None, expected=None, actual=None):
        """
        An error raised when the target position is different than the true position (this is used primarily in the
        mopytracker functionality of the RobotArm class).

        :param name: name of the
        :param expected: expected position
        :param actual: actual position
        """
        string = f'A position mismatch was encountered'
        if name is not None:
            string += f' for {name},'
        if expected is not None:
            string += f' expected: {expected}'
        if actual is not None:
            string += f' actual: {actual}'
        string += '. '
        super(PositionMismatch, self).__init__(string)


class ZeroPositionError(Exception):
    def __init__(self, *MOPYSET):
        """
        An error raised when a position change of zero is handed in a MOPY trajectory set. Such a case will cause
        unexpected behaviour in the controller.
        """
        super(ZeroPositionError, self).__init__(f'A position delta of zero was specified in the trajectory set\n'
                                                f'{MOPYSET}\n'
                                                f'positions: {MOPYSET[4::3]}')


class VelocityLimit(Exception):
    def __init__(self, velocity, maximum_velocity, name=None):
        """
        An error raised when a specified velocity exceeds that allowable by an actuator

        :param int velocity: specified velocity
        :param int maximum_velocity: velocity limit
        :param string name: name of the actuator or object (recommended for error clarity)
        """
        string = f'The specified velocity {velocity} exceeds the allowable limit {maximum_velocity}'
        if name is not None:
            string += f' for {name}'
        string += '.'
        super(VelocityLimit, self).__init__(string)


class AccelerationLimit(Exception):
    def __init__(self, acceleration, maximum_acceleration, name=None):
        """
        An error raised when a specified acceleration exceeds that allowable by an actuator

        :param int acceleration: specified velocity
        :param int maximum_acceleration: velocity limit
        :param string name: name of the actuator or object (recommended for error clarity)
        """
        string = f'The specified acceleration {acceleration} exceeds the allowable limit {maximum_acceleration}'
        if name is not None:
            string += f' for {name}'
        string += '.'
        super(AccelerationLimit, self).__init__(string)


class TrayEmpty(Exception):
    def __init__(self):
        """custom exception to indicate that this tray is empty"""
        super(TrayEmpty, self).__init__("The tray does not contain any more items.")


class TrayFull(Exception):
    def __init__(self):
        """custom exception to indicate that this tray is empty"""
        super(TrayFull, self).__init__("The tray is full.")


class EmptyLocation(Exception):
    def __init__(self, location=None):
        """custom exception to indicate that this tray is empty"""
        super(EmptyLocation, self).__init__(f"The location {location} of the tray is empty.")


class MaxPierces(Exception):
    def __init__(self):
        """custom exception to indicate that this tray is empty"""
        super(MaxPierces, self).__init__(
            "The maximum number of pierces for this septum has been reached")


class InvalidCommand(Exception):
    def __init__(self, command):
        super(InvalidCommand, self).__init__(f"The specified command ('{command}') is not recognized.")
