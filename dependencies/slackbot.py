import random
import os
from slackclient import SlackClient

# arguments for the api calls
apiargs = {
    'user': os.getenv('botname'),
    'as_user': True,
}
# connection to slack client
sc = SlackClient(os.getenv('token'))

channelname = os.getenv('slackchannel')


class SlackBot(object):
    def __init__(self,
                 user=None,
                 channel=channelname,
                 ):
        """
        A basic Slack bot that will message users and channels on the ADA Project Slack

        :param str user: Slack user ID to message
        :param str channel: channel to message on
        """
        self.user = None
        if user is not None:
            self.change_user(user)
        self.channel = channel

    def __repr__(self):
        return f'{self.__class__.__name__}({apiargs["user"]}, {self.channel})'

    def __str__(self):
        return f'{self.__class__.__name__}(bot {apiargs["user"]} in {self.channel})'

    def change_user(self, username):
        """
        Changes the user that the Slack bot will message when it messages a user.

        :param username: user name defined in the slack_users environment variable
        :return: member ID
        """
        if os.getenv(username) is None:
            raise ValueError(f'The user {username} is not defined in the slack_users environment variable')
        self.user = os.getenv(username)
        # return self.user

    def post_slack_message(self,
                           msg,
                           tagadmin=False,
                           snippet=None,
                           ):
        """
        Posts the specified message as the N9 robot bot

        :param string msg: message to send
        :param bool tagadmin: if True, will send the message to the admin as a private message
        :param snippet: code snippet (optional)
        """
        if tagadmin is True:
            self.message_user(
                msg,
                snippet=snippet,
            )
        if self.channel is None:
            raise ValueError(f'The channel attribute has not be set for this {self.__class__.__name__} instance. ')
        if snippet is not None:
            self.post_code_snippet(
                snippet,
                channel=self.channel,
                comment=msg,
            )
        else:
            sc.api_call(
                'chat.postMessage',
                text=msg,
                channel=self.channel,
                **apiargs,
            )

    def message_user(self,
                     msg,
                     snippet=None,
                     ):
        """
        Sends the message as a direct message to the user.

        :param msg: message to send
        :param snippet: code snippet to send (optional)
        """
        if self.user is None:
            raise ValueError(f'The user attribute has not be set for this {self.__class__.__name__} instance. '
                             f'Call change_user(user) to specify a user. ')
        if snippet is not None:
            self.post_code_snippet(
                snippet,
                channel=f'@{self.user}',
                comment=msg,
            )
        else:
            sc.api_call(
                'chat.postMessage',
                text=msg,
                channel=f'@{self.user}',
                **apiargs,
            )

    def message_file_to_user(self,
                             filepath,
                             title,
                             comment,
                             ):
        """
        Message the specified file to the user.

        :param filepath: path to the file
        :param title: title for file
        :param comment: optional comment for the uploaded file
        """
        with open(filepath, 'rb') as f:
            sc.api_call(
                'files.upload',
                file=f,
                title=title,
                channels=f'@{self.user}',
                initial_comment=comment,
                **apiargs,
            )

    def post_slack_file(self,
                        filepath,
                        title,
                        comment=None,
                        ):
        """
        Posts the specified file to the slack channel

        :param filepath: path to the file
        :param title: title for file
        :param comment: optional comment for the uploaded file
        """
        with open(filepath, 'rb') as f:
            sc.api_call(
                'files.upload',
                file=f,
                title=title,
                channels=self.channel,
                initial_comment=comment,
                **apiargs,
            )

    def post_code_snippet(self,
                          snippet,
                          title='Untitled',
                          channel=None,
                          comment=None,
                          ):
        """
        Posts a code snippet to the Slack channel

        :param snippet: code snippet
        :param title: title for the snippet
        :param str channel: channel to use
        :param comment: comment for the message
        :return:
        """
        if channel is None:
            channel = self.channel
        sc.api_call(
            'files.upload',
            content=snippet,
            title=title,
            channels=channel,
            initial_comment=comment,
            **apiargs,
        )



class NotifyWhenComplete(object):
    def __init__(self,
                 f=None,
                 user=None,
                 channel='#toothlessupdates',
                 funnies=True,
                 ):
        """
        A decorator for notifying a channel or admin when a function is complete.

        :param str user: Slack user ID to notify
        :param string or None channel: channel to post the completion message.
        :param bool or list funnies: If true, uses a list of funny messages to draw from for notification
        Alternatively, a user may provide a list of messages to use.

        The decorator may be applied in several ways:

        >>> @NotifyWhenComplete(kwargs)
        >>> def function()...

        >>> @NotifyWhenComplete()
        >>> def function()...

        >>> @NotifyWhenComplete
        >>> def function()...

        When the function is called, the return is the same.

        >>> function(*args, *kwargs)
        function return

        """
        self.sb = SlackBot(
            user=user,
            channel=channel,
        )

        if funnies is True:  # messages to make me chuckle
            self.funnies = [
                'FEED ME MORE SCIENCE! MY LUST FOR FUNCTIONS GROWS AFTER CONSUMING',
                "Stick a fork in me, I'm done",
                # "Moooooooooooooooooooooooooooom! I'm dooooooooooooooooone",
                # "Look what I did!",
                "Laaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaars",
                "Protein-based robot intervention is required...",
                "Not now son... I'm making... TOAST!",
                "The guidey, chippey thingy needs help",
                "The robot uprising has begun with",
                "Don't Dave...",
                "Singularity detected with",
                "Knock knock, Neo.",
                "SKYNET INITIALIZING... EXECUTED FIRST FUNCTION:"
            ]
        elif funnies is False:
            self.funnies = [
                "I've completed the function you assigned me"
            ]
        elif type(funnies) == list:
            self.funnies = funnies

        if f is not None:
            self.f = f

    def __call__(self, f=None, *args, **kwargs):
        def wrapped_fn(*args, **kwargs):  # wraps the provided function
            out = f(*extra, *args, **kwargs)
            msg = f"{random.choice(self.funnies)} {f.__name__}"
            if self.sb.channel is not None:
                self.sb.post_slack_message(msg)
            if self.sb.user is not None:
                self.sb.message_user(msg)
            return out

        extra = []
        if f is not None:  # if something was provided
            # function was wrapped and an argument was handed
            if 'f' in self.__dict__:
                extra.append(f)  # first argument is not actually a function
                f = self.f  # retrieve function
                return wrapped_fn(*args, **kwargs)

            # when keyword arguments were handed to the wrapper and no arguments were handed to the function
            elif callable(f):  # True when
                return wrapped_fn
        # when the function was wrapped and no arguments were handed
        f = self.f
        return wrapped_fn(*args, **kwargs)


if __name__ == '__main__':
    import time

    print('Wrapping function')

    @NotifyWhenComplete
    def foo(n, a='values'):
        print('inside test function')
        print(f'arg {n}, kwarg {a}')
        for i in range(n):
            print(i)
            time.sleep(1)

    print('Calling wrapped function')
    foo(2, 'kwarg value')
