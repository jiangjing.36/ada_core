"""
The valid set of commands recognized by the North Robotics NR9 robot

New commands may be added to this file in call_key: CMD,
with the appropriate command prefix being defined here. 

CMD: four letter command to send to the robot

Copy, paste, and edit the following to add new commands
    'KEY': 'CMND',

"""
commands = {
    'com': 'SECO',  # send to comport
    'readcom': 'RECO',  # read from comport
    'echo': 'ECHO',  # echo the robot
    'flagcheck': 'FLAG',  # checks whether axes are moving
    'go': 'GOAX',  # execute buffered movement commands
    'home': 'HOME',  # home the robot
    'keyboard': 'KEYB',  # enable keyboard driving mode
    'move': 'MOPY',  # assign a trajectory set to an axis
    'output': 'OUTP',  # set the specified output to 1 or 0
    'wait': 'WAIT',  # wait for V1 seconds
    # 'ttl': 'TTLC',  # send a bit command to a TTL object
    'tomttlvalve': 'VALV',  # send a commmand to a TTL-controlled valve (only for Tom's direct inject setup)
    'line': 'GOTO',  # go to line number V1

    # need to add
    'stop_axes': '',  # TODO get Allan to write this to firmware

    # unused
    'zero': 'ZERO',  # zeros axes to the current values

    # for troubleshooting only
    'servo_on': 'SEON',  # turns the servos on and sets values to zero (for troubleshooting)
    'servo_off': 'SEOF',  # turns the servos off (for troubleshooting)

    # depreciated
    'moveaxis': 'MOAX',  # move a single axis (depreciated)
    'movesync': 'MOSY',  # syncronous movement of 2 axes (depreciated)
    'position': 'POSR',  # returns the position of all axes (depreciated)
    'plustwo': 'AN02',  # returns the value plus two (depreciated)
    'random': 'AN01',  # returns a random number
    'spin': 'MOID',  # spins the gripper until toggled off (depreciated)
}
