import importlib.util
import os
import warnings
import numpy as np
from copy import deepcopy
import pylab as pl
from matplotlib.collections import CircleCollection, LineCollection, AsteriskPolygonCollection
from ..components.joints import RevoluteJoint, PrismaticJoint
from ..dependencies.exceptions import UnreachablePosition, PositionMismatch
from ..dependencies.simulator import simulator, Asset
from ..components.pneumatic import PneumaticHolder
from ..dependencies.interface import controller
from ..dependencies.coordinates import Location, Rotation
from ..dependencies.motion import trajectory_duration, match_trajectory, mopy_position, pull_dirflag


def forward_kinematics(gamma, alpha, vert, module=None, upper=170.25, lower=170.25, alpha_mod=0., voffset=0.,
                       x_offset=0., y_offset=0.):
    """
    Converts joint angles and distances to a location of a module.

    :param float gamma: shoulder angle from zero (radians)
    :param float alpha: elbow angle from zero (radians)
    :param float vert: vertical distance from deck of gripper reference (mm)
    :param module: ArmModule instance (keyword arguments will be pulled from this)
    :param float upper: upper arm length (mm)
    :param float lower: lower arm length (mm)
    :param float alpha_mod: elbow angle modifer
    :param float voffset: vertical offset of target from gripper reference (mm)
    :param float x_offset: x offset from effective to true location (mm)
    :param float y_offset: y offset from effective to true location (mm)
    :return: location of the point
    :rtype: dependencies.coordinates.Location
    """
    # if handed joint instances, retrieve values
    if isinstance(gamma, RevoluteJoint):
        gamma = gamma.radians
    if isinstance(alpha, RevoluteJoint):
        alpha = alpha.radians
    if isinstance(vert, PrismaticJoint):
        vert = vert.location

    # if handed an ArmModule, retrieve values (overrides keyword arguments)
    if module is not None and isinstance(module, ArmModule):
        upper = module.upper
        lower = module.lower
        alpha_mod = module.alpha_mod
        voffset = module.voffset

    return Location(
        x=(
                upper * np.sin(gamma)
                + lower * np.sin(gamma + alpha + alpha_mod)
                - x_offset
        ),
        y=(
                upper * np.cos(gamma)
                + lower * np.cos(gamma + alpha + alpha_mod)
                - y_offset
        ),
        z=vert + voffset
    )


def inverse_kinematics(
        targetlocation,
        module=None,
        upper=170.25,
        lower=170.25,
        alpha_mod=0.,
        voffset=0.,
        x_offset=0.,
        y_offset=0.,
):
    """
    Calculates the joint angles required to move a robot arm to.

    :param dict or Location targetlocation: Target xyz location
    :param ArmModule module: ArmModule instance (the remaining keyword arguments will be pulled from this if provided)
    :param float upper: upper arm length (mm)
    :param float lower: lower arm length (mm)
    :param float alpha_mod: elbow angle modifer
    :param float voffset: vertical offset of target from gripper reference (mm)
    :param float x_offset: x offset from effective to true location (mm)
    :param float y_offset: y offset from effective to true location (mm)
    :return: dictionary of angles and lengths calculated to get to the target location
    :rtype: dict

    The frame of reference for the coordinate system is from the perspective of the shoulder, looking out onto the bed.
    The y axis is towards the far edge of the bed (increasing towards the far edge of the bed from the shoulder),
    and the x axis is perpendicular to this.

    y axis
    ^
    |
    ----------------------------------------
    |                                       |
    |    -+         bed           ++        |
    |                                       |
    |                                       |
    |                                       |
    |             shoulder                  |
    |             ___o____                  |
    |            |        |                 | o: origin
    |    --      |        |       +-        |
    +----------------------------------------------> x-axis

    """
    if module is not None and isinstance(module, ArmModule):
        upper = module.upper
        lower = module.lower
        alpha_mod = module.alpha_mod
        voffset = module.voffset

    # convert to Location instance if not handed one
    if isinstance(targetlocation, Location) is False:
        targetlocation = Location(targetlocation)
    # if handed a targetlocation instance, create copy to avoid mutation
    else:
        targetlocation = deepcopy(targetlocation)

    # apply deck offsets
    targetlocation.x += x_offset
    targetlocation.y += y_offset

    # convert values to float to prevent unexpected behaviour with custom types
    x = float(targetlocation.x)
    y = float(targetlocation.y)
    z = float(targetlocation.z)

    hypotenuse = np.sqrt(x ** 2 + y ** 2)
    if hypotenuse > (upper + lower):
        raise UnreachablePosition(
            dict(targetlocation),
            name='the robot arm',
        )
    vert = z - voffset  # update vertical

    alpha = np.math.acos(  # shoulder-elbow-gripper angle
        (
                x ** 2 + y ** 2 - upper ** 2 - lower ** 2
        ) / (
                -2 * upper * lower
        )
    ) - alpha_mod

    alphaprime = np.pi - alpha

    # atan2 increases counterclockwise from the x=+ plane
    # to get the angle from the y-axis to the target point that increases the CW direction,
    # the target must be rotated 90 CW, then mirrored along the x axis
    epsilon = np.math.atan2(x, y)

    gamma = np.math.acos(  # elbow-shoulder-gripper angle
        (
                upper ** 2 + hypotenuse ** 2 - (lower ** 2)
        ) / (
                2 * upper * hypotenuse
        )
    )
    # gammaprimecw = atan2 - np.pi / 2 - gamma  # CW angle
    # gammaprimeccw = atan2 - np.pi / 2 + gamma  # CCW angle
    gammaprimecw = epsilon + gamma
    gammaprimeccw = epsilon - gamma

    return {
        'vert': vert,
        'alpha': alpha,
        'alphaprime': alphaprime,
        'hypotenuse': hypotenuse,
        'epsilon': epsilon,
        'gamma': gamma,
        'gammaprimecw': gammaprimecw,
        'gammaprimeccw': gammaprimeccw,
        'deltacw': -alpha + gammaprimecw,
        'deltaccw': alpha + gammaprimeccw,
    }


def inverse_jacobian(gammaprime, alphaprime, upper, lower, vx, vy):
    """
    Uses the inverse jacobian to determine the appropriate joint velocities to achieve the specified x and y velocity.

    :param float gammaprime: angle between the y axis and the upper arm (radians)
    :param float alphaprime: angle between the upper and lower arm (outer, radians)
    :param float upper: length of the upper arm (mm)
    :param float lower: length of the lower arm (mm)
    :param float vx: x velocity (mm/second)
    :param float vy: y velocity (mm/second)
    :return: shoulder and elbow velocities
    :rtype: tuple
    """
    gammaprime = np.pi/2 - gammaprime
    both = gammaprime + alphaprime
    return np.dot(
        [
            [
                lower * np.cos(both),
                lower * np.sin(both)
            ],
            [
                - upper * np.cos(gammaprime) - lower * np.cos(both),
                - upper * np.sin(gammaprime) - lower * np.sin(both)
            ]
        ],
        [vx, vy]
    ) / (lower * upper * np.sin(alphaprime))


class ArmModule(object):
    def __init__(self,
                 sh_el=170.25,
                 el_point=170.25,
                 hoffset=0.,
                 base_voffset=0.,
                 length=20.,
                 asset=None,
                 **kwargs
                 ):
        """
        Defines a module installed on the robot arm.

        :param float sh_el: shoulder elbow distance (mm, it is not recommended to change this value)
        :param float el_point: distance from the elbow to the point of interest (mm)
        :param float hoffset: horizontal offset (mm).
            The horizontal offset should be positive on the clockwise side of the arm (user's left) and negative on the
            counterclockwise side of the arm (user's right)

        :param float base_voffset: The vertical offset distance from the top of the module anchor point (e.g. the probe
            anchor point) to the installation point of the gripper attachment (mm).

        :param float length: vertical length of the module (mm; relative to the bottom of the gripper teeth)
            The vertical offset is relative to point at which the gripper teeth are attached to the gripper assembly.
            Negative values are closer to the robot deck than the teeth, and positive values are higher than the teeth.
            A convenient way of calculating this difference is to use the keyboard driving mode to touch the new module
            to a flat surface, then remove the module and drive the gripper teeth to the same flat surface. The
            difference between the two z values, plus 20 mm, will be the height.

        :param dict asset: an optional dict used to create an a simulator asset (see AssetInfo for parameters)
        """
        # todo refactor to link lengths in robotarm and have link-extension attributes for the modules
        self.upper = sh_el
        self._lower = el_point
        self.lower_offset = 0.
        self._hoffset = 0.
        self.hoffset = hoffset
        self.base_voffset = base_voffset
        self.length = length

        self.alpha_mod = 0.

        self._lastlocation = {}  # todo implement lastlocation tracking and retrieval
        self._location = Location()

        self._item = None

        self.module_asset = Asset(self._location, asset, visible=False)

    def __repr__(self):
        return f'{self.__class__.__name__}({self.upper}, {self.lower}, {self.hoffset}, {self.item})'

    def __str__(self):
        return f'{self.__class__.__name__} upper {self.upper} mm, lower {self.lower} mm, ' \
               f'horizontal offset {self.hoffset} mm, installed {self.item}'

    @property
    def lower(self):
        return self._lower + self.lower_offset

    @property
    def hoffset(self):
        return self._hoffset

    @hoffset.setter
    def hoffset(self, offset):
        """
        Calculates the necessary parameters for a horizontally offset arm module.

        :param offset: offset (mm)
        """
        self._hoffset = offset
        self.alpha_mod = np.tan(  # calculate new alpha modifier
            offset / self._lower
        )
        self.lower_offset = np.sqrt(  # calculate new lower length
             self._lower ** 2 + offset ** 2
        ) - self._lower

    @property
    def location(self):
        """Retrieves either the item (if present) or the module location."""
        if self.item is not None:  # if there is an item installed, return its location
            return self.item.get_location('bottom')
        else:
            return self.module_location   # otherwise, return the module location

    @property
    def item_location(self):
        """Allows retrieval of the item location but not setting or deleting"""
        if self.item is None:
            raise AttributeError('There is no item installed in the module. ')
        return self.item.get_location('bottom')

    @property
    def module_location(self):
        """Allows retrieval of the module location but not setting or deleting"""
        return self._location

    @property
    def item(self):
        return self._item

    @item.setter
    def item(self, item):
        if item is None:  # catch for setting item to None
            del self.item
            return

        if self.item is not None:
            raise ValueError(f'There is already an item installed on this module: {self.item}')
        if hasattr(item, 'location_interaction'):  # if it has a location interaction point
            item.location_interaction = self.location
        elif hasattr(item, 'update_location'):
            item.update_location(self.location)  # update the item's location using the module location
            item.offset_location(z=-item.length)  # offset by the length
        self._item = item  # store item

    @item.deleter
    def item(self):
        if self.item is None:
            raise ValueError(f'There is no item installed in the {self.__class__.__name__} instance')
        self._item = None

    @property
    def voffset(self):
        return (
            self.base_voffset  # base vertical offset from gripper anchor point
            - self.length  # length of the module
            - (self.item.length if self.item is not None else 0.)  # installed item length
        )

    def remove_item(self):
        """
        Legacy method for retrieval of the removed item.

        :return: removed item
        """
        out = self.item  # retrieve current item
        del self.item  # delete item attribute
        return out  # return item

    def update_location(self, location):
        """
        Updates the location of the module, taking into account the item installed on the tip.

        :param dict or Location location: dictionary x,y,z location of module
        """
        # if there is an item on the dispenser, adjust z value of location accordingly
        if self.item is not None:
            self.item.update_location(location)
            self._location.update(location)  # update the module location
            self._location.offset(z=self.item.length)  # offset for the length of the item
        else:  # if there is no item, just update location
            self._location.update(
                location,
            )


class DispenserProbe(ArmModule):
    def __init__(self,
                 probe_base_offset=28.,
                 **kwargs
                 ):
        """
        Describes a dispenser probe for attaching bd needles or pipette tips to

        :param probe_base_offset: the distance from the top of the dispenser probe threads to the installation point of
            the gripper attachment (mm).
        :param float base_voffset: The vertical offset distance from the top of the module anchor point (e.g. the probe
            anchor point) to the installation point of the gripper attachment (mm).

        :param kwargs: ArmModule keyword arguments
        """
        warnings.warn('The DispenserProbe class has been completely incorporated into ArmModule. Create an ArmModule '
                      'class instance using base_voffset=probe_base_offset', DeprecationWarning)
        ArmModule.__init__(
            self,
            base_voffset=probe_base_offset,
            **kwargs
        )


class ArmModuleDaemon(object):
    def __init__(self, **kwargs):
        """
        A class for managing installed ArmModules instances

        :param kwargs: ArmModule keyword arguments for the gripper
        """
        self._instances = {}

    def __repr__(self):
        return f'{self.__class__.__name__}({", ".join(str(x) for x in self.installed)})'

    def __str__(self):
        return f'{self.__class__.__name__} module manager with {", ".join(str(x) for x in self.installed)} installed. '

    def __getitem__(self, item):
        """:rtype: ArmModule"""
        if self.__contains__(item):
            return self._instances[item]
        else:
            raise KeyError(f'The specified module {item} is not installed in the {repr(self)} instance. ')

    def __getattr__(self, item):
        if self.__contains__(item):
            return self._instances[item]
        elif item in self.__dict__:
            return self.__dict__[item]
        else:
            raise AttributeError(f"'{self.__class__.__name__}' object has no attribute '{item}'")

    def __iter__(self):
        for item in self._instances:
            yield self._instances[item]

    def __contains__(self, item):
        return item in self._instances

    @property
    def installed(self):
        """Legacy support for checking what modules are installed"""
        return list(self._instances.keys())

    def install_module(
            self,
            name,
            module=None,
            base_voffset=0.,
            **kwargs
    ):
        """
        Installs a module in the daemon

        :param str name: name for the module
        :param ArmModule module: an ArmModule instance
        :param kwargs: keyword arguments for the ArmModule instance
        """
        if name in self._instances:
            raise KeyError(f'The name {name} is already installed in the {self.__class__.__name__} instance. ')
        if module is not None:
            if isinstance(module, ArmModule) is False:
                raise TypeError(f'A non-ArmModule instance was provided as the module keyword argument. ')
            self._instances[name] = module
            return

        if 'module_type' in kwargs and kwargs['module_type'] == 'DispenserProbe':
            if base_voffset == 0.:
                base_voffset = 28.  # catch for legacy support of dispenser modules

        self._instances[name] = ArmModule(
            # legacy catch for DispenserProbe instances
            base_voffset=kwargs['probe_base_offset'] if 'probe_base_offset' in kwargs else base_voffset,
            **kwargs
        )


class RobotArm(object):
    def __init__(self,
                 shoulder,
                 elbow,
                 gripper,
                 z,
                 shax=2,
                 elax=1,
                 grax=0,
                 grop=0,
                 vertax=3,
                 modules=None,
                 velocity=20000,
                 acceleration=150000,
                 safeheight=280.,
                 armbias='min shoulder change',
                 x_offset=0.,
                 y_offset=0.,
                 stepmove=False,
                 mopyinspect=False,
                 simulate=False,
                 blocking=True,
                 ):
        """
        Defines a North Robotics arm and its motions

        :param RevoluteJoint or dict shoulder: a RevoluteJoint instance or keyword arguments required to create one for
            the shoulder joint
        :param RevoluteJoint or dict elbow: a RevoluteJoint instance or keyword arguments required to create one for
            the elbow joint
        :param RevoluteJoint or dict gripper: a RevoluteJoint instance or keyword arguments required to create one for
            the gripper joint
        :param PrismaticJoint or dict z: a PrismaticJoint instance or keyword arguments required to create one for
            the z joint
        :param int shax: the axis number for the shoulder
        :param int elax: the axis number for the elbow
        :param int grax: the axis number for the gripper
        :param int grop: the output number for the gripper
        :param int vertax: the axis number for the z axis
        :param bool stepmove: enables a mode where user input is required before continuing after a movement
        :param False or str mopyinspect: allows for mopy position command inspection
        """
        # create joint instances
        if isinstance(shoulder, RevoluteJoint):
            self._shoulder = shoulder
        elif type(shoulder) == dict:
            self._shoulder = RevoluteJoint(**shoulder)
        else:
            raise TypeError('A RevoluteJoint instance or a dictionary of keyword arguments for creating a RevoluteJoint'
                            ' instance are required for the shoulder argument.')

        if isinstance(elbow, RevoluteJoint):
            self._elbow = elbow
        elif type(elbow) == dict:
            self._elbow = RevoluteJoint(**elbow)
        else:
            raise TypeError('A RevoluteJoint instance or a dictionary of keyword arguments for creating a RevoluteJoint'
                            ' instance are required for the elbow argument.')

        if isinstance(gripper, RevoluteJoint):
            self._gripper = gripper
        elif type(gripper) == dict:
            self._gripper = RevoluteJoint(**gripper)
        else:
            raise TypeError('A RevoluteJoint instance or a dictionary of keyword arguments for creating a RevoluteJoint'
                            ' instance are required for the gripper argument.')

        if isinstance(z, PrismaticJoint):
            self._vert = z
        elif type(z) == dict:
            self._vert = PrismaticJoint(**z)
        else:
            raise TypeError('A PrismaticJoint instance or a dictionary of keyword arguments for creating a '
                            'PrismaticJoint instance are required for the z argument.')

        # store axes numbers
        self._shax = shax
        self._elax = elax
        self._grax = grax
        self._vertax = vertax
        self._velocity = None
        self._acceleration = None
        self.velocity = velocity
        self.acceleration = acceleration
        self._x_offset = x_offset
        self._y_offset = y_offset
        self.safe_height = safeheight
        self.armbias = armbias
        self.stepmove = stepmove
        self.mopyinspect = mopyinspect
        self.simulate = simulate
        self.blocking = blocking

        self.modules = ArmModuleDaemon()
        if modules is not None:  # if additional modules are specified
            for mod in modules:
                self.modules.install_module(**mod)

        self.ik = {}  # storage dictionary for inverse kinematic solutions

        # initialize gripper instance
        self.gripper = PneumaticHolder(output=grop)

        # todo figure out a better way to do these associations
        self._ax_assoc = {
            'elbow': self._elax,
            'shoulder': self._shax,
            'vert': self._vertax,
            'gripper': self._grax,
        }

        # setup a RobotSimulator connection
        self.robot_simulator = simulator

        self.home()
        if self.mopyinspect is not False:  # enable mopy tracker if specified
            self.mopytracker = None
            self._enable_mopytracker(self.mopytracker)

    def __repr__(self):
        return f'{self.__class__.__name__}({self._shoulder.position}, {self._elbow.position}, ' \
               f'{self._vert.position}, {self._gripper.position})'

    def __str__(self):
        return f'{self.__class__.__name__} shoulder: {self._shoulder.position}, elbow: {self._elbow.position}, ' \
               f'vertical: {self._vert.position}, gripper: {self._gripper.position}'

    def _calculate_gripper_alignment(self, angle, reference='+y', delta=None):
        """
        Calculates the gripper counts to go to in order to align to the specified axis. By default the provided value will
        be calculated relative to the +y axis.

        :param angle: angle to align the gripper to with respect to the reference.
            Valid angle inputs are '+x', '-x', '+y', '-y' and a float value in degrees
            The angle will be interpreted as a value in degrees relative to the reference axis.
        :param reference: reference line for the alignment. Valid alignments are '+x', '-x', '+y', '-y' 'forearm', 'origin'
        :param delta: the angle (in degrees) between the forearm and the y-axis.
            If this is not specified, the angle will be calculated from the current joint positions
        :return: counts for the gripper to go to
        :rtype: int
        """

        # if not specified, calculate the angle between the forearm and the y axis
        # delta acts as a correcting factor to account for the rotation of the arm with respect to the y axis
        if delta is None:
            delta = -(
                    self._shoulder.degrees
                    + self._elbow.degrees
            )

        if angle == 'origin' and reference == '+y':  # legacy API catch
            warnings.warn("Change aligngripper='origin' calls to orient_gripper=0., reference_axis='origin'",
                          DeprecationWarning)
            self._gripper.temp_lock = True
            angle = 0.
            reference = 'origin'

        # converting the angle to a rotation about the z axis with respect to y
        target = Rotation(z=angle)

        # determining the reference axis based on the input reference
        if reference == 'forearm':
            reference_axis = Rotation(z=0.)
            delta = 0.
        elif reference == 'origin':
            reference_axis = Rotation(z=-np.degrees(self.ik['epsilon']))
        else:
            reference_axis = Rotation(reference)

        # if the reference axis isn't 'y' then adjust the target based on the orientation axis.
        # if the input 'angle' is an an integer or float angle then this doesn't need to be done.
        if reference in ['-y', 'x', '+x', '-x'] and type(angle) is str:
            # offset relative to the reference
            target.offset(
                -reference_axis.x,
                -reference_axis.y,
                -reference_axis.z,
            )

        # Calculating the rotation angle of the arm from the y axis to reach the desired position.
        rotation_angle = delta + target.z + reference_axis.z

        return self._gripper.degrees_to_counts_closest(rotation_angle)

    @property
    def lock_gripper_symmetry(self):
        """Exposed low level gripper symmetry lock state. Set this to True to forcibly prevent the symmetry of the gripper
        from affecting movement commands. """
        return self._gripper.lock_symmetry

    @lock_gripper_symmetry.setter
    def lock_gripper_symmetry(self, state):
        self._gripper.lock_symmetry = state

    def orient_gripper(self, orientation, reference_axis='+y'):
        """
        Calculates the gripper counts to go to in order to orient the front of the gripper to the specified axis.

        For example:

        >>> RobotArm.orient_gripper(orientation='-x')

        This call will rotate the gripper so that the front of the gripper will point in the -x direction.

        :param orientation: selected orientation
            Valid orientations are '+x', '-x', '+y', '-y' or a float value, which will be
            interpreted as a value in degrees relative to the reference axis.
            If the reference_axis is 'forearm' or 'origin' use a float value
        :param reference_axis:  reference line for the orientation.
            Valid inputs are '+x', '-x', '+y', '-y' 'forearm', 'origin'
        """
        self._gripper.temp_lock = True  # temporarily lock the gripper for correct calculation
        counts = self._calculate_gripper_alignment(
            angle=orientation,
            reference=reference_axis,
        )
        mopy = self._gripper.change_position_locked_symmetry(counts)
        self._gripper.temp_lock = False
        if len(mopy) == 0:  # catch for no movement
            return None
        if self.mopyinspect == 'gripper':
            self._track_mopy(mopy)
        controller.execute(
            'move',
            self._grax,
            *mopy,
        )
        controller.start_axes(self._grax)
        controller.zzz(trajectory_duration(mopy))
        controller.wait_on_axes(self._grax)

    def align_gripper(self, alignment, reference_axis='+y'):
        """
        Calculates the gripper counts to go to in order to align to the specified axis

        :param alignment: selected alignment
            Valid orientations are '+x', '-x', '+y', '-y' or a float value, which will be
            interpreted as a value in degrees relative to the reference axis.
            If the reference_axis is 'forearm' or 'origin' use a float value
        :param reference_axis: reference line for the alignment.
            Valid inputs are '+x', '-x', '+y', '-y' 'forearm', 'origin'
        """
        if alignment == '-x':
            alignment = 'x'
        elif alignment == '-y':
            alignment = 'y'
        counts = self._calculate_gripper_alignment(angle=alignment, reference=reference_axis)
        mopy = self._gripper.change_position(counts)
        if len(mopy) == 0:  # catch for no movement
            return None
        if self.mopyinspect == 'gripper':
            self._track_mopy(mopy)
        controller.execute(
            'move',
            self._grax,
            *mopy,
        )
        controller.start_axes(self._grax)
        self.move_simulator_gripper(trajectory_duration(mopy))
        controller.zzz(trajectory_duration(mopy))
        controller.wait_on_axes(self._grax)

    def axes_location(self):
        """Returns the current location of the Joint instances"""
        return {
            'shoulder': self._shoulder.position,
            'elbow': self._elbow.position,
            'vert': self._vert.position,
            'gripper': self._gripper.position,
        }

    @property
    def velocity(self):
        """
        The set maximum velocity of the joints, which should be the maximum value set. The individual velocities of the
        encoders will be less or equal to this value.
        """
        return self._velocity

    @velocity.setter
    def velocity(self, v):
        self._gripper.encoder_v = v
        self._shoulder.encoder_v = v
        self._elbow.encoder_v = v
        self._vert.encoder_v = v
        self._velocity = v

    def change_velocity(self, v):
        """
        Changes the velocity of the joint actuators of the robot arm

        :param int v: velocity (counts/second)
        """
        warnings.warn('The change_velocity method has been depreciated. Modify the velocity attribute directly to '
                      'change the default velocity.')
        self.velocity = v

    @property
    def acceleration(self):
        """
        The set maximum acceleration of the joints, which should be the maximum value set. The individual accelerations
        of the encoders will be less or equal to this value.
        """
        return self._acceleration

    @acceleration.setter
    def acceleration(self, a):
        self._gripper.encoder_a = a
        self._shoulder.encoder_a = a
        self._elbow.encoder_a = a
        self._vert.encoder_a = a
        self._acceleration = a

    def change_acceleration(self, a):
        """
        Changes the acceleration of the joint actuators of the robot arm

        :param a: acceleration (counts/second^2)
        """
        warnings.warn('The change_acceleration method has been depreciated. Modify the acceleration attribute directly '
                      'to change the default acceleration.')
        self.acceleration = a

    def calculate_spacefill(self):
        """
        Calculates the spacefilling description of the robot arm
        :return:
        """
        raise NotImplementedError('Not written yet.')
        # TODO figure out how to make the robot itself a dead zone and define no-fly zones

    def _enable_mopytracker(self, joint):
        """
        Enables mopy tracking (debug mode) for the specified axis

        :param joint: joint name
        """
        if joint not in self._ax_assoc:
            raise KeyError(
                f'The mopyinspect keyword value must be one of {", ".join(str(x) for x in self._ax_assoc.keys())}')
        self.mopyinspect = joint
        self.mopytracker = vars(self)[f'_{joint}'].position  # get current mopytracker position
        if controller.verbose is True:
            print(f'Enabling mopytracker for joint {joint}, current position: {self.mopytracker}')

    def invert_elbow(self, target='gripper'):
        """
        Inverts the orientation of the robot elbow (provided that the inversion is within range)

        :param string target: target ArmModule key to invert about(defined in the modules dictionary of the instance)
        """
        # todo fix it so that this won't break with other arm biases
        options = ['elbow left', 'elbow right']
        options.remove(self.armbias)
        self.armbias = options[0]
        self.move_to_location(
            self.module_location(target),
            target=target,
        )

    def home(self):
        """Homes the robot and updates the module locations"""
        controller.execute('home')
        self._elbow.home()
        self._shoulder.home()
        self._gripper.home()
        self._vert.home()
        self.update_module_locations()
        if self.modules.gripper.item is not None:  # reset gripper and gripper item tracker
            self.gripper_off()
        if self.mopyinspect is not False:
            self.mopytracker = vars(self)[f'_{self.mopyinspect}']._initial
        self.move_axes({'gripper': 0})
        if not self.simulate:
            self.sync_simulator()

    def fast_home(self, approx_home_count=100):
        """

        :param approx_home_count:   The count buffer to leave in case of drift.
                                    Longer runs without homing may require larger buffers.
        :return:
        """
        self.move_axes({'shoulder': approx_home_count, 'elbow': approx_home_count, 'vert': approx_home_count, 'gripper': 0})
        self.home()

    def gripper_on(self, item=None):
        """
        Turns the robot arm gripper on and installs the provided item into the necessary module trackers.

        :param item: incoming item instance to go into the gripper
        """
        self.modules.gripper.item = item  # install item in module
        self.gripper.on(item)  # turn on the pneumatic gripper
        self.close_simulator_gripper()

    def gripper_off(self):
        """
        Turns the robot arm gripper off and removes installed item from the necessary module trackers.

        :return: item previously in grip
        """
        self.gripper.off()  # turn off the pneumatic gripper
        self.open_simulator_gripper()
        out = self.modules.gripper.item
        del self.modules.gripper.item  # remove the item from the module
        return out  # return the removed item

    def install_arm_module(self, name, module=None, module_type='ArmModule', **kwargs):
        """
        Installs an arm module in the arm instance. The modules are used to track the location of items installed on
        the arm.

        :param string name: unique name for the module
        :param module: optional supply of a preinitialized ArmModule instance
        :param module_type: ArmModule or DispenserProbe
        :param kwargs: keyword arguments for initializing an ArmModule instance (see ArmModule for details)
        """
        self.modules.install_module(
            name,
            module=module,
            module_type=module_type,
            **kwargs,
        )

    def jerk(self, distance=0.5):
        """
        Jerks the z axis (useful for jerking droplets out of tubes)

        :param float distance: distance to jerk (mm)
        """
        controller.execute('move', 3, 1, 45000, 0, int(distance * self._vert.cpmm))
        controller.start_axes(3)
        controller.wait_on_axes(3)
        controller.execute('move', 3, 0, 45000, 0, int(distance * self._vert.cpmm))
        controller.start_axes(3)
        controller.wait_on_axes(3)

    def keyboard(self, target='gripper'):
        """
        Keyboard driving mode

        :param string target: the target name on the arm to move (only applies to cartesian movements)

        Commands:

        Move in units of mm:

        - w  y-axis    -        away from user
        - s  y-axis    +        towards user
        - a  x-axis    +        user's left
        - d  x-axis    -        user's right
        - r  z-axis    up/-     (axis 3)
        - f  z-axis    down/+   (axis 3)

        Move in units of counts:

        - y  shoulder  CCW/-    (axis 2)
        - u  shoulder  CW/+     (axis 2)
        - h  elbow     CCW/+    (axis 1)
        - j  elbow     CW/-     (axis 1)
        - n  z-axis    up/-     (axis 3)
        - m  z-axis    down/+   (axis 3)
        - q  gripper   CW/-     (axis 0)
        - e  gripper   CCW/+    (axis 0)

        Increments:

        - z  10.0  cm   1000 counts
        - x   1.0  cm    100 counts
        - c   1.0  mm     10 counts
        - v   0.1  mm      5 counts
        - b   0.01 mm      1 count

        Special:

        - i or invert will invert the elbow about the current target

        Targets:

        gripper plus any other ArmModule keys defined in the modules dictionary

        'exit' or '/'  exit driving mode

        """
        print('Initializing keyboard driving mode, input "exit" to exit this mode')
        increments = {  # predefined increments
            'z': {  # biggest jump
                'm': 100.,  # 100 mm
                'c': 1000,  # 1000 counts
            },
            'x': {
                'm': 10.,  # 10 mm
                'c': 100,  # 100 counts
            },
            'c': {
                'm': 1.,  # 1 mm
                'c': 10,  # 10 counts
            },
            'v': {
                'm': 0.1,  # 0.1 mm
                'c': 5,  # 5 counts
            },
            'b': {
                'm': 0.01,  # 0.01 mm
                'c': 1,  # 1 count
            }
        }
        assoc = {  # axis association, direction, and increment type
            'w': ['y', -1., 'm'],  # 1 will give positive movement, -1 will give negative
            's': ['y', 1., 'm'],
            'a': ['x', 1., 'm'],
            'd': ['x', -1., 'm'],
            'r': ['z', 1., 'm'],
            'f': ['z', -1., 'm'],
            'y': ['shoulder', -1., 'c'],
            'u': ['shoulder', 1., 'c'],
            'h': ['elbow', 1., 'c'],
            'j': ['elbow', -1., 'c'],
            'n': ['vert', -1., 'c'],
            'm': ['vert', 1., 'c'],
            'q': ['gripper', -1., 'c'],
            'e': ['gripper', 1., 'c'],
        }

        # build valid keys list
        valid = ['/', 'exit', 'help', 'h', 'i', 'invert']
        valid.extend(increments.keys())
        valid.extend(assoc.keys())
        valid.extend(self.modules.installed)

        inp = ''  # initial input value
        incr = 'x'  # default to x increment
        while inp not in ['exit', '/']:
            inp = ''  # reset input
            while inp not in valid:  # wait for valid command
                inp = input('Input: ').lower()  # get the user's input
                if inp not in valid:
                    print(f"Invalid input '{inp}', type 'help' for valid commands")

            if inp == 'help':  # if the user asks for help
                print(self.keyboard.__doc__)

            elif inp in ['exit', '/']:  # exit command
                # continue
                pass

            elif inp in ['i', 'invert']:  # elbow inversion
                self.invert_elbow(target)

            elif inp in increments.keys():  # change increment
                incr = inp
                print(f'Changed increment to {increments[incr]["m"]:.1f} mm and {increments[incr]["c"]} counts')
                # continue

            elif inp in self.modules:  # change cartesian target
                target = inp
                print(f'Changed cartesian target to {target}')

            else:  # movement command
                if assoc[inp][0] in self._ax_assoc.keys():
                    if assoc[inp][0] == 'shoulder':
                        loc = shoulder.position
                    elif assoc[inp][0] == 'elbow':
                        loc = elbow.position
                    elif assoc[inp][0] == 'gripper':
                        loc = gripper.position
                    elif assoc[inp][0] == 'vert':
                        loc = z.position
                    self.move_axes(
                        {
                            assoc[inp][0]: (
                                    loc
                                    + assoc[inp][1]
                                    * increments[incr][assoc[inp][2]]
                            )
                        },
                    )
                elif assoc[inp][0] in ['x', 'y', 'z']:
                    self.move_to_location(
                        {
                            assoc[inp][0]: (
                                    self.module_location(target)[assoc[inp][0]]
                                    # self.modules[target].location()[assoc[inp][0]]
                                    + assoc[inp][1]
                                    * increments[incr][assoc[inp][2]]
                            )
                        },
                        target=target,
                    )
                self.location_print()

    def location_print(self, *module):
        """
        Prints the location of the specified modules. If none are specified, prints all locations.
        format.

        :param module: module name defined in the modules dictionary
        :return: location
        :rtype: dict
        """
        if len(module) == 0:
            module = self.modules.installed

        print('Modules:')
        for mod in module:  # print details of each module
            loc = self.module_location(mod)
            print(f'{mod} \t{", ".join(f"{key}: {loc[key]}" for key in loc)}')
        print(f'\nAxes counts\t'  # print axes counts
              f'shoulder: {self._shoulder.position}, '
              f'elbow: {self._elbow.position}, '
              f'z-axis: {self._vert.position}, '
              f'gripper: {self._gripper.position}'
              )

    def module_location(self, module='gripper'):
        """Returns the cartesian location of the specified ArmModule """
        if module not in self.modules:
            raise KeyError(f'The module {module} is not installed in the arm instance')
        return self.modules[module].location

    def move_to_relative_location(self,
                                  location,
                                  v=None,
                                  a=None,
                                  splitxyz='xyz',
                                  target='gripper',
                                  armbias=None,
                                  aligngripper=None,
                                  orient_gripper=None,
                                  reference_axis='+y',
                                  ):
        current_location = Location(self.modules[target].module_location)
        new_location = Location(location.offset(current_location))
        self.move_to_location(
            new_location,
            v=v,
            a=a,
            splitxyz=splitxyz,
            target=target,
            armbias=armbias,
            aligngripper=aligngripper,
            orient_gripper=orient_gripper,
            reference_axis=reference_axis
        )

    def move_to_location(self,
                         *locations,
                         v=None,
                         a=None,
                         splitxyz='xyz',
                         target='gripper',
                         armbias=None,
                         aligngripper=None,
                         orient_gripper=None,
                         reference_axis='+y',
                         ):
        """
        Moves the arm to the specified location(s). If multiple locations are provided, the movements will be sequenced
        in the order provided.

        :param dict locations: dictionary target locations including one or more of x, y, z keys
        :param int v: velocity (counts/s)
        :param int a: acceleration (counts/s/s)
        :param string splitxyz: split synchronization of movements
        :param string target: target ArmModule key (defined in the modules dictionary of the instance)
        :param string armbias: bias for the final arm position (see choose_option function)
        :param aligngripper: whether to align the gripper to a given axis
            The angle to rotate to relative to the reference axis
        :param orient_gripper: whether to orient the gripper to a given axis or angle.
        :param reference_axis: the reference axis for gripper alignment/orientation
            Can be -x, x, -y, y, origin or forearm

        If the aim is to move axes to a value, call move_axes.
        """
        # if handed a single location, interpret
        if len(locations) == 1:
            location = Location(locations[0])  # convert to Location instance

        # if handed a key, location
        elif len(locations) == 2 and locations[0] in ['x', 'y', 'z']:
            location = Location(**{locations[0]: locations[1]})

        else:
            for loc in locations:
                self.move_to_location(
                    loc,
                    v=v,
                    a=a,
                    splitxyz=splitxyz,
                    target=target,
                )
            return None  # break out when complete

        if any([key in ['shoulder', 'elbow', 'vert', 'gripper'] for key in location]):
            # todo allow gripper incorporation in move_to_location
            raise KeyError('Axis movement is not supported in the move_to_location method. Call move_axes instead.')

        if target not in self.modules:  # check that the target is valid
            raise KeyError(f'The target "{target}" is not installed on the arm. ')

        if controller.verbose is True:
            print(f'Moving {target} to {location}')

        # retrieve parameters from default if not specified
        if armbias is None:
            armbias = self.armbias

        # retrieve current target's location for any keys not present
        if any(key not in location for key in ['x', 'y', 'z']):
            current = self.modules[target].location
            for key in current.keys() ^ location.keys():
                location[key] = current[key]

        # calculate inverse kinematics for moving the target to the location
        ik = inverse_kinematics(
            location,
            self.modules[target],
            x_offset=self._x_offset,
            y_offset=self._y_offset,
        )

        # convert to options and check that options are reachable
        options = {}

        # elbow left
        el = self._elbow.radians_to_counts(-ik['alphaprime'])
        sh = self._shoulder.radians_to_counts(ik['gammaprimecw'])
        vert = self._vert.mm_to_counts_relative(ik['vert'])
        if all([  # check that all are in range
            self._elbow._check_in_axis_range(el),
            self._shoulder._check_in_axis_range(sh),
            self._vert._check_in_axis_range(vert),
        ]) is True:
            options['left'] = {
                'elbow': el,
                'shoulder': sh,
                'vert': vert,
            }

        # elbow right
        el = self._elbow.radians_to_counts(ik['alphaprime'])
        sh = self._shoulder.radians_to_counts(ik['gammaprimeccw'])
        vert = self._vert.mm_to_counts_relative(ik['vert'])
        if all([
            self._elbow._check_in_axis_range(el),
            self._shoulder._check_in_axis_range(sh),
            self._vert._check_in_axis_range(vert),
        ]) is True:
            options['right'] = {
                'elbow': el,
                'shoulder': sh,
                'vert': vert,
            }
        if len(options) == 0:
            raise UnreachablePosition(dict(location), self.__class__.__name__)
        elif len(options) == 1:
            chosen = list(options.keys())[0]
        elif len(options) == 2:  # choices available, select based on armbias
            if armbias is None:  # retrieve if not specified
                armbias = self.armbias
            if armbias == 'min shoulder change':  # minimum shoulder movement
                chosen = min(
                    options,
                    key=lambda x: abs(options[x]['shoulder'] - self._shoulder.position)
                )
            elif armbias == 'min elbow change':  # minimum elbow movement
                chosen = min(
                    options,
                    key=lambda x: abs(options[x]['elbow'] - self._elbow.position)
                )
            elif armbias == 'elbow left':
                chosen = min(
                    options,
                    key=lambda x: options[x]['shoulder']
                )
            elif armbias == 'elbow right':
                chosen = max(
                    options,
                    key=lambda x: options[x]['shoulder']
                )
            elif armbias == 'shoulder middle':  # closest to shoulder centered
                chosen = min(
                    options,
                    key=lambda x: abs(options[x]['shoulder'] - self._shoulder.zero)
                )
            elif armbias == 'accuracy':
                raise NotImplementedError('Still working on the accuracy armbias')
                # todo add a 'high accuracy' method that will choose the angle with the closest delta to the target
                chosen = min(
                    options,
                    key=lambda x: sum([

                    ])
                )
            else:
                valid = [  # valid options for returning
                    'min shoulder change',
                    'min elbow change',
                    'elbow left',
                    'elbow right',
                    'shoulder middle',
                    # 'accuracy',
                ]
                raise KeyError(
                    f'The bias "{armbias}" is not recognized. \nValid options: {", ".join(str(x) for x in valid)}')

        # # calculate gripper-yaxis angle (for gripper alignment)
        self.ik = ik  # update angles tracker

        # if gripper alignment/orientation is specified, calculate
        if aligngripper is not None or orient_gripper is not None:
            if chosen == 'left':
                delta = -np.degrees(-ik['alphaprime'] + ik['gammaprimecw'])
            elif chosen == 'right':
                delta = -np.degrees(ik['alphaprime'] + ik['gammaprimeccw'])
            # calculate gripper alignment counts based on delta
            if aligngripper is not None:  # gripper alignment
                if aligngripper == '-x':
                    aligngripper = 'x'
                elif aligngripper == '-y':
                    aligngripper = 'y'
                options[chosen]['gripper'] = self._calculate_gripper_alignment(
                    angle=aligngripper,
                    reference=reference_axis,
                    delta=delta
                )
            elif orient_gripper is not None:  # gripper orientation
                if self._gripper.lock_symmetry is False:  # if the gripper symmetry is not locked, temporarily lock it
                    self._gripper.temp_lock = True  # enable temporary lock
                options[chosen]['gripper'] = self._calculate_gripper_alignment(
                    angle=orient_gripper,
                    reference=reference_axis,
                    delta=delta
                )

        # call move axes
        return self.move_axes(
            options[chosen],
            v=v,
            a=a,
            splitxyz=splitxyz,
        )

    def move_axes(self, *position, v=None, a=None, splitxyz='xyz'):
        """
        Moves the arm axes to the specified position(s).
        If multiple positions are provided, the movements will be sequenced in the order provided.

        :param dict position: dictionary target locations including one or more of sh, el, vert keys
        :param int v: velocity (counts/s)
        :param int a: acceleration (counts/s /s)
        :param string splitxyz: split synchronization of movements
        """
        if v is None:
            v = self.velocity
        if a is None:
            a = self.acceleration

        if len(position) == 1:  # if a single position dictionary
            posdct = position[0]
        elif len(position) == 2 and type(position[0]) == str:
            posdct = {position[0]: position[1]}
        else:
            for pos in position:
                self.move_axes(
                    pos,
                    v=v,
                    a=a,
                    splitxyz=splitxyz,
                )
            return None  # break out when complete

        if any([key not in ['elbow', 'shoulder', 'gripper', 'vert'] for key in posdct]):
            raise KeyError(f'An unexpected key was encountered in the position dictionary.\n'
                           f'Encountered: {", ".join(str(x) for x in posdct.keys())}\n'
                           f'Valid: {", ".join(str(x) for x in ["elbow", "shoulder", "gripper", "vert"])}')

        # check for axes that are actually going to move
        moving = {key for key in posdct if vars(self)[f'_{key}'].position_delta(posdct[key]) != 0}
        if len(moving) == 0:  # break out if no moving axes
            return None

        orders = {
            'xyz': [{'elbow', 'shoulder', 'gripper'}, {'vert'}],  # xy, then z
            'zxy': [{'vert'}, {'elbow', 'shoulder', 'gripper'}],  # z, then xy
            'all': [{'elbow', 'shoulder', 'gripper', 'vert'}],  # move all simultaneously
        }
        if splitxyz is None:
            splitxyz = 'all'
        elif splitxyz not in orders:
            raise ValueError(f'Unrecognized axis order "{splitxyz}". \n'
                             f'Valid options: {", ".join(str(x) for x in orders.keys())}')

        # if gripper is a moving axis, temporarily lock symmetry
        if 'gripper' in moving and self._gripper.temp_lock is False:
            self._gripper.temp_lock = True

        duration = 0
        # for every set of synchronized axes
        for od in orders[splitxyz]:
            if all([key not in moving for key in od]):  # if there are no moving axes in the order set
                continue

            # calcuate the MOPY trajectory
            mopys = {
                key: vars(self)[f'_{key}'].change_position(
                    posdct[key],
                    v=v,
                    a=a,
                ) for key in od & moving  # for each moving axis in the set
            }
            # if the gripper was tempoaraily locked, unlock
            if 'gripper' in od and self._gripper.temp_lock is True:
                self._gripper.temp_lock = False

            # find the MOPY with the longest duration
            longest = max(
                mopys,
                key=lambda key: trajectory_duration(mopys[key]),
            )

            # scale the trajectories to the longest duration trajectory (match acceleration/deceleration/coast times)
            scaled = {
                key: match_trajectory(
                    mopys[longest],
                    mopys[key],
                ) for key in mopys
            }
            for key in scaled:
                if self.mopyinspect is not False and key == self.mopyinspect:  # inspect movement
                    self._track_mopy(scaled[key])
                if not self.simulate:
                    controller.execute(  # load the movement command
                        'move',
                        self._ax_assoc[key],
                        *scaled[key],
                    )
                    self.move_simulator(key, scaled[key])

            # start and wait for axis
            if not self.simulate:
                controller.start_axes(*[self._ax_assoc[key] for key in od & moving])
                if self.blocking:
                    controller.zzz(  # wait for the longest axis to finish moving
                        trajectory_duration(scaled[longest])
                    )
                    controller.wait_on_axes(*[self._ax_assoc[key] for key in od & moving])  # wait for moving flag check

            if self.stepmove is True:
                if input(f'Axes {", ".join(str(x) for x in od)} moved, continue (Y/N)? ').lower() in ['no', 'n']:
                    raise KeyboardInterrupt('User declined to continue. ')

            self.update_module_locations()  # update module locations

            duration += trajectory_duration(scaled[longest])

        return duration

    def safeheight(self,
                   height=None,
                   target=None,
                   armbias='min shoulder change',
                   **kwargs,
                   ):
        """
        Sends the robot arm to the safe height specified. If no height is specified go to the maximum z safe height.
        If height is specified, move the end of the target to go to that height.

        :param None or float height: Height to move to (mm); this will move the end of the target to that height.
            If this is not specified, the arm will move to the maximum z height possible (self.safe_height)
        :param string target: The target on the arm to move to the height
        :param string armbias: armbias for the arm. By default, the arm will choose the angle with the smallest
            shoulder change (i.e. its current orientation). In nearly all cases, this should not require modification.
        :param kwargs: keyword arguments for the move location (see self.goto)
        """
        # if height AND target are provided, just call move_to_location() which will figure it out on its own
        if height is not None and target is not None:
            if target not in self.modules:
                raise KeyError(f'The target "{target}" is not an installed ArmModule instance.')

        # elif target is specified, but no height, move to maximum height
        # to that end, subtract the current item length from height in order to avoid overshoot
        elif height is None and target is not None:
            if target not in self.modules:
                raise KeyError(f'The target "{target}" is not an installed ArmModule instance.')
            if target is 'gripper':
                # make sure the type in the gripper is not a
                # string; this would happen if 'cap' without a length is in the gripper; if item ingrip is
                # string, trying to access length would cause an error
                if self.modules.gripper.item is not None and type(self.modules.gripper.item) is not str:
                    height = self.safe_height - self.modules.gripper.item.length
                else:
                    height = self.safe_height
            elif target is 'probe':
                if self.modules.gripper.item is not None:
                    height = self.safe_height - self.modules.gripper.item.length
                else:
                    height = self.safe_height

        # elif height is specified, but no target, work out which bit sticks out the most at the moment,
        # and set it as target
        elif height is not None and target is None:
            gripper_height = self.module_location('gripper')['z']
            probe_height = self.module_location('probe')['z']
            if gripper_height < probe_height:
                target = 'gripper'
            else:
                target = 'probe'

        # else, if neither is specified, move the gripper to safeheight (arbitrary decision)
        elif height is None and target is None:
            target = 'gripper'
            # make sure the type in the gripper is not a
            # string; this would happen if 'cap' without a length is in the gripper; if item ingrip is
            # string, trying to access length would cause an error
            if self.modules.gripper.item is not None and type(self.modules.gripper.item) is not str:
                height = self.safe_height - self.modules.gripper.item.length
            else:
                height = self.safe_height

        # if the current position is below height, call move_to_location()
        if self.module_location(target)['z'] < height:
            self.move_to_location(
                {'z': height},
                target=target,
                armbias=armbias,  # always choose the option that doesn't move the shoulder
                **kwargs,
            )

    def spin_gripper(self, t=60., rpm=750, direction=1, wait=False):
        """
        Spins the gripper for the specified amount of time at the and rpm specified.

        :param float or int t: time to spin for (seconds)
        :param float or int rpm: revolutions per minute
        :param direction: direction to spin (1 or -1)
        :param wait: whether to wait for the spin to finish
        :return: eta if wait is False
        :rtype: float or None
        """
        mopy = self._gripper.spin(t, rpm, direction)
        eta = trajectory_duration(mopy)

        if not self.simulate:
            controller.execute(
                'move',
                self._grax,
                *mopy,
            )
            controller.start_axes(self._grax)
            self.spin_simulator_gripper(eta, rpm, direction)

            if wait and self.blocking:
                controller.zzz(eta)

        return eta

    def update_module_locations(self):
        """Updates the location of all installed modules based on the current axes positions"""
        for module in self.modules:
            module.update_location(
                forward_kinematics(
                    self._shoulder.radians,
                    self._elbow.radians,
                    self._vert.location,
                    module,
                    x_offset=self._x_offset,
                    y_offset=self._y_offset,
                )
            )

    def _track_mopy(self, mopy):
        """
        If the mopyinspect attribute is active, tracks and verifies the mopy set against the position of the tracked
        axis.

        :param mopy: mopy list (including the direction flag)
        """
        self.mopytracker += (
            mopy_position(mopy)  # total movement of mopy
            * (-1 if pull_dirflag(mopy) == 0 else 1)  # retrieve direction flag and scale +/-
        )
        if controller.verbose is True:
            print(f'key mopy: {mopy}')
            # print(f'mopytracker {self.mopytracker}')
            print(f'MOPYinspect tracked: '
                  f'{self.mopytracker}, nominal: {vars(self)[f"_{self.mopyinspect}"].position}')
        if self.mopytracker != vars(self)[f"_{self.mopyinspect}"].position:
            raise PositionMismatch(  # raise position mismatch if there is one
                self.mopyinspect,
                self.mopytracker,
                vars(self)[f"_{self.mopyinspect}"].position,
            )

    def plot_position(self):
        """
        Plots the current arm position, as well as the location of each installed arm module.
        """
        # figure instance
        fig, ax = pl.subplots(
        )
        fig.canvas.set_window_title('N9 Robot Arm - Current Position')
        ax.axis('equal')  # set equal sizing on units
        ax.set_ylabel('y (mm)')
        ax.set_xlabel('x (mm)')

        # add the lines for the x and y axes
        facecolors = [(0, 0, 0, 0)]

        zero_zero_lines = LineCollection(
            segments=[[(-375 - 20, 0), (375 + 20, 0)], [(0, -219 - 20), (0, 381 + 20)]],
            colors=['black'],
            alpha=0.5,
            linewidths=0.5,
            transOffset=ax.transData,
            linestyle='dotted',
        )

        # create grid of holes
        hole_grid_collection = CircleCollection(
            sizes=(2,),
            facecolors=facecolors,
            edgecolors='k',
            linewidths=0.5,
            offsets=[
                [x, y]
                for x in np.linspace(
                    -375.,
                    375,
                    21
                )
                for y in np.linspace(
                    -219,
                    381,
                    17,
                )
            ],
            transOffset=ax.transData
        )
        # reachable gripper circle
        big_radius = self.modules['gripper'].upper + self.modules['gripper'].lower
        outer_reachable = pl.Circle(
            [0., 0.],
            radius=big_radius,
            fill=None,
            linewidth=0.5,
        )
        # minimum reachable circle
        inner_reachable = pl.Circle(
            [0., 0.],
            radius=90.,
            fill=None,
            linewidth=0.5,
        )
        ax.add_artist(outer_reachable)
        ax.add_artist(inner_reachable)

        ax.add_collection(hole_grid_collection)
        ax.add_collection(zero_zero_lines)

        positions = {key: None for key in self.modules.installed}
        for key in positions:
            currpos = self.module_location(key)
            positions[key] = AsteriskPolygonCollection(
                5,
                sizes=(100,),
                facecolors='r',
                edgecolors='r',
                linewidths=1.,
                offsets=[(currpos['x'], currpos['y'])],
                transOffset=ax.transData
            )
            ax.add_collection(positions[key])

        theta = -(self._shoulder.radians - np.pi / 2)  # angle from x to upper arm
        p1_x = self.modules.gripper.upper * np.cos(theta)  # x position of elbow
        p1_y = self.modules.gripper.upper * np.sin(theta)  # y position of elbow
        line = ax.plot(
            [0., p1_x, self.module_location()['x']],
            [0., p1_y, self.module_location()['y']],
            linewidth=20,
            alpha=0.5,
        )[0]

        ax.set_xlim(
            -375 - 38 / 2,
            375 + 38 / 2,
        )
        ax.set_ylim(
            -219 - 38 / 2,
            381 + 38 / 2,
        )
        fig.tight_layout()
        pl.show()

    def start_simulation(self):
        self.simulate = True
        self.home()

    def stop_simulation(self):
        self.simulate = False

    def move_simulator(self, joint, mopy):
        """
        Instructs the N9 simulator to move a joint, if connected.
        :param str joint: Name of the joint to move
        :param float eta: Duration of the move
        :param list mopy: Mopy movement trajectory
        """

        # try and find a handler based on the name of the join, and call it if it exists
        try:
            handler = getattr(self, f'move_simulator_{joint}')
            eta = trajectory_duration(mopy)
            handler(eta)
        except AttributeError:
            pass

    def sync_simulator(self, duration=2):
        self.move_simulator_shoulder(duration)
        self.move_simulator_elbow(duration)
        self.move_simulator_vert(duration)
        self.move_simulator_gripper(duration)

    def move_simulator_shoulder(self, duration):
        """
        Instructs the N9 simulator to move the shoulder joint to it's current position, over the given duration.
        :param float duration: Duration of the move
        :param int direction: Direction of the move
        """
        position = self._shoulder.degrees
        self.robot_simulator.move_joint('ShoulderJoint', position, duration)

    def move_simulator_elbow(self, duration):
        """
        Instructs the N9 simulator to move the elbow joint to it's current position, over the given duration.
        :param float duration: Duration of the move
        """
        position = self._elbow.degrees
        self.robot_simulator.move_joint('ElbowJoint', position, duration)

    def move_simulator_vert(self, duration):
        """
        Instructs the N9 simulator to move the vertical joint to it's current position, over the given duration.
        :param float duration: Duration of the move
        """
        position = self._vert.location
        self.robot_simulator.move_joint('VerticalJoint', position, duration)

    def move_simulator_gripper(self, duration):
        """
        Instructs the N9 simulator to move the gripper to it's current position, over the given duration.
        :param float duration: Duration of the move
        """
        self.robot_simulator.move_joint('GripperJoint', self._gripper.degrees, duration)

    def spin_simulator_gripper(self, duration, rpm, direction):
        """
        Instructs the N9 simulator to spin the gripper at the given RPM and direction for a duration.
        :param float duration: Amount of time to spin gripper for
        :param float rpm: Speed to spin at (revs / min)
        :param int direction: Direction to spin in (0 or 1)
        """
        revolutions = (duration / 60.0) * rpm * direction
        self.robot_simulator.spin_joint('GripperJoint', revolutions, duration)

    def open_simulator_gripper(self):
        """
        Instructs the N9 simulator to open the gripper.
        """
        self.robot_simulator.send_command('Gripper', 'Open')

    def close_simulator_gripper(self):
        """
        Instructs the N9 simulator close the gripper.
        :return:
        """
        self.robot_simulator.send_command('Gripper', 'Close')


try:
    if controller.verbose is True:
        print(f'Retrieving robot parameters from {os.getcwd()}')
    spec = importlib.util.spec_from_file_location(
            'robot_parameters',
            os.path.join(
                os.getcwd(),
                'robot_parameters.py'
            )
        )
    robot_parameters = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(robot_parameters)
    arm_parameters = robot_parameters.arm_parameters
except ImportError:
    raise ImportError(f'The file "robot_parameters.py" is expected to be in the {os.getcwd()} directory, and should '
                      f'have a "arm_parameters" dictionary defined to define the robot. ')


shoulder = RevoluteJoint(
    **arm_parameters['shoulder'],
)
elbow = RevoluteJoint(
    **arm_parameters['elbow'],
)
gripper = RevoluteJoint(
    **arm_parameters['gripper']
)
z = PrismaticJoint(
    **arm_parameters['z']
)

robotarm = RobotArm(
    shoulder,
    elbow,
    gripper,
    z,
    **arm_parameters['kwargs'],
)

if __name__ == '__main__':
    pass
