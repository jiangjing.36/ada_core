import os
import time
import logging
import warnings

try:
    from ftdi_serial import Serial, SerialException, SerialTimeoutException
except Exception:
    # catch all exception was used as the missing ftd2xx.dll driver seems to be version specific
    warnings.warn("FTDI driver(ftd2xx.dll) failed to load")
from ..dependencies.general import intround
# parameters and descriptions of the robot
from ..dependencies.commands import commands  # commands dictionary
from ..dependencies.exceptions import InvalidCommand, ZeroPositionError


class Communicator(object):
    def __init__(self,
                 device_serial=None,
                 device_number=None,
                 baudrate=115200,
                 timeout=2,
                 verbose=False,
                 stepcycle=False,
                 log=False
                 ):
        """
        The interface between python and a North Robotics controller.

        :param str device_serial: serial number of the FTDI chip to connect to, connects to first device by default
        :param str device_number: the index of the device to connect to (starting at 0)
        :param int baudrate: baud rate for serial communication (default 115200)
        :param float timeout: timeout for serial communication connection (default 1)
        :param bool verbose: whether the instance should print details to console (Bool, default False)
        :param bool stepcycle: enables user input after every execute command (bool, default False)
        :raises KeyError: Encountered if an unsupported keyword argument is provided
        :raises ConnectionError: If no connection could be made to a serial controller
        """

        self.sercon = Serial(
            device_serial=device_serial,
            device_number=int(device_number) if device_number is not None else device_number,
            baudrate=baudrate,
            connect_timeout=timeout,
            write_timeout=0.1,
            read_timeout=60 * 60,  # 1 hour
        )
        self.verbose = verbose
        self.log = log  # whether you want to put verbose to log file or not
        self.logger = logging.getLogger('dependencies.interface.Communicator')
        self.stepcycle = stepcycle
        self.ping()

    def __repr__(self):
        return f'{self.__class__.__name__}({self.sercon.device_serial})'

    def __str__(self):
        return f'{self.__class__.__name__} connecting to serial port {self.sercon.device_serial}'

    def ping(self, retries=5):
        """
        Sends an echo command, retrying if necessary
        :param int retries: number of times to retry ECHO command
        """
        try:
            self.sercon.request(b'<ECHO\r', timeout=1)
        except SerialTimeoutException as err:
            if retries == 0:
                raise err

            time.sleep(1)
            # clear read buffer
            self.sercon.read()
            self.sercon.init_device()
            self.ping(retries - 1)

    def serial_command(self, port, command, wait=None):
        """
        Sends the specified command to the specified N9 comport

        :param int port: N9 comport
        :param string command: string command
        :param float wait: if specified, waits for the specified number of seconds before continuing
        :return: returned value
        :rtype: string
        """
        ret = self.execute(  # execute the specified command
            'com',
            port,
            command,
        )
        if wait is not None:  # if a pause is specified
            self.zzz(wait)
        return ret

    def execute(self, cmd, *args, mute=False, wait=True, validate=False):
        """
        Sends a function command followed by packaged arguments to the North Robotics controller.
        Arguments are the sequential arguments to pass the function.

        :param string cmd: command keyword defined in dependencies.robot_commands
        :param int or string args: arguments to be passed after the function (order will be retained)
        :param bool mute: whether to mute the output of the execute (overrides the class default)
        :param bool wait: whether to wait and read output after passing a the command to the controller
        :param bool validate: error validation of returned value (currently inoperative)
        :return: retrieved output from the controller

        **Example**

        >>> controller = Communicator()
        >>> controller.execute('com', 1, 'flying circus')  # sends command 'flying circus' to controller com port 1
        'SECO'
        """
        # TODO update the errorcheck function to check for <executed_command>
        def errorcheck(written, read):
            """error checks the written string against the read string"""
            if not read.startswith('<') or not read.endswith('>'):  # if the command is not packaged properly
                return False
            elif read[1:-1] != written:  # if the packaged execution does not match what was written
                return False
            else:
                return True  # successful execution and return

        try:
            try:
                strcmd = commands[cmd]  # retrieve the command
            except KeyError:
                self.logger.error(f'InvalidCommand raised: {InvalidCommand(cmd)}')
                raise InvalidCommand(cmd)

            if strcmd == 'MOPY':  # check for zero position errors
                if any([val == 0 for val in args[4::3]]):
                    self.logger.error(f'ZeroPositionError raised: {ZeroPositionError(*args)}')
                    raise ZeroPositionError(*args)

            for i, v in enumerate(args):  # package variables
                if type(v) == float:  # the variable must be an integer
                    v = intround(v)
                strcmd += ' V' + str(i + 1) + '[' + str(v) + ']'

            if self.log is True:
                self.logger.debug(f"Executing command '{strcmd}'")
            if self.verbose is True and not mute:
                print(f"Executing command '{strcmd}'")
            if len(strcmd) >= 1024 - 4:
                self.logger.error(f'ValueError raised: The length of the command ({len(strcmd)})'
                                  f'exceeds 1024 characters (the maximum supported by the controller)')
                raise ValueError(f'The length of the command ({len(strcmd)}) exceeds 1024 characters '
                                 '(the maximum supported by the controller)')
            strcmd = '<' + strcmd + '\r'
            self.sercon.write(  # execute the string
                strcmd.encode('utf-8')
            )

            if wait is True:  # if the user wants to wait for output
                output = self._read()  # retrieve the output
                if self.log is True:
                    self.logger.debug(f"Command response '{output}'")
                if self.verbose is True and not mute:
                    print(f"Command response '{output}'")
            else:
                output = None
            if validate is True:  # if return validation is called for
                pass

            if self.stepcycle is True:  # if step-cycle mode is enabled
                inp = input(f'Command {cmd} executed successfully, continue?')
                if inp.lower() in ['n', 'no']:
                    self.logger.error(f'ValueError: User declined to continue.')
                    raise ValueError('User declined to continue.')

            return output

        except KeyboardInterrupt:  # CTRL+C will break out of any execute command
            self.logger.error(f'KeyboardInterrupt: User interrupted execute function.')
            raise KeyboardInterrupt('User interrupted execute function.')

        except SerialException as e:
            self.logger.error(f'SerialException: {e}', exc_info=True)
            raise SerialException(e)

    def output(self, output, value, sleep=0.2):
        """
        Sets the ouput axis to the specified value (or toggles if no value is provided)

        :param string or int output: the output to toggle
        An axis name can be provided here if it is defined in robot_parameters.outputs)

        :param int or None value: the value to set it to (if omitted, the function will toggle the axis)
        :param float sleep: the wait time after executing to allow the output to engage before moving
        """
        self.execute(
            'output',
            output,
            value,
        )
        self.zzz(sleep)

    def _read(self, sercon=None, sleeptime=0.001):
        """
        Reads the serial buffer.
        The function will wait until the end character is encountered.

        :param Serial instance sercon: optional passing of a serial instance to read from
        Used primarily for automatic connection attempts.

        :param float sleeptime: the time to wait between read calls
        :return: The read value.
        :rtype: string
        """
        if sercon is None:
            sercon = self.sercon
        buffer = ''
        while buffer.endswith('>\r') is False:  # check for the end character
            while sercon.in_waiting == 0:  # wait for more bytes
                self.zzz(sleeptime)
            buffer += sercon.read(sercon.in_waiting).decode('ascii')
        return buffer[1:-2]  # returns the unpackaged string

    def reconnect(self):
        """disconnects and reconnects to the C9"""
        self.sercon.disconnect()
        self.zzz(1.)
        self.sercon.connect()

    def start_axes(self, *axes):
        """
        Sends the axis start command for the axes specified. A full movement requires loading a MOPY set and starting
        the axis for each axis desired.

        :param int axes: axes to start
        """
        # todo figure out a better way to do this (that doesn't require predefining)
        self.execute(
            'go',
            *[1 if i in axes else 0 for i in range(10)],
        )

    def subroutine(self, sequence):
        """
        A decorator function to execute the specified sequence of commands as an N9 subroutine
        (i.e. the commands will be executed in the background while allowing manipulation of the robot arm)
        An example would be executing a sample sequence (manipulating pumps and valves) while manipulating the arm to
        prepare for the next sample.

        :param sequence: sequence of commands that will be executed in function format (a series of n9 commands).
        This function should take the N9 communicator instance as the first argument.
        :return: decorated function
        """
        raise NotImplementedError('The subroutine function has not been completed yet')
        # TODO make sure wait on axes is disasbled for all these commands
        def enclose(*args, **kwargs):
            self.execute('queue subroutine')
            out = sequence(self, *args, **kwargs)  # queue the commands
            self.execute('end subroutine')
            self.execute('go subroutine')
            return out  # if there is any output, return it

        return enclose

    def wait_on_axes(self, *axes, wait=0.001):
        """
        Waits for the specified axes to finish moving.

        :param int axes: axes to wait for
        :param float wait: wait time between reads
        """
        def check():
            """parses the supplied string and checks the axes for movement"""
            spl = [int(i) for i in self.execute('flagcheck', mute=True)][::-1]
            for axis in axes:  # for each of the axes to check
                if spl[axis] is 1:  # if axis is moving, keep moving True
                    return True
            return False  # if no moving axes are encountered, return False

        moving = check()  # perform initial check
        while moving is True:  # if axes are moving
            self.zzz(wait)  # wait for specified time between calls
            moving = check()  # check again

    def zzz(self, t, msg=None):
        """
        Convenience function for waiting and displaying a message. Ignores negative wait times (useful if the wait time
        was calculated, the wait time has elapsed). If a message is specified, the estimated wait time (in seconds)
        is displayed at the end of the provided message.

        :param float t: time to wait (non-zero values will be ignored)
        :param string msg: message to print (optional)
        """
        if t is not None and t > 0.:  # catch for negative times
            if msg is not None:  # append time to message string in appropriate format
                if t > 3600:
                    if self.log is True:
                        self.logger.debug(f'{msg} ~{time.strftime("%H:%M:%S", time.gmtime(t))}')
                    print(msg + f' ~{time.strftime("%H:%M:%S", time.gmtime(t))}')
                elif t > 60:
                    if self.log is True:
                        self.logger.debug(f' {msg} ~{time.strftime("%M:%S", time.gmtime(t))}')
                    print(msg + f' ~{time.strftime("%M:%S", time.gmtime(t))}')
                else:
                    if self.log is True:
                        self.logger.debug(f'{msg} ~{t:.0f} s')
                    print(msg + f' ~{t:.0f} s')
            time.sleep(t)


class PseudoCommunicator(Communicator):
    def __init__(self,
                 verbose=False,
                 stepcycle=False,
                 log=False,
                 **kwargs,
                 ):
        """
        A Communicator mimic that doesn't actually connect to or communicate over a comport. An instance of this may be
        used instead of a true Communicator instance for debugging purposes.

        :param bool verbose: whether the instance should print details to console (Bool, default False)
        :param bool stepcycle: enables user input after every execute command (bool, default False)
        """
        self.verbose = verbose
        self.stepcycle = stepcycle

        # self.logger = None
        # if logging is True:
        self.logger = logging.getLogger('dependencies.interface.PseudoCommunicator')
        self.log = log

    def __repr__(self):
        return f'{self.__class__.__name__}()'

    def __str__(self):
        return self.__repr__()

    def serial_command(self, port, command, wait=None):
        """
        Sends the specified command to the specified N9 comport

        :param int port: N9 comport
        :param string command: string command
        :param float wait: if specified, waits for the specified number of seconds before continuing
        :return: returned value
        :rtype: string
        """
        if wait is not None:  # if a pause is specified
            self.zzz(wait)

    def execute(self, cmd, *args, mute=False, wait=True, validate=False):
        """
        Sends a function command followed by packaged arguments to the North Robotics controller.
        Arguments are the sequential arguments to pass the function.

        :param string cmd: command keyword defined in dependencies.robot_commands
        :param int or string args: arguments to be passed after the function (order will be retained)
        :param bool mute: whether to mute the output of the execute (overrides the class default)
        :param bool wait: whether to wait and read output after passing a the command to the controller
        :param bool validate: error validation of returned value (currently inoperative)
        :return: retrieved output from the controller

        **Example**

        >>> controller = Communicator()
        >>> controller.execute('com', 1, 'flying circus')  # sends command 'flying circus' to controller com port 1
        'SECO'
        """
        try:
            try:
                strcmd = commands[cmd]  # retrieve the command
            except KeyError:
                self.logger.error(f'InvalidCommand raised: {InvalidCommand(cmd)}')
                raise InvalidCommand(cmd)

            if strcmd == 'MOPY':  # check for zero position errors
                if any([val == 0 for val in args[4::3]]):
                    self.logger.error(f'ZeroPositionError raised: {ZeroPositionError(*args)}')
                    raise ZeroPositionError(*args)

            for i, v in enumerate(args):  # package variables
                if type(v) == float:  # the variable must be an integer
                    v = intround(v)
                strcmd += ' V' + str(i + 1) + '[' + str(v) + ']'
            if self.log is True:
                self.logger.debug(f"Executing command '{strcmd}'")
            if self.verbose is True and not mute:
                print(f"Executing command '{strcmd}'")
            if len(strcmd) >= 1024 - 4:
                self.logger.error(f'ValueError raised: The length of the command ({len(strcmd)})'
                                  f'exceeds 1024 characters (the maximum supported by the controller)')
                raise ValueError(f'The length of the command ({len(strcmd)}) exceeds 1024 characters '
                                 '(the maximum supported by the controller)')

            if self.stepcycle is True:  # if step-cycle mode is enabled
                inp = input(f'Command {cmd} executed successfully, continue?')
                if inp.lower() in ['n', 'no']:
                    self.logger.error(f'ValueError raised: User declined to continue.')
                    raise ValueError('User declined to continue.')

        except KeyboardInterrupt:  # CTRL+C will break out of any execute command
            self.logger.error(f'KeyboardInterrupt raised: User interrupted execute function.')
            raise KeyboardInterrupt('User interrupted execute function.')

    def output(self, output, value, sleep=0.2):
        """
        Sets the ouput axis to the specified value (or toggles if no value is provided)

        :param string or int output: the output to toggle
        An axis name can be provided here if it is defined in robot_parameters.outputs)

        :param int or None value: the value to set it to (if omitted, the function will toggle the axis)
        :param float sleep: the wait time after executing to allow the output to engage before moving
        """
        pass

    def _read(self, sercon=None, sleeptime=0.001):
        """
        Reads the serial buffer.
        The function will wait until the end character is encountered.

        :param Serial instance sercon: optional passing of a serial instance to read from
        Used primarily for automatic connection attempts.

        :param float sleeptime: the time to wait between read calls
        :return: The read value.
        :rtype: string
        """
        pass

    def reconnect(self):
        """disconnects and reconnects to the C9"""
        pass

    def start_axes(self, *axes):
        """
        Sends the axis start command for the axes specified. A full movement requires loading a MOPY set and starting
        the axis for each axis desired.

        :param int axes: axes to start
        """
        pass

    def wait_on_axes(self, *axes, wait=0.001):
        """
        Waits for the specified axes to finish moving.

        :param int axes: axes to wait for
        :param float wait: wait time between reads
        """
        pass


def get_communicator(*args, **kwargs):
    """
    Attempts to create a true Communicator instance, and if no propeller chips are detected, creates a
    PseudoCommunicator instance in its place

    :param args: Communicator arguments
    :param kwargs: Communicator keyword arguments
    :return: Communicator instance
    :rtype: Communicator
    """
    try:
        return Communicator(*args, **kwargs)
    except Exception:
        print('No propeller chip was detected, creating PseudoCommunicator instance in its place. ')
        return PseudoCommunicator(*args, **kwargs)


#  initialized communicator instance (import this)
controller = get_communicator(
    # verbose=True,
    log=True,
    device_number=os.environ.get('ADA_DEVICE_NUMBER', None),
    device_serial=os.environ.get('ADA_DEVICE_SERIAL', None),
)

if __name__ == '__main__':
    pass
