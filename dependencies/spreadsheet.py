"""
Class for opening and handling excel files with commonly used data formats

This class was taken from PythoMS.
"""
import sys
import openpyxl as op
import pandas as pd
from ..dependencies.general import inds_to_cellname, cellname_to_inds
from ..dependencies.chemistry import UnitFloat


class XLSX(object):
    def __init__(self,
                 bookname,
                 verbose=False,
                 create=False,
                 ):
        """
        A class for interacting with *xlsx (Microsoft Excel) files. This class requires the openpyxl package to
        function.

        :param str bookname: The name of the *xlsx file to load. The file extension is optional and the input is not
        case sensitive.

        :param bool verbose: Chatty mode.
        :param bool create: Whether to create the specified workbook if it cannot be located.

        **Examples**

        >>> xlfile = XLSX('book1')
        Loading workbook "book1.xlsx" into memory DONE
        >>> xlfile
        XLSX('book1.xlsx')
        """
        self.verbose = verbose
        self.wb, self.bookname = self.loadwb(bookname, create)

    def __str__(self):
        return 'Loaded excel file "%s"' % self.bookname

    def __repr__(self):
        return "%s('%s')" % (self.__class__.__name__, self.bookname)

    def add_cell(self, sheet, cellname, value, save=True):
        """
        Adds a value to the specified cell.

        :param str sheet: sheet name
        :param str cellname: cell name
        :param value: the valuve to inset
        :param bool save: whether to save after writing (default True)
        """
        if isinstance(value, UnitFloat):
            value = float(value)
        if sheet in self.wb.sheetnames:  # check if sheet already present
            cs = self.get_sheet(sheet)
            cs[cellname] = value
        else:  # if not present, create
            cs = self.wb.create_sheet()
            cs.title = sheet
            cs[cellname] = value
        if save is True:  # if saving is desired
            self.save()

    def add_line(self, sheet, *values, save=True):
        """
        Adds the specified values to the next available line in the specified sheet

        :param str sheet: sheet name
        :param values:  values to insert
        :param bool save: whether to save after writing (default True)
        """
        if sheet in self.wb.sheetnames:  # check if sheet already present
            cs = self.get_sheet(sheet)
            nlines = cs.max_row
            for ind, val in enumerate(values):  # for each of the specified values
                if isinstance(val, UnitFloat):
                    val = float(val)
                cs.cell(
                    row=nlines + 1,
                    column=ind + 1,
                ).value = val
        else:  # if not present, create
            cs = self.wb.create_sheet()
            cs.title = sheet
            for ind, heading in enumerate(values):  # fill first row with values
                if isinstance(heading, UnitFloat):
                    val = float(heading)
                cs.cell(
                    row=1,
                    column=ind + 1,
                ).value = heading
        if save is True:  # if saving is desired
            self.save()

    def add_column(self, sheet, *values, save=True):
        """
        Adds the specified values to the next available column in the specified sheet.

        :param str sheet: sheet name
        :param values:  values to insert
        :param bool save: whether to save after writing (default True)
        """
        if sheet in self.wb.sheetnames:  # check if sheet already present
            cs = self.get_sheet(sheet)
            ncols = cs.max_column
            for ind, val in enumerate(values):  # for each of the specified values
                if isinstance(val, UnitFloat):
                    val = float(val)
                cs.cell(
                    row=ind + 1,
                    column=ncols + 1,
                ).value = val
        else:  # if not present, create
            cs = self.wb.create_sheet()
            cs.title = sheet
            for ind, val in enumerate(values):  # fill first row with values
                if isinstance(val, UnitFloat):
                    val = float(val)
                cs.cell(
                    row=ind + 1,
                    column=1,
                ).value = val
        if save is True:  # if saving is desired
            self.save()

    def checkduplicatesheet(self, sheet):
        """
        checks for duplicate sheets in the workbook and creates a unique name

        :param sheet: desired sheet name
        :return: unique sheet name
        :rtype: str
        """
        i = 1
        while sheet + ' (%s)' % str(i) in self.wb.sheetnames:
            i += 1
        return sheet + ' (%s)' % str(i)

    def correctextension(self, bookname):
        """attempts to correct the extension of the supplied workbook name"""
        # todo move this to general
        oops = {'.xls': 'x', '.xl': 'sx', '.x': 'lsx', '.': 'xlsx', '.xlsx': ''}  # incomplete extensions
        for key in oops:
            if bookname.endswith(key) is True:
                bookname += oops[key]
                return bookname
        return bookname + '.xlsx'  # absent extension

    def evaluate(self, string, sheet):
        """
        Attempts to interpret and evaluate the contents of a cell containing basic transformations
        
        **Parameters**
        
        string: *string*
            The contents of a cell
        
        sheet: *string*
            The current sheet
        
        **Returns**
        
        return item: *type*
            description
        
        """
        if string.startswith('=') is False:  # if not an equation
            raise ValueError('The string %s is not an evaluable string' % string)
        contents = string[1:]

        rd = {  # replacement dictionary
            '^': '**',  # power
        }

        cs = self.get_sheet(sheet)

        evalstring = ''
        address = ''
        for ind, val in enumerate(contents):
            if val.isalpha() or val.isdigit():  # if part of an address
                address += val
                continue
            if val in rd:  # if in replacement dictionary
                if len(address) > 0:
                    cell_contents = cs[address]
                    # !! check if cell contents are a value, if not, call another instance of evaluate

                evalstring += rd[val]
            else:
                evalstring += val

        return eval(evalstring)

    def get_sheet(self, sheetname):
        """tries to retrieve the specified sheet name, otherwise returns None"""
        try:
            return self.wb[sheetname]
        except KeyError:
            return None

    def loadwb(self, bookname, create=False):
        """loads specified workbook into class"""
        if self.verbose is True:
            sys.stdout.write('\rLoading workbook "%s" into memory' % bookname)
        try:
            wb = op.load_workbook(bookname)  # try loading specified excel workbook
        except IOError:
            bookname = self.correctextension(bookname)  # attempts to correct the extension of the provided workbook
            if self.verbose is True:
                sys.stdout.write('\rLoading workbook "%s" into memory' % bookname)
            try:
                wb = op.load_workbook(bookname)
            except IOError:
                if self.verbose is True:
                    sys.stdout.write(' FAIL\n')
                if create is True:
                    """
                    Due to write-only mode, creating and using that book breaks the cell calls
                    The workbook is therefore created, saved, and reloaded
                    The remove sheet call is to remove the default sheet
                    """
                    if self.verbose is True:
                        sys.stdout.write('Creating workbook "%s" and loading it into memory' % bookname)
                    # wb = self.op.Workbook(bookname,write_only=False) # create workbook
                    wb = op.Workbook(bookname)  # create workbook
                    wb.save(bookname)  # save it
                    wb = op.load_workbook(bookname)  # load it
                    wb.remove(wb.worksheets[0])  # remove the old "Sheet"
                else:
                    raise IOError(
                        '\nThe excel file "%s" could not be found in the current working directory.' % (bookname))
        if self.verbose is True:
            sys.stdout.write(' DONE\n')
        return wb, bookname

    def removesheets(self, delete):
        """
        Removes a sheet from the excel workbook. 
        
        **Parameters**
        
        delete: *string* or *list*
            The name(s) of the sheet to be deleted from the excel workbook. 
            If a string is supplied, that sheetname will be deleted. 
            If a list of strings is supplied, each sheetname in the list will 
            be deleted. 
        
        
        **Returns**
        
        return item: *type*
            description
        
        
        **Examples**
        
        ::
        
            code line 1
            code line 2
        
        
        **See Also**
        
        optional
        
        """
        """
        removes sheets from the excel file
        delete is a set of strings to be removed
        """
        if type(delete) is str:  # if a single string is provided
            delete = [delete]
        for sheet in self.wb.sheetnames:  # clears sheets that will contain new peak information
            if sheet in delete:
                dels = self.wb[sheet]
                self.wb.remove_sheet(dels)

    def save(self, outname=None):
        """
        Commits changes to the workbook. 
        
        **Parameters**
        
        outname: *string*
            Allows specification of a separate workbook to save as. 
            If this is left as None (default), the provided filename 
            given on initialization will be used. 
        
        
        """
        def version_input(string):
            """checks the python version and uses the appropriate version of user input"""
            import sys
            if sys.version.startswith('3.'):
                return input('%s' % string)
            else:
                raise EnvironmentError('The version_input method encountered an unsupported version of python.')

        if outname is None:
            outname = self.bookname

        try:
            self.wb.save(outname)
        except IOError:
            version_input(
                '\nThe excel file could not be written. Please close "%s" and press any key to retry save.' % outname)
            try:
                self.wb.save(outname)
            except IOError:
                raise IOError('\nThe excel file "%s" could not be written.' % outname)

    def load_excel(self, file):
        # loads excel file and returns a few different formats of the data
        array = []
        dictionary_col = {}
        dictionary_row = {}

        if type(file) is str:
            df = pd.read_excel(file)
        else:
            # assume will only input a file path as a string, or directly load a pandas data frame
            df = file

        # for row at top for headers for the columns
        # for the dictionary_col, the key is the row number, and values are a dictionary of the column headings: value
        # for dictionary_row, key is item in the row of the first column, values are a dictionary of other columns: values
        headers = list(df)

        array.append(headers)

        for i, row in df.iterrows():
            array.append(list(row))
            dict_col = {}
            row_header = None
            dict_row = {}
            idx = 0
            for j, column in row.iteritems():
                dict_col[j] = column
                if idx is 0:
                    row_header = column
                    idx = idx + 1
                if idx is not 0:
                    dict_row[j] = column
            dictionary_col[i] = dict_col
            dictionary_row[row_header] = dict_row

        # df is a panda data frame
        # array is an array; so it will be returned as a list of list
        # dictionary_col: returns a dictionary where every row after the heading row has an index starting at 0
        #   which is the key, and the value is itself a dictionary with row headings as keys and the actual cell value
        #   as the value
        # dictionary_row: returns a dictionary where it does something to similar to dictionary_col, but instead of having
        #   the row number as the key value, it takes the value in the first column and makes it the key (not including
        #   the header row)
        return df, array, dictionary_col, dictionary_row

if __name__ == '__main__':
    name = 'Useless delete this'
    xlfile = XLSX(name, create=True)
