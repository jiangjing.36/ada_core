"""
Methods relating-to and for calculating motion
"""
import numpy as np
from ..dependencies.general import intround, chunk_list
from ..dependencies.coordinates import Location


def trajectory(delta, v=None, a=None, dt=None, bias=None):
    """
    Calculates the trajectory profile of an axis movement

    :param int delta: end value for the movement (counts)
    :param int v: velocity (counts/second)
    :param int a: acceleration (counts/second^2)
    :param float dt: amount of time the movement should take (s). May be specified instead of v.
    :param string bias: bias to use
    :return: appropriately formated list of variables to hand the robot's MOPY function
    :rtype: list of int
    """
    if delta == 0:  # if there is no movement, return empty list
        return []
    elif delta == 1:  # if a movement of 1 count
        return 1, 0, 1

    if v is None:
        if a is None and dt is None:
            raise ValueError('Velocity and acceleration, or time must be specified')

    if bias is None:  # automatically determine bias if not specified
        # if the top of acceleration would be beyond the endpoint, triangular velocity profile
        if intround((v ** 2 / a) / 2) * 2 >= delta:
            bias = 'triangular'
        else:
            bias = 'trapezoidal'

    # catch attempted trapezoidal that can't reach the target velocity
    if bias == 'trapezoidal' and intround((v ** 2 / a) / 2) * 2 >= delta:
        bias = 'triangular'

    if bias == 'triangular':
        if v is None:
            raise ValueError('A velocity must be specified for a triangular profile.')
        v = np.sqrt(delta * a)  # redefine peak velocity
        dist_1 = delta // 2  # midpoint of movement
        out = [
            0,  # start velocity
            a,  # acceleration
            dist_1,  # stop accelerating at this point
            v,  # next velocity
            -a,
            delta - dist_1,
        ]

    elif bias == 'trapezoidal':  # trapezoidal velocity profile
        if v is None:
            raise ValueError('A velocity must be specified for a trapezoidal profile')
        dist_1 = intround(v ** 2 / (2 * a))  # v_f ** 2 = v_i ** 2 + 2 * a * dist
        if dist_1 == 0:  # if the acceleration is too high, the ramp up/down will have a position of zero
            return trajectory(  # return a constant trajectory to avoid this
                delta,
                v,
                a,
                dt,
                bias='constant',
            )
        dist_2 = delta - 2 * dist_1
        out = [
            0,  # start velocity
            a,  # acceleration
            dist_1,  # distance traveled in the acceleration interval
            v,  # velocity
            0,  # no acceleration
            dist_2,  # distance traveled in the constant velocity interval
            v,  # constant velocity
            -a,  # decelerate
            delta - dist_1 - dist_2,  # distance traveled in the deceleration interval
        ]

    elif bias == 'ramp up':
        if a is None:
            a = 2 / (dt ** 2)  # calculate acceleration
        out = [
            0,
            a,
            delta
        ]

    elif bias == 'ramp down':
        if a is None:
            a = 2 / (dt ** 2)  # calculate acceleration
        out = [
                v,
                -a,
                delta
        ]

    elif bias == 'constant':  # constant velocity
        if v is None:
            v = delta / dt
        if v < 1:  # catch for constant velocities less than 1 (v must be at least 1)
            v = 1
        out = [
            v,
            0,
            delta
        ]

    else:
        raise ValueError('The specified bias "%s" is not recognized' % bias)
    return [intround(val) for val in out]  # integer round all values in trajectory


def time_from_vap(v, a, p):
    """
    Calculates the time from a velocity, acceleration

    :param int v: initial velocity
    :param int a: acceleration
    :param int p: position
    :return: time (seconds)
    :rtype: float
    """
    if a != 0:  # if accelerating
        vf = np.sqrt(
            abs(  # absolute to avoid rounding errors
                v ** 2  # initial velocity
                + 2. * a * p
            )
        )

        return (vf - v) / a
    return p / v  # if constant velocity


def trajectory_duration(mopy):
    """
    Calculates the time to complete the trajectory movement of a mopy set.

    :param list mopy: mopy trajectory (can include axis and flag)
    :return: duration of movement
    :rtype: float
    """
    if len(mopy) == 0:  # if there are no values
        return 0.
    elif len(mopy) % 3 != 0:  # if it has axis and direction flags
        mopy = vaps_only(mopy, 3)  # consolidate to vaps only
    return sum([
        time_from_vap(*vap) for vap in chunk_list(mopy, 3)
    ])


def mopy_position(mopy):
    """
    Calculates the total position of a mopy set.

    :param list mopy: list of mopy values (looks for VAP sets)
    :return: position delta
    """
    # todo catch direction flag
    return sum([vap[2] for vap in chunk_list(
        vaps_only(
            mopy
        )
    )])


def pull_dirflag(mopy):
    """
    Retrieves the direction flag from a mopy set.

    :param mopy: list of mopy values
    :return: direction flag
    """
    if len(mopy) % 3 == 0:
        raise ValueError('The mopy set contains only VAPs')
    elif len(mopy) % 3 == 1:  # has a direction flag only
        return mopy[0]  # dirflag is first argument
    elif len(mopy) % 3 == 2:  # if there is an axis and a dirflag
        return mopy[1]  # dirflag is second argument


def vaps_only(lst, n=3):
    """
    Extracts the last n components of a list (useful for extracting VAP sets from a list that has axis and
    direction flags included at the beginning).

    :param list lst: list of items
    :param int n: number of items in the set
    :return: last n extractable items of the list
    :rtype: list
    """
    return lst[len(lst) // n * -n:]


def match_trajectory(target, current):
    """
    Matches the current trajectory to the target trajectory's profile. The acceleration, coasting, and deceleration
    phases will be kept to the same time, and the displacement will remain unchanged.
    If lists of length indivisible by 3 are handed, it will be assumed that the VAP sets are at the end of the provided
    lists.

    :param list target: target trajectory
    :param list current: current trajectory
    :return: adjusted trajectory
    """
    currentflags = current[:len(current) // 3 * -3]  # retrieve flags for current trajectory (if any)
    current = vaps_only(current)  # restrict current list to trajectory only
    target = vaps_only(target)  # restrict target list to trajectory only

    # calculate movement time for each VAP segment
    times = [
        time_from_vap(*vap)
        for vap in chunk_list(target)
    ]

    distance = mopy_position(current)  # calculate required distance to travel
    if len(times) == 1:  # constant
        out = [
            distance / times[0],
            0,
            distance,
        ]
    elif len(times) == 2:  # triangular
        dist_1 = distance // 2  # distance traveled in the acceleration interval
        dist_2 = distance - dist_1
        if any([val < 1 for val in [dist_1, dist_2]]):
            out = trajectory(  # use constant trajectory if any distance regime results in a motion fewer than 1 count
                distance,
                dt=sum(times),
                bias='constant',
            )
        else:
            a = 2 * (distance / 2) / times[0] ** 2
            v = np.sqrt(2 * a * dist_1)
            out = [
                0,
                a,
                dist_1,
                v,  # peak velocity sqrt(v_i ** 2 + 2 * a* d)
                # -1,
                -a,
                distance - dist_1,
            ]
    elif len(times) == 3:  # trapezoidal
        # First try
        v = distance / (times[1] + times[2])  # total distance = d_1 + d_2 + d_3 = v(t_2 + t_3)
        a = v / times[0]  # d = vt + at^2
        dist_1 = a * times[0] ** 2 / 2  # distance traveled in the acceleration/deceleration interval
        dist_2 = distance - 2 * dist_1  # distance traveled in the constant velocity interval
        dist_3 = distance - dist_2 - dist_1

        # Second try
        # dist_1 = current[2]
        # dist_2 = current[5]
        # dist_3 = current[8]
        # v = distance / (times[1] + times[2])  # total distance = d_1 + d_2 + d_3 = v(t_2 + t_3)
        # a = v / times[0]

        if any([val < 1 for val in [dist_1, dist_2, dist_3]]):
            out = trajectory(  # use constant trajectory if any distance regime results in a motion fewer than 1 count
                distance,
                dt=sum(times),
                bias='constant',
            )
        else:
            out = [
                0,
                a,
                dist_1,
                v,
                0,
                dist_2,
                v,
                -a,
                dist_3,
            ]
    else:
        raise ValueError('Unrecognized trajectory length')
    currentflags.extend([intround(val) for val in out])  # insert initial flags (if any)
    return currentflags  # return scaled trajectory with flags appended
