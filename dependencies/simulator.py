"""
This file contains classes related to N9 simulator integration. It exports a 'simulator' instance that can be used to
communicate with the simulator.
"""
import os
import threading
import queue
import time
import json
import atexit
import zmq
import logging
from ..dependencies.coordinates import Location


# track how many assets we've created per type so we can generate new ids
asset_counts = {}


def find_asset_id(asset_name):
    """
    Generates a new asset id by appending an asset number to the name.
    :param str asset_name: the base name of the asset
    :return str: the base name appended with an asset number
    """
    if asset_name not in asset_counts:
        asset_counts[asset_name] = 0
        return asset_name

    asset_counts[asset_name] += 1
    return f'{asset_name}:{asset_counts[asset_name]}'


class AssetVector3(object):
    def __init__(self, x=None, y=None, z=None, default={}, **kwargs):
        """
        Stores a vector with x, y and z elements with optional defaults. The constructor also accepts a list, dict or
        AssetVector3 as the first argument.
        :param Union[float, dict, list, AssetVector3, Location] x: X value
        :param float y: Y value
        :param float z: Z value
        :param dict default: default values
        """
        if type(x) is dict:
            AssetVector3.__init__(self, default=default, **x)
            return

        elif type(x) is Location:
            AssetVector3.__init__(self, default=default, **x.dict('x', 'y', 'z'))
            return

        elif type(x) is list:
            (x, y, z) = x

        elif type(x) is AssetVector3:
            (x, y, z) = x.to_list()

        self.x = x if x is not None else default.get('x', None)
        self.y = y if y is not None else default.get('y', None)
        self.z = z if z is not None else default.get('z', None)

    def to_list(self):
        return [
            float(self.x) if self.x is not None else None,
            float(self.y) if self.y is not None else None,
            float(self.z) if self.z is not None else None,
        ]
        # return [self.x, self.y, self.z]


class AssetJointInfo(object):
    def __init__(self,
                 #type=None,
                 axis=AssetVector3(z=1),
                 position=0,
                 # revolute join
                 offset_angle=0,
                 # prismatic joint
                 input_range=[0, 100],
                 output_range=[0, 100],
                 **kwargs,
                 ):
        """
        Stores information about asset joints.
        :param AssetVector3 axis: Axis to move (the axis vector is multiplied by the position to get the final vector)
        :param AssetVector3 position: Initial position of the joint
        :param float offset_angle: [revolute joint] Degrees to offset the joint angle by
        :param list<float> input_range: [prismatic joint] The range of values for the joint input position
        :param list<float> output_range: [prismatic joint] The range of values for the joint output position
        """
        self.type = kwargs.get('type', None)
        self.axis = AssetVector3(axis)
        self.position = AssetVector3(position)
        self.offset_angle = offset_angle
        self.input_range = input_range
        self.output_range = output_range

    @property
    def message(self):
        """
        :return dict: A dict with joint info ready for serialization
        """
        if self.type is None:
            return None

        return {
            'type': self.type,
            'axis': self.axis.to_list(),
            'position': self.position.to_list(),
            'offset_angle': self.offset_angle,
            'input_range': self.input_range,
            'output_range': self.output_range,
        }


class AssetInfo(object):
    DEFAULT_LOCATION = {'x': 0, 'y': 0, 'z': 0}
    DEFAULT_ROTATION = {'x': 0, 'y': 0, 'z': 0}
    DEFAULT_SCALE = {'x': 1, 'y': 1, 'z': 1}

    def __init__(self,
                 #type='default_asset',
                 file=None,
                 name=None,
                 location=None,
                 rotation={},
                 scale={},
                 model_location={},
                 model_rotation={},
                 model_scale={},
                 model_scale_factor=1,
                 joint={},
                 material=None,
                 color=None,
                 children=[],
                 **kwargs,
                 ):
        """
        Stores information needed for simulator assets. This class is used to fill asset definition default values.
        :param str type: Built-in asset type to load (empty, cube or cylinder) (optional)
        :param str file: Absolute or relative path to model file (relative to the assets directory) (optional)
        :param str name: Name to use when generating IDs (optional)
        :param dict location: Optional asset position dict with x, y, z keys (all optional)
        :param dict rotation: Optional asset rotation dict with x, y, z keys (all optional)
        :param dict scale: Optional asset scale dict with x, y, z keys (all optional)
        :param dict model_location: Optional model position dict with x, y, z keys (all optional)
        :param dict model_rotation: Optional model rotation dict with x, y, z keys (all optional)
        :param dict model_scale: Optional model scale dict with x, y, z keys (all optional)
        :param dict model_scale_factor: Optional model scale factor (optional)
        :param dict joint:
        :param dict[] children: List of kwargs for child AssetInfo instances
        """
        self.type = kwargs.get('type', None)
        self.name = name
        self.material = material
        self.color = color
        self.model_scale_factor = model_scale_factor
        self.children = children
        self.joint = AssetJointInfo(**joint)

        self.location = AssetVector3(Location(location))

        if file:
            if os.path.isabs(file):
                self.file = file
            else:
                self.file = os.path.abspath(os.path.join(__file__, '../../assets/', file))
        else:
            self.file = None

        if type(rotation) in (int, float):
            self.rotation = AssetVector3(z=rotation, default=self.DEFAULT_ROTATION)
        else:
            self.rotation = AssetVector3(rotation, default=self.DEFAULT_ROTATION)

        if type(scale) in (int, float):
            self.scale = AssetVector3(scale, scale, scale)
        else:
            self.scale = AssetVector3(scale, default=self.DEFAULT_SCALE)

        self.model_location = AssetVector3(model_location, default=self.DEFAULT_LOCATION)

        if type(model_rotation) in (int, float):
            self.model_rotation = AssetVector3(z=model_rotation, default=self.DEFAULT_ROTATION)
        else:
            self.model_rotation = AssetVector3(model_rotation, default=self.DEFAULT_ROTATION)

        if type(model_scale) in (int, float):
            self.model_scale = AssetVector3(model_scale, model_scale, model_scale)
        else:
            self.model_scale = AssetVector3(model_scale, default=self.DEFAULT_SCALE)

    @property
    def message(self):
        """
        :return dict: Asset information for serialization
        """
        return {
            'type': self.type,
            'file': self.file,

            'material': self.material,
            'color': self.color,

            'location': self.location.to_list(),
            'rotation': self.rotation.to_list(),
            'scale': self.scale.to_list(),
            'modelLocation': self.model_location.to_list(),
            'modelRotation': self.model_rotation.to_list(),
            'modelScale': self.model_scale.to_list(),
            'modelScaleFactor': self.model_scale_factor,

            'joint': self.joint.message,
        }



class Asset(object):
    def __init__(self,
                 location=None,
                 asset_info=None,
                 parent=None,
                 visible=True,
                 enabled=True,
                 *args,
                 **kwargs):
        """
        Creates simulator assets with the supplied location and asset info.
        :param Location location: Location of the asset
        :param dict asset_info: Asset information (see AssetInfo for fields)
        :param visible: Default visibility (asset is sent to simulator when shown)
        :param enabled: Disabled assets aren't sent to the simulator
        """
        # disable this asset if we didn't get any asset_info
        if not asset_info:
            self.enabled = False
            return

        # create an instance of AssetInfo, which fills in default values
        self.asset_info = AssetInfo(**asset_info)
        self.id = find_asset_id(self.asset_info.name or self.asset_info.type)
        self.parent = parent
        self.visible = visible
        self.enabled = enabled

        if location:
            self.location = Location(location)
        else:
            self.location = Location(*self.asset_info.location.to_list())

        # recursively create child assets
        self.children = [Asset(parent=self, asset_info=child, visible=False) for child in self.asset_info.children]

        if visible:
            self.show()

    @property
    def parent_id(self):
        """
        :return str: The id of the parent asset, or None
        """
        if self.parent:
            return self.parent.id

    @property
    def asset_info_message(self):
        """
        Constructs an asset info message for the simulator
        :return dict: Asset info message
        """
        return json.dumps({
            **self.asset_info.message,
            **{
                'id': self.id,
                'location': AssetVector3(self.location).to_list(),
                'children': [child.asset_info_message for child in self.children],
            }
        })

    def find_child(self, name=None, type=None, id=None, recursive=True):
        """
        Searches child assets (recursively by default) for the first asset matching the given name, type or id.
        :param str name: Asset name
        :param str type: Asset type
        :param str id: Asset id
        :param bool recursive: Search through children recursively
        :return:
        """
        for child in self.children:
            info = child.asset_info
            if name and info.name == name or type and info.type == type or id and child.id == id:
                return child

            if recursive:
                match = child.find_child(name=name, type=type, id=id, recursive=recursive)
                if match:
                    return match

    def show(self, location=None):
        """
        Show this asset in the simulator, if hidden. Updates the location, if given.
        :param Location location: An optional location update
        """
        if not self.enabled:
            return

        if location:
            self.location = Location(location)

        simulator.add_asset(self.asset_info_message)
        self.visible = True

    def hide(self):
        """
        Hide (remove) this asset from the simulator.
        """
        if not self.enabled:
            return

        simulator.remove_asset(self.id)
        self.visible = False

    def move(self, location):
        """
        Move this asset to a new location.
        :param Location location: A new asset location
        """
        if not self.enabled:
            return

        self.location = Location(location)

        if self.visible:
            simulator.move_asset(self.id, self.location)

    def update_location(self, location):
        """
        Update this asset's location (these updates are ignored if "Synchronize Positions" is disabled in the simulator)
        :param Location location: An updated asset location
        """
        if not self.enabled:
            return

        self.location = Location(location)

        if self.visible:
            simulator.update_asset_position(self.id, self.location)

    def attach(self, asset_id):
        """
        Attaches this asset to the given parent asset.
        :param asset_id: Id (or Asset instance) of the parent asset to attach to (use "gripper" or "arm" to attach to
            the robot gripper or arm)
        """
        if not self.enabled:
            return

        if type(asset_id) == Asset:
            asset_id = asset_id.id

        simulator.attach_asset(self.id, asset_id)

    def detach(self):
        """
        Detaches this asset from any parent asset.
        """
        if not self.enabled:
            return

        simulator.detach_asset(self.id)


class RobotSimulator(object):
    def __init__(self, address="tcp://127.0.0.1:8888", heartbeat=5000, enabled=True, verbose=False):
        """
        Communicates with the N9 Robot Simulator over the given TCP socket.
        :param str address: the IP address of the n9_sim server
        :param int port: the port of the n9_sim server
        """
        # try getting the simulator address from an environment variable, otherwise use the provided address
        self.address = os.getenv('N9_SIMULATOR_ADDRESS', address)
        self.enabled = enabled
        self.verbose = verbose
        self.connected = False

        # setup a queue for sending messages from the message thread
        self.message_queue = queue.Queue()
        self.message_thread = threading.Thread(target=self.message_thread_handler)

        # start the message thread and setup a close handler, if enabled
        if self.enabled:
            self.start()

            atexit.register(self.stop)

    def start(self):
        """
        Starts the message thread.
        """
        logging.info(f'Connecting to N9 Simulator on {self.address}')
        self.connected = True
        self.message_thread.start()
        # remove any existing components in the simulator
        self.remove_assets()

    def stop(self):
        """
        Stops the message thread gracefully.
        """
        logging.info("Stop robot simulator")
        self.remove_assets()
        # order of the flag and sleep important
        self.connected = False
        time.sleep(0.1)
        # ensure queue is empty before joining, not strictly required
        while self.message_queue.qsize() > 0:
            try:
                self.message_queue.get(False)
            except queue.Empty:
                continue
            try:
                self.message_queue.task_done()
            except ValueError:
                continue
        self.message_queue.join()
        self.message_thread.join()

    def message_thread_handler(self):
        """
        Thread handler for the message thread.
        """
        try:
            # try creating a ZeroMQ context and socket, then bind it to the simulator address
            self.context = zmq.Context()
            self.socket = self.context.socket(zmq.PUB)
            self.socket.bind(self.address)
        except:
            logging.error("Unable to connect to N9 simulator")
            self.enabled = False
            self.connected = False
            return

        time.sleep(1)

        # start the main loop and keep looping until connected is False
        while self.connected:
            # wait for a new message from the message queue
            topic, message = self.message_queue.get()
            # convert the message to JSON and publish it on the given topic
            json_message = json.dumps(message)
            self.socket.send_multipart([str.encode(topic), str.encode(json_message)])
            if self.verbose:
                logging.info(f'SIM> {json_message}')

            # let the queue know we are done handling this message
            self.message_queue.task_done()

        # close the ZeroMQ socket and context when this thread exits
        self.socket.close()
        self.context.term()
    
    def send_message(self, message, topic="simulator"):
        """
        Sends a raw JSON message to the N9 simulator.
        :param dict message: Message to send
        """
        if not self.enabled:
            return

        self.message_queue.put((topic, message))

    def send_command(self, module, command, *arguments):
        logging.info(f'SIM $ {module} {command} {arguments}')

        args = [json.dumps(a) for a in arguments]
        self.send_message({
            'module': module,
            'command': command,
            'arguments': args
        })

    def move_joint(self, joint, position, duration):
        """
        Sends a message to the N9 simulator instructing it to move a joint to a position over a duration.
        :param str joint: The name of the joint to move
        :param float position: The absolute position of the joint
        :param float duration: The duration of the movement
        """

        self.send_message({
            "module": joint,
            "command": "MoveAbsolute",
            "arguments": [str(position), str(duration)],
        })

    def spin_joint(self, joint, revolutions, duration):
        """
        Spin the given joint for the given number of revolutions over the given duration.
        :param str joint: Joint name
        :param float revolutions: Number of revolutions to spin
        :param float duration: Duration of the spin
        """
        self.send_message({
            "module": joint,
            "command": "Spin",
            "arguments": [str(revolutions), str(duration)],
        })

    def set_module_parameter(self, module, name, value):
        """
        Sets a parameter for the given module.
        :param str module: Module name
        :param str name: Parameter name
        :param any value: Parameter value (must be serializable with json.dumps)
        """
        self.send_message({
            "module": module,
            "command": "SetParameter",
            "arguments": [name, json.dumps(value)]
        })

    def add_asset(self, asset_info_message):
        """
        Adds an asset to the simulator.
        :param dict asset_info_message: A message with the asset name, id, location, offset, rotation and scale
        """
        self.send_message({
            'module': 'SceneManager',
            'command': 'AddAsset',
            # we have to double-encode this due work around Unity's serializing
            'arguments': [asset_info_message]
        })

    def remove_asset(self, asset_id):
        """
        Removes an asset from the simulator.
        :param str asset_id: Id of the asset to remove
        """
        self.send_message({
            'module': 'SceneManager',
            'command': 'RemoveAsset',
            'arguments': [asset_id]
        })

    def remove_assets(self):
        """
        Removes all assets from the simulator.
        """
        self.send_message({
            'module': 'SceneManager',
            'command': 'RemoveAssets',
            'arguments': []
        })

    def move_asset(self, asset_id, location):
        """
        Moves an asset to a new location (ignores the "Synchronize Positions" option in the simulator)
        :param str asset_id: Id of the asset to move
        :param Location location: Location to move the asset to
        """
        self.send_message({
            'module': 'SceneManager',
            'command': 'MoveAsset',
            'arguments': [asset_id, location['x'], location['y'], location['z'] or 0]
        })

    def rotate_asset(self, asset_id, rotation):
        """
        Rotates an asset around the Z axis (ignores the "Synchronize Positions" option in the simulator)
        :param str asset_id: Id of the asset to rotate
        :param float rotation: Degrees of rotation around the Z axis
        :return:
        """
        self.send_message({
            'module': 'SceneManager',
            'command': 'RotateAsset',
            'arguments': [asset_id, rotation]
        })

    def update_asset_position(self, asset_id, location):
        """
        Updates the position of an asset in the simulator (only if "Synchronize Positions" is enabled in the simulator)
        :param str asset_id: Id of the asset to update
        :param Location location: An updated location
        """
        self.send_message({
            'module': 'SceneManager',
            'command': 'UpdateAssetPosition',
            'arguments': [asset_id, location['x'] or 0, location['y'] or 0, location['z'] or 0]
        })

    def attach_asset(self, asset_id, parent_asset_id):
        self.send_message({
            'module': 'SceneManager',
            'command': 'AttachAsset',
            'arguments': [asset_id, parent_asset_id]
        })

    def detach_asset(self, assetId):
        self.send_message({
            'module': 'SceneManager',
            'command': 'DetachAsset',
            'arguments': [assetId]
        })


# create a global instance of the simulator
simulator = RobotSimulator()