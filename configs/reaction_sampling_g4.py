"""
Initializes the setup for reaction sampling generation 4
"""
from ..components.pumps import ValveSyringePump, CavroPump, ContinuousPump
from ..components.valves import SerialValve, ActuatorValve
from ..components.basic import OutputComponent, Plumbing
from ..components.manipulated import Dispenser, Tray, TipRemover, OnDeckPneumaticHolder, Camera
from ..dependencies.general import inds_to_cellname

from ..profiles.pumps import nema_11, VICI_M6_16O, VICI_M6_15S
from ..profiles.trays import needle_tray_gen1, reaction_tray_gen1, vial_tray_gen1
from ..profiles.vials import HPLC_2mL_pierce
from ..profiles.dispensers import prototype_gen1
from ..profiles.tip_removers import Gen1
from ..profiles.syringes import UNF2a_1mL_3cm, UNF2a_250uL_3cm, UNF2a_2_5mL_3cm, UNF2a_5mL_3cm
from ..profiles.grippers import vial_gripper_gen2

tubing = Plumbing(
    *[
        {  # the injection loop of the arm valve
            'name': 'arm loop',
            'volume': 0.040,
        },
        {  # from the sampling needle to the arm valve
            'name': 'needle loop',
            'volume': 0.010,
        },
        {  # from the arm valve to the HPLC valve
            'name': 'arm HPLC valve',
            'volume': 0.620,
        },
        {  # injection loop of HPLC valve
            'name': 'HPLC loop',
            'volume': 0.002,  # roughly 1 uL sample loop
        },
    ]
)

camera = Camera(
    location='G2',
)

vialgripper = OnDeckPneumaticHolder(
    location='K19',
    # orientation='x',  # todo implement rotation capability
    output=1,
    **vial_gripper_gen2,
)

samplepump = ValveSyringePump(
    axis=4,
    output=4,
    positions='valve',
    **UNF2a_250uL_3cm,
    **nema_11,
)

pushpump = ValveSyringePump(
    axis=5,
    output=5,
    positions='valve',
    **UNF2a_1mL_3cm,
    **nema_11,
)

needles40 = Tray(
    location='L3',
    rotate='CCW',
    **needle_tray_gen1,
    population={inds_to_cellname(i, j): {
            'gauge': 22,
            'bd_length': 40.,
        } for j in range(0, 16) for i in range(0, 3)
    },
)

needles50 = Tray(
    location='L3',
    rotate='CCW',
    **needle_tray_gen1,
    population={inds_to_cellname(i, j): {
            'gauge': 21,
            'bd_length': 50.,
        } for j in range(0, 16) for i in range(3, 6)
    },
)

hplctrigger = OutputComponent(
    output=2
)

reactiontray = Tray(
    location='N15',
    rotate='CCW',
    itemclass=None,
    **reaction_tray_gen1,
)

HPLC_2mL_pierce.update({
    'volume': 0.,
    'reaction': True,
    'sampleevery': 600.,
    'piercediameter': 0.,
})

vialtray = Tray(
    location='O6',
    **vial_tray_gen1,
    itemkwargs=HPLC_2mL_pierce,
)

# cavro = CavroPump(
#     comport=2,
#     address='/2',
#     **UNF2a_1mL_3cm,
# )

tricontinent = CavroPump(
    comport=2,
    address='/1',
    **UNF2a_1mL_3cm,
)

spare = ValveSyringePump(
    axis=6,
    output=6,
    positions='valve',
    **UNF2a_1mL_3cm,
    **nema_11,
)

m6_1 = ContinuousPump(
    axis=7,
    **VICI_M6_16O,
)

m6_15S = ContinuousPump(
    axis=8,
    **VICI_M6_15S,
)

dispenser = Dispenser(
    location='N16',
    installed={
        1: m6_1,
        # 2: '',
        3: m6_15S,
        4: tricontinent,
        5: spare,
    },
    **prototype_gen1,
)

remover = TipRemover(
    location='B6',
    **Gen1,
)

hplcvalve = SerialValve(
    comport=1,
    commandset='VICI_2way',
)

armvalve = ActuatorValve(
    axis=9,
    cpr=2000,
    velocity=2000,
)

if __name__ == '__main__':
    pass
