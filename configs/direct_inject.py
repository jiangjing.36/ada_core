from ..components.pumps import CavroPump
from ..components.basic import OutputComponent, Plumbing
from ..profiles.syringes import UNF2a_1mL_3cm, UNF2a_2_5mL_3cm


# sampling pump
samplepump = CavroPump(
    1,
    '/2',
    protocol=485,
    # waittime=1.5,
    **UNF2a_1mL_3cm,
)

# push pump
pushpump = CavroPump(
    1,
    '/1',
    protocol=485,
    # waittime=1.5,
    **UNF2a_2_5mL_3cm,
)

hplctrigger = OutputComponent(
    output=0
)

tubing = Plumbing(
    *[
        {  # the sample loop
            'name': 'sample draw',
            'volume': 0.11,
        },
        {  # from the sample loop to the HPLC
            'name': 'loop to HPLC',
            'volume': 0.70,
        },
        {
            'name': 'HPLC loop',
            'volume': 0.010,
        },
    ]
)
