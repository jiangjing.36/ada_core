import time
import matplotlib.pyplot as pl
from matplotlib.collections import CircleCollection
from matplotlib.collections import LineCollection
from matplotlib.collections import AsteriskPolygonCollection
from matplotlib import patches
from ada_core.dependencies.arm import robotarm as n9
from ada_core.dependencies.exceptions import UnreachablePosition
import numpy as np

# list of grid locations
xys = [
    [x, y]
    for x in np.linspace(
        -375.,
        375,
        21
    )
    for y in np.linspace(
        -219,
        381,
        17,
    )
]


class ClickAndMove(object):
    def __init__(self, no_fly_y=-100.):
        """
        Interactive plot that drives the robot around, showing its current position

        :param no_fly_y: restricts the minimum y value that the robot will go to (no-fly zone)
        """
        n9.safeheight(280.)
        self.currpos = n9.module_location('gripper')
        self.xys = xys

        # figure instance
        self.fig, self.ax = pl.subplots(
        )
        self.fig.canvas.set_window_title('N9 Interactive Driving Mode')
        self.ax.axis('equal')  # set equal sizing on units
        self.ax.set_ylabel('y (mm)')
        self.ax.set_xlabel('x (mm)')

        # add the lines for the x and y axes
        facecolors = [(0, 0, 0, 0)]

        self.zero_zero_lines = LineCollection(
            segments=[[(-375 - 20, 0), (375 + 20, 0)], [(0, -219 - 20), (0, 381 + 20)]],
            colors=['black'],
            alpha=0.5,
            linewidths=0.5,
            transOffset=self.ax.transData,
            linestyle='dotted',
        )

        # define no-fly zone
        self.no_fly_y = no_fly_y
        self.no_fly_patch = patches.Rectangle(
            (
                -375 - 38 / 2,
                -219 - 38 / 2,
            ),
            788,
            abs(no_fly_y + 219 + 38 / 2),
            hatch='x',
            alpha=0.2,
            facecolor='r',
        )
        self.ax.add_patch(self.no_fly_patch)

        # create grid of holes
        self.hole_grid_collection = CircleCollection(
            sizes=(2,),
            facecolors=facecolors,
            edgecolors='k',
            linewidths=0.5,
            offsets=self.xys,
            transOffset=self.ax.transData
        )
        # reachable gripper circle
        big_radius = n9.modules['gripper'].upper + n9.modules['gripper'].lower
        self.outer_reachable = pl.Circle(
            [0., 0.],
            radius=big_radius,
            fill=None,
            linewidth=0.5,
        )
        # minimum reachable circle
        self.inner_reachable = pl.Circle(
            [0., 0.],
            radius=90.,
            fill=None,
            linewidth=0.5,
        )
        self.ax.add_artist(self.outer_reachable)
        self.ax.add_artist(self.inner_reachable)

        self.ax.add_collection(self.hole_grid_collection)
        self.ax.add_collection(self.zero_zero_lines)
        self.current_pos_marker = AsteriskPolygonCollection(
            5,
            sizes=(100,),
            facecolors='r',
            edgecolors='r',
            linewidths=1.,
            offsets=[(self.currpos['x'], self.currpos['y'])],
            transOffset=self.ax.transData
        )
        self.ax.add_collection(self.current_pos_marker)

        self.theta = -(n9._shoulder.radians - np.pi / 2)  # angle from x to upper arm
        self.p1_x = n9.modules.gripper.upper * np.cos(self.theta)  # x position of elbow
        self.p1_y = n9.modules.gripper.upper * np.sin(self.theta)  # y position of elbow
        self.line = self.ax.plot(
            [0., self.p1_x, self.currpos['x']],
            [0., self.p1_y, self.currpos['y']],
            linewidth=20,
            alpha=0.5,
        )[0]

        self.ax.set_xlim(
            -375 - 38/2,
            375 + 38/2,
        )
        self.ax.set_ylim(
            -219 - 38/2,
            381 + 38/2,
        )
        self.fig.tight_layout()
        # using a lambda for the event handler seems to fix the issue with click events not registering on first launch
        self.cid = self.fig.canvas.mpl_connect('button_press_event', lambda event: self.onpress(event))

    def onpress(self, event):
        """Triggered on button press"""
        if event.inaxes is None:
            return

        if event.ydata < self.no_fly_y:
            print(f'The robot is restricted from going to y-positions below {self.no_fly_y}. ')
            return
        # get current position and callback
        self.currpos = {
            'x': event.xdata,
            'y': event.ydata,
        }
        self.fig.canvas.draw_idle()
        self.callback()

    def callback(self):
        """action performed after event"""
        try:
            n9.move_to_location(self.currpos)
        except UnreachablePosition as e:
            print(f'{e}')
            return
        self.current_pos_marker.set_offsets([[self.currpos['x'], self.currpos['y']]])
        theta = -(n9._shoulder.radians - np.pi / 2)
        p1_x = n9.modules.gripper.upper * np.cos(theta)
        p1_y = n9.modules.gripper.upper * np.sin(theta)
        p2_x = self.currpos['x']
        p2_y = self.currpos['y']
        self.line.set_xdata([0., p1_x, p2_x])
        self.line.set_ydata([0., p1_y, p2_y])
        print(f'moved robot arm to x: {self.currpos["x"]:.1f} and y: {self.currpos["y"]:.1f}')


if __name__ == '__main__':
    ClickAndMove()
    pl.show()