import pickle
import os
import pylab as pl
import numpy as np
from itertools import groupby
from datetime import datetime
import matplotlib
from matplotlib.widgets import Lasso
from matplotlib.collections import CircleCollection
import matplotlib.gridspec as gridspec
from matplotlib import colors as mcolors
import matplotlib.animation as animation
import matplotlib.path as path
from ..dependencies.general import Watcher
from ..dependencies.general import avg_stdev

if os.getenv('chemospath') is None:
    raise ValueError('A file path for ChemOS could not be found. Please define a "chemospath" environment variable '
                     'or add a definition to your .env file in the root of your project. ')


chemospath = os.getenv('chemospath')

inputfolder = 'Parameters'  # name of input folder
outputfolder = 'Results'  # name of output folder
conditions_key = 'processes'  # name of the dictionary key containing conditions and values
measurements_key = 'properties'  # name of the dictionary key where the output values will be stored
inputwatch = Watcher(  # create Watcher instance for the input folder
    os.path.join(chemospath, inputfolder),
    '*.pkl'
)
chemosstatuswatch = Watcher(  # watcher for ChemOS status file
    chemospath,
    'ChemOS_status.pkl',
    includesubfolders=False,
)

class InteractiveInspectionPlot(object):
    def __init__(self, key):
        """
        Creates an interactive plot of injection # vs the supplied key. The points can be lassoed to highlight values.
        The details of those values will be printed in the console, and a parameterplot with those highlighed values
        will be generated

        :param key: key defined for each job
        """
        self.fig, self.ax = pl.subplots(
            1,
            figsize=(9.6, 5.4),
        )

        self.canvas = self.ax.figure.canvas

        self.ydata = [job[key] for job in timesort]
        self.xdata = [i + 1 for i, val in enumerate(self.ydata)]
        self.xys = [vals for vals in zip(self.xdata, self.ydata)]
        self.areas = [job['average'] for job in timesort]

        facecolors = [(0, 0, 0, 0) for d in self.xys]
        self.collection = CircleCollection(
            sizes=(30,),
            facecolors=facecolors,
            edgecolors='k',
            offsets=self.xys,
            transOffset=self.ax.transData
        )

        self.ax.add_collection(self.collection)
        self.ax.autoscale()

        self.ax.set_ylabel(key)
        self.ax.set_xlabel('injection #')
        self.ax.set_title('lasso points of interest')
        pl.autoscale()
        pl.tight_layout()
        self.cid = self.canvas.mpl_connect('button_press_event', self.onpress)

    def callback(self, verts):
        """Things to do after that mouse click"""
        facecolors = self.collection.get_facecolors()
        p = path.Path(verts)
        ind = p.contains_points(self.xys)
        inds = []
        ci = 0
        for i, val in enumerate(self.xys):
            if ind[i]:
                print_details(i)
                inds.append(i)
                facecolors[i] = colors[ci % len(colors)]
                ci += 1
            else:
                facecolors[i] = (0, 0, 0, 0)
        parameter_plot(
            highlightpoint=inds
        )
        print('-' * 40, '\n')
        self.canvas.draw_idle()
        self.canvas.widgetlock.release(self.lasso)
        del self.lasso

    def onpress(self, event):
        """Triggered even on mouse click"""
        if self.canvas.widgetlock.locked():
            return
        if event.inaxes is None:
            return
        self.lasso = Lasso(
            event.inaxes,
            (event.xdata, event.ydata),
            self.callback
        )
        # acquire a lock on the widget drawing
        self.canvas.widgetlock(self.lasso)


class InteractiveParameterPlot(object):
    def __init__(self):
        self.vals = {
            ind: {
                'values': [job['scaled parameters'][ind] for job in timesort],
            } for ind, val in enumerate(parameter_order)
        }
        self.y = [job['average'] for job in timesort]

        self.fig, *ax = pl.subplots(
            3,
            2,
            sharey=True,
            # figsize=(9.6, 5.4),
            figsize=(19.2, 10.8)
        )
        self.ax = [axes for subaxes in ax[0] for axes in subaxes]
        self.facecolors = [(0, 0, 1., 0.1) for val in timesort]
        for ind in self.vals:
            curr = self.vals[ind]
            curr['xys'] = list(zip(
                curr['values'],
                self.y,
            ))
            curr['collection'] = CircleCollection(
                sizes=(30,),
                facecolors=list(self.facecolors),
                edgecolors='k',
                offsets=curr['xys'],
                transOffset=self.ax[ind].transData
            )
            self.ax[ind].add_collection(curr['collection'])
            self.ax[ind].autoscale(axis='y')
            self.ax[ind].set_xlim(*current_scalars[ind])
            self.ax[ind].set_xlabel('%s (%s)' % (parameter_order[ind], units[ind]))
        self.ax[2].set_ylabel('peak area (a.u.)')
        pl.tight_layout()
        self.cid = [ax.figure.canvas.mpl_connect('button_press_event', self.onpress) for ax in self.ax]

    def callback(self, verts):
        """Things to do after that mouse click"""
        facecolors = list(self.facecolors)
        p = path.Path(verts)
        for j in self.vals:
            # facecolors = self.vals[j]['collection'].get_facecolors()
            ind = p.contains_points(self.vals[j]['xys'])
            ci = 0
            for i, val in enumerate(self.vals[j]['xys']):
                if ind[i]:
                    print_details(i)
                    facecolors[i] = colors[ci % len(colors)]
                    ci += 1
                # else:
                #     facecolors[i] = (0, 0, 0, 0)
        for j in self.vals:
            self.vals[j]['collection'].set_facecolor(facecolors)
        print('-' * 40, '\n')
        for ax in self.ax:
            ax.figure.canvas.draw_idle()
            ax.figure.canvas.widgetlock.release(self.lasso)
        del self.lasso

    def onpress(self, event):
        """Triggered even on mouse click"""
        # if self.canvas.widgetlock.locked():
        #     return
        if event.inaxes is None:
            return
        self.lasso = Lasso(
            event.inaxes,
            (event.xdata, event.ydata),
            self.callback
        )
        # acquire a lock on the widget drawing
        for ax in self.ax:
            ax.figure.canvas.widgetlock(self.lasso)


def retrieve_contents(filepath, keys=None):
    """
    Retrieves ChemOS dictionaries from pickle file

    :param filepath: path to file
    :param keys: keys to retrieve (if None, all keys will be returned)
    :return: dictionary present in file
    """
    with open(filepath, 'rb') as openpickel:
        out = pickle.load(openpickel)
    if keys is None or len(keys) == 0:
        return out
    return {
        key: out[key] for key in keys
    }


def retrieve_files(folder):
    filewatch = Watcher(
        folder,
        '*.pkl',
        includesubfolders=False,
    )
    outputfiles = filewatch.contents
    if len(outputfiles) == 0:
        raise ValueError('There were no pickle files found in the target directory.')
    # retrieve contents
    retrieved = [retrieve_contents(file) for file in outputfiles]

    # consolidates by job id
    consolidated = []
    for i, group in groupby(retrieved, key=lambda item: item['job_id']):
        grouped = {}
        areas = []
        for j, job in enumerate(group):
            if j == 0:
                grouped.update(job)
                grouped.__delitem__('peak_area')
            areas.append(job['peak_area'])
        grouped['areas'] = areas
        avg, stdev, pstdev = avg_stdev(areas)
        grouped['average'] = avg
        grouped['stdev'] = stdev
        grouped['pstdev'] = pstdev
        consolidated.append(grouped)

    # set of values for the maximum peak area observed
    maxset = max(consolidated, key=lambda x: x['average'])

    for ind, val in enumerate(consolidated):
        # difference between parameter and optimal parameter
        consolidated[ind]['difference'] = [i[1] - i[0] for i in zip(val['parameters'], maxset['parameters'])]
        # squared difference
        consolidated[ind]['square difference'] = [i ** 2 for i in consolidated[ind]['difference']]
        # summed square difference for keys of interest
        consolidated[ind]['summed error'] = sum([
            v for i, v in enumerate(consolidated[ind]['square difference']) if i in [
                0,  # loop volume
                1,  # extra pull volume
                2,  # push volume
                # 3,  # sample velocity
                # 4,  # push velocity
                # 5,  # wait time after draw
            ]
        ])

    # sort retrieved contents by date and timestamp
    timesort = sorted(
        consolidated,
        key=lambda x: x['timestamp'],
    )
    return timesort, maxset


def plot_the_works(extrakey, *highlightinds):
    """
    Plots a subplot of the specified key beside a set of paramter subplots

    :param extrakey: key for the y-axis values of the left-most subplot
    :param highlightinds: optional highlighting of the indicies specified (indicies correspond to the pythonic index
    in the timesort list)
    :return: figure object
    """
    if highlightinds is None:
        highlightinds = []
    fig = pl.figure(  # create base figure
        # figsize=(19.20, 10.80),
        figsize=(9.6, 5.4),
    )
    outer = gridspec.GridSpec(  # create grid
        1,
        2,
        # wspace=0.1,
        width_ratios=[1, 2],
    )
    left = gridspec.GridSpecFromSubplotSpec(
        1,
        1,
        subplot_spec=outer[0]
    )
    facecolors = [(0, 0, 1, 0.) for job in timesort]
    mainax = pl.Subplot(  # create choice subplot
        fig,
        left[0]
    )
    mainxys = [vals for vals in zip(
        [ind for ind, val in enumerate(timesort)],
        [job[extrakey] for job in timesort]
    )]
    highlightxys = [mainxys[ind] for ind in highlightinds]
    collection = CircleCollection(
        sizes=(20,),
        facecolors=facecolors,
        edgecolors=(0, 0, 0, 0.5),
        offsets=mainxys,
        transOffset=mainax.transData
    )
    highlightcol = CircleCollection(
        sizes=(30,),
        facecolors=[colors[i % len(colors)] for i, val in enumerate(highlightinds)],
        edgecolors='k',
        offsets=highlightxys,
        transOffset=mainax.transData,
    )
    mainax.add_collection(collection)
    mainax.add_collection(highlightcol)
    mainax.set_ylabel(extrakey)
    mainax.set_xlabel('injection #')
    mainax.autoscale()
    fig.add_subplot(mainax)

    indxys = [
        [(val, timesort[ind]['average']) for val in timesort[ind]['scaled parameters']] for ind in highlightinds
    ]
    inner = gridspec.GridSpecFromSubplotSpec(
        3,
        2,
        subplot_spec=outer[1],
    )
    # parameter axes
    paxes = [pl.Subplot(fig, i) for i in inner]
    areas = [job['average'] for job in timesort]
    for ind, ax in enumerate(paxes):
        xys = [vals for vals in zip([job['scaled parameters'][ind] for job in timesort], areas)]
        coll = CircleCollection(
            sizes=(20,),
            facecolors=facecolors,
            edgecolors=(0, 0, 0, 0.5),
            offsets=xys,
            transOffset=ax.transData,
        )
        for i, high in enumerate(highlightinds):
            highcol = CircleCollection(
                sizes=(30,),
                facecolors=colors[i % len(colors)],
                edgecolors='k',
                offsets=indxys[i][ind],
                transOffset=ax.transData
            )
            ax.add_collection(highcol)
        ax.add_collection(coll)
        ax.set_xlabel('%s (%s)' % (parameter_order[ind], units[ind]))
        ax.set_xlim(*current_scalars[ind])
        fig.add_subplot(ax)
        ax.autoscale(axis='y')
    outer.tight_layout(fig)
    return fig


def print_details(ind):
    """
    Prints the details of the job in the time-sorted list of jobs

    :param ind: pythonic index of the job in the timesort list
    """
    print('Selected index: %d' % ind)
    print('Areas: %s' % str(timesort[ind]['areas']))
    print('Job ID: %s' % timesort[ind]['job_id'])
    print('Parameters: %s' % str(timesort[ind]['scaled parameters']))
    print()


def key_plot(key):
    """
    Plots the key vs injection number for the entire series

    :param key: dictionary key defined in the jobs
    """
    yvals = [job[key] for job in timesort]
    xvals = [i + 1 for i, val in enumerate(yvals)]
    fig, ax = pl.subplots(1)
    ax.scatter(
        xvals,
        yvals,
        marker='o',
        facecolors=(0, 0, 1, 0.5),
        s=10,
        picker=10,
    )
    ax.set_ylabel(key)
    ax.set_xlabel('injection #')
    pl.tight_layout()
    return fig, ax, [xvals, yvals]


def parameter_plot(*inds, key='scaled parameters', highlightpoint=None):
    """
    Plots specified parameters in subplots

    :param inds: the indicies of the parameters list to plot
    :param key: 'difference' from max set or raw 'parameters' values
    :param highlightpoint: indicies in the time-sorted list to highlight
    :return: figure, axes
    """
    if len(inds) == 0:
        inds = [ind for ind, val in enumerate(current_scalars)]
    elif len(inds) == 1:
        inds = [inds]
    vals = {
        ind: [] for ind in inds
    }
    y = []
    for job in timesort:
        for ind in inds:
            vals[ind].append(job[key][ind])
        y.append(job['average'])
    if len(inds) <= 3:
        fig, *ax = pl.subplots(len(inds), sharey=True)
        if len(inds) > 1:
            ax = [axes for axes in ax[0]]
    else:
        fig, *ax = pl.subplots(
            3,
            2,
            sharey=True,
            figsize=(9.6, 5.4),
        )
        ax = [axes for subaxes in ax[0] for axes in subaxes]
    for i, ind in enumerate(inds):
        ax[i].scatter(
            vals[ind],
            y,
            label='parameter %d' % ind,
            marker='o',
            facecolors=(0, 0, 1, 0.5),
            s=10,
        )
        coll = CircleCollection(
            sizes=(30,),
            facecolors=(0, 0, 0, 0),
            edgecolors='r',
            offsets=[
                maxset['scaled parameters'][ind],
                maxset['average']
            ],
            transOffset=ax[i].transData
        )
        ax[i].add_collection(coll)
        # ax[i].scatter(
        #     maxset['parameters'][ind],
        #     maxset['average'],
        #     marker='o',
        #     # color='g',
        #     facecolors='none',
        #     edgecolors='g',
        #     s=20,
        # )
        ax[i].set_xlabel('%s (%s)' % (parameter_order[ind], units[ind]))
        ax[i].set_xlim(*current_scalars[ind])
    if highlightpoint is not None:
        if type(highlightpoint) == int:
            highlightpoint = [highlightpoint]
        for pi, point in enumerate(highlightpoint):
            vals = timesort[point]
            for i, ind in enumerate(inds):
                # col = CircleCollection(
                #     sizes=(30,),
                #     facecolors=colors[pi % len(colors)],
                #     edgecolors='k',
                #     offsets=[
                #         vals['scaled parameters'][ind],
                #         maxset['average']
                #     ],
                #     transOffset=ax[i].transData
                # )
                ax[i].scatter(
                    vals['scaled parameters'][ind],
                    vals['average'],
                    marker='o',
                    s=30,
                    facecolors=colors[pi % len(colors)],
                    edgecolors='k',
                )
    pl.tight_layout()
    return fig, ax


def animate_parameters(*inds, interval=200, save=False):
    """
    Animates the ChemOS parameter optimization

    :param inds: indicies to track
    :param interval: interval (ms) between frames
    :param save: whether to save the animation as an .mp4 file in the output folder
    :return: animation object
    """
    def set_axlims(marginfactor):
        """
        Fix for a scaling issue with matplotlibs scatterplot and small values.
        Takes in a pandas series, and a marginfactor (float).
        A marginfactor of 0.2 would for example set a 20% border distance on both sides.
        Output:[bottom,top]
        To be used with .set_ylim(bottom,top)
        """
        border = abs(maxset['average'] * marginfactor)
        maxlim = maxset['average'] + border
        minlim = -border
        return minlim, maxlim

    def animate(i):
        nonlocal vals, maxval
        # if newy is not None:  # skips first value
        #     yvals = np.append(yvals, newy)
        nextup = timesort[i]  # get next job
        newy = nextup['average']

        # check for new maximum values
        if newy > maxval:
            maxval = newy
            newmax = True  # flag for new max value
        else:
            newmax = False

        # update scatters
        for ind in vals:
            param = nextup['scaled parameters'][ind]
            if vals[ind]['newval'] is not None:  # append last value
                vals[ind]['sequential'].append(vals[ind]['newval'])
            vals[ind]['newval'] = [param, newy]  # current set of values
            if newmax is True:  # if there is a new value, change max value
                vals[ind]['maxplot'].set_offsets(np.asarray([param, maxval]))
            vals[ind]['current'].set_offsets(np.asarray([param, newy]))  # current value
            vals[ind]['valplot'].set_offsets(np.asarray(vals[ind]['sequential']))  # previous values
        return [vals[ind]['axes'] for ind in vals]

    if len(inds) == 0:  # if no parameters are specified, use all
        inds = [ind for ind, val in enumerate(parameter_order)]
    elif len(inds) == 1:  # if only 1, cast to list
        inds = [inds]
    vals = {  # create dictionary
        ind: {
            'maxval': 0.,  # the maximum value found
            'newval': None,  # the incoming value
            'sequential': [],  # the sequential list of values

        } for ind in inds
    }

    fs = 16
    maxval = 0.
    # initial state of figure
    fig, *ax = pl.subplots(
        3,
        2,
        sharey=True,
        figsize=(19.20, 10.80)
    )
    ax = [axes for subaxes in ax[0] for axes in subaxes]
    ax[2].set_ylabel('peak area (a.u.)', size=fs)
    yrange = set_axlims(0.05)
    for ind, axes in enumerate(ax):
        axes.set_xlabel('%s (%s)' % (parameter_order[ind], units[ind]), size=fs)
        axes.set_xlim(*current_scalars[ind])
        axes.set_ylim(*yrange)
        axes.tick_params(labelsize=fs)
    pl.tight_layout()

    for ind, axes in enumerate(ax):  # associate axes with indicies and set initial state
        vals[ind]['axes'] = axes
        vals[ind]['maxplot'] = axes.scatter(  # scatter for max value
            [],
            [],
            marker='o',
            s=40,
            facecolors=(0, 0, 0, 0),
            edgecolors='r',
        )
        vals[ind]['valplot'] = axes.scatter(  # scatter for values
            [],
            [],
            marker='o',
            s=15,
            facecolors=(0, 0, 1, 0.5),
        )
        vals[ind]['current'] = axes.scatter(  # incoming value
            [],
            [],
            marker='o',
            s=30,
            facecolors='orange',
            edgecolors='k',
            # animated=True,
        )
    interval = interval
    ani = animation.FuncAnimation(
        fig,
        animate,
        frames=len(timesort),
        interval=interval,
        # blit=True,  # doesn't work with figure saving
        repeat=False,
    )
    if save is True:
        ani.save(
            os.path.join(
                folder,
                'run parameters animation (%d ms).mp4' % interval,
            ),
            # writer='imagemagick'
            writer='ffmpeg',
            bitrate=1000,
            codec='libx264',
        )
    return ani


def animate_key(key='average', forced_ylabel='peak area (a.u.)', interval=200, save=False):
    """
    Animates the ChemOS parameter optimization

    :param key: the key to plot on the y-axis
    :param forced_ylabel: overrides the use of the key name for the y axis label
    :param interval: interval (ms) between frames
    :param save: whether to save the animation as an .mp4 file in the output folder
    :return: animation object
    """

    def set_axlims(minv, maxv, marginfactor=0.02):
        """
        Fix for a scaling issue with matplotlibs scatterplot and small values.
        Takes in a pandas series, and a marginfactor (float).
        A marginfactor of 0.2 would for example set a 20% border distance on both sides.
        Output:[bottom,top]
        To be used with .set_ylim(bottom,top)
        """
        border = abs((maxv - minv) * marginfactor)
        maxlim = maxv + border
        minlim = minv - border
        return minlim, maxlim

    def animate(i):
        nonlocal xys, maxval
        nextup = xys[i]  # get next job
        newy = nextup[1]

        # check for new maximum values
        if newy > maxval:
            maxval = newy
            maxsc.set_offsets(np.asarray(nextup))

        if i > 0:
            sc.set_offsets(np.asarray(xys[:i]))
        currsc.set_offsets(np.asarray(nextup))

        return ax

    yvals = [job[key] for job in timesort]
    xvals = list(range(1, len(yvals) + 1))

    xys = list(zip(xvals, yvals))  # xy values

    fs = 16
    maxval = 0.
    # initial state of figure
    fig, ax = pl.subplots(
        1,
        1,
        figsize=(19.20, 10.80)
    )
    if forced_ylabel is not None:
        ax.set_ylabel(forced_ylabel, size=fs)
    else:
        ax.set_ylabel(key, size=fs)
    ax.set_xlabel('injection #', size=fs)
    ax.set_xlim(*set_axlims(1, xvals[-1]))
    ax.set_ylim(*set_axlims(
        min(yvals),
        max(yvals)
    ))
    ax.tick_params(labelsize=fs)
    sc = ax.scatter(  # value scatter
        [],
        [],
        marker='o',
        s=15,
        facecolors=(0, 0, 1, 0.5),
    )
    maxsc = ax.scatter(  # max set
        [],
        [],
        marker='o',
        s=40,
        facecolors=(0, 0, 0, 0),
        edgecolors='r',
    )
    currsc = ax.scatter(
        [],
        [],
        marker='o',
        s=30,
        facecolors='orange',
        edgecolors='k',
    )
    pl.tight_layout()
    interval = interval

    ani = animation.FuncAnimation(
        fig,
        animate,
        frames=len(timesort),
        interval=interval,
        # blit=True,  # doesn't work with figure saving
        repeat=False,
    )
    if save is True:
        ani.save(
            os.path.join(
                folder,
                'run %s animation (%d ms).mp4' % (key, interval),
            ),
            writer='ffmpeg',
            bitrate=1000,
            codec='libx264',
        )
    return ani


def check_chemos_status():
    """checks the status of chemos"""
    with open(chemosstatuswatch.contents[0], 'rb') as file:
        return pickle.load(file)['status']


def set_chemos_status(status):
    """
    Sets the chemos status to that specified

    :param status: desired status
    :return: previous status
    """
    with open(chemosstatuswatch.contents[0], 'rb') as file:
        previous = pickle.load(file)
    with open(chemosstatuswatch.contents[0], 'wb') as file:
        pickle.dump(
            {'status': status},
            file
        )
    return previous


def get_parameters(wait_for=False):
    """
    Gets the oldest set of parameters from the conditions folder.

    :param bool wait_for: whether to wait for the appearance of a file.
    :return: dictionary stored in the oldest loaded_conditions file.
    :rtype: dict
    """
    if len(inputwatch) == 0:  # if there is no file
        if wait_for is False:  # and wait for is False, return None
            return None
        inputwatch.wait_for_presence()
    with open(inputwatch.oldest_instance(), 'rb') as inputfile:
        loaded_conditions = pickle.load(inputfile)
    if 'filename' not in loaded_conditions:  # if the filename is not in the file, store in dictionary
        loaded_conditions['filename'] = inputwatch.oldest_instance().split(os.sep)[-1]
    return loaded_conditions


def remove_conditions_file(filename):
    """
    Removes the specified conditions file from the conditions directory. If the file does not exist, nothing is modified.

    :param filename: name of the file to remove
    """
    try:
        os.remove(
            os.path.join(inputwatch.path, filename)
        )
    except FileNotFoundError:
        pass


def write_completed_parameters(
        parameters,
        remove_conditions=True,
        filename=None,
):
    """
    Writes the parameters of a completed experiment to a file in the measurements folder. If no timestamp is included
    in the parameters dictionary, one will be added in this method.

    :param dict parameters: parameter dictionary to write
    :param bool remove_conditions: whether to remove the parameter file from the conditions folder
    :param str filename: filename to remove. If there is a `filename` key in the `parameters` dictionary,
        this need not be provided.
    """
    if filename is None and 'filename' not in parameters:
        raise ValueError('No filename was provided and the filname key is not in the parameters dictionary.')
    elif filename is None:
        filename = parameters['filename']
    fullpath = os.path.join(chemospath, outputfolder, filename)
    if os.path.isfile(fullpath) is True:
        if input(
                f'The file {filename} already exists in the target directory, overwrite? Y/N'
        ).lower() not in ['y', 'yes']:
            print('User declined to continue, writing aborted')
            return

    if 'timestamp' not in parameters:
        parameters['timestamp'] = str(datetime.now())

    if remove_conditions is True:  # remove the input file
        remove_conditions_file(filename)

    with open(os.path.join(chemospath, outputfolder, filename), 'wb') as outputfile:
        pickle.dump(
            parameters,
            outputfile,
        )


if __name__ == '__main__':
    # list of colours to use for highlighting
    colors = [mcolors.to_rgba(col) for col in ['red', 'orange', 'yellow', 'green', 'blue', 'violet']]

    font = {
        'family': 'Arial',
        'size': 14,
    }
    matplotlib.rc('font', **font)

    # todo figure out how to combine runs from multiple folders
    # target folder
    folders = [
        # 'backup_random_search_012718_1116am',  # triplicate run #1
        # 'backup_random_search_012918_0844am',  # triplicate run #2
        # 'backup_smac_013018',  # 2018-01-29 smac run
        # 'backup_phoenics_013118_1001am',  # 2018-01-30 pheonics run (truncated due to bug)
        # 'backup_spearmint_020118_1422pm',  # 2018-01-31 spearmin run
        'backup_020818_2008pm',  # random exploration run 1
        'backup_2018-02-13',  # random exploration run 2
    ]
    folder = os.path.join(
        chemospath,
        'output',
        # 'backup_2018-03-30',  # first ranked optimization
        # 'backup_random_search_012718_1116am',  # triplicate run #1
        # 'backup_random_search_012918_0844am',  # triplicate run #2
        # 'backup_smac_013018',  # 2018-01-29 smac run
        'backup_phoenics_013118_1001am',  # 2018-01-30 pheonics run (truncated due to bug)
        # 'backup_spearmint_020118_1422pm',  # 2018-01-31 spearmin run
        # 'backup_020818_2008pm',  # random exploration run 1 (load this one for entire run)
        # 'backup_2018-02-13',  # random exploration run 2
    )



    try:
        with open(os.path.join(folder, 'lars_run_summary.sum'), 'rb') as preprocessed:
            loaddct = pickle.load(preprocessed)
            timesort = loaddct['timesort']
            maxset = loaddct['maxset']
            parameter_order = loaddct['parameter_order']
            current_scalars = loaddct['current_scalars']
            units = loaddct['units']
        print('Loaded from preprocessed')

    except FileNotFoundError:
        try:  # try to load scalars used in the run
            with open(os.path.join(folder, 'run_scalars.scalars'), 'rb') as scalarfile:
                runscalardetails = pickle.load(scalarfile)
                parameter_order = runscalardetails['parameter order']
                current_scalars = [runscalardetails['scalars'][key] for key in parameter_order]
                units = runscalardetails['units']
        except FileNotFoundError:
            print('No scalar file found, using most recently defined values.')
            from auto_inject_scalars import scalars, parameter_order, units
            current_scalars = [
                scalars[-1][key] for key in parameter_order
            ]

        timesort, maxset = retrieve_files(folder)

        with open(os.path.join(folder, 'lars_run_summary.sum'), 'wb') as preprocessed:
            pickle.dump(
                {
                    'timesort': timesort,
                    'maxset': maxset,
                    'parameter_order': parameter_order,
                    'current_scalars': current_scalars,
                    'units': units,
                },
                preprocessed
            )
