"""
Store syringe profiles here

The items in this dictionary may be handed to the Syringe class as the "profile" parameter.
The syringe profiles must not start with "Syringe".

:param inner_diameter
inner diameter of the syringe
:param vol
volume that the syringe currently contains

UNIQUE_NAME = {
'id': ###,
'vol': ###,
}

"""

Hamilton_500uL = {
    'inner_diameter': 0.,
    'volume': 0.5,
}


# 3 cm travel sryinges with 1/4-28 UNF 2a threads (used with N9 pumps as well as Tricontinent-style pumps)
# max volume, max distance to travel, internal diameter
UNF2a_250uL_3cm = {
    # 'inner_diameter': 3.14,
    # 'max_vol': 0.23231135195500444,
    'inner_diameter': 3.254477342,
    'volume': 0.249559232,
    'length': 30
}

UNF2a_1mL_3cm = {   # 1 on the Excel sheet
    'inner_diameter': 6.520897118,
    'volume': 1.001903359,
    'length': 30
}

UNF2a_1mL_3cm_6_4901mm = {      # 2 on the Excel sheet
    'inner_diameter': 6.490100145,
    'volume': 0.992462103,
    'length': 30
}

UNF2a_2_5mL_3cm = {     # 1 on the Excel sheet
    'inner_diameter': 10.29440018,
    'volume': 2.496969455,
    'length': 30
}

UNF2a_2_5mL_3cm_10_2938mm = {       # 2 on the Excel sheet
    'inner_diameter': 10.29376608,
    'volume': 2.496661856,
    'length': 30
}

UNF2a_5mL_3cm = {
    'inner_diameter': 14.41684453,
    'volume': 4.897242012,
    'length': 30
}

