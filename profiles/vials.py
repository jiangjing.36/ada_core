"""
Store vial profiles here

The items in this dictionary may be handed to the Vial class as the vialproperties argument
All measurements should be in mm.


capdiameter: the diameter of the cap (if this is the same as the vial diameter, this is not required)
crimp: whether it is a crimp cap (True or False)
diameter: the diameter of the vial
height: the total height from the base to the top of the vial (with cap tightened)
screw: whether it is a screw cap (True or False)
piercediameter: the diameter of the pieceable area of the cap (if there is no pieceable area, this is not required)
pitch: the pitch of the threads (tighen cap, measure vial height (H0), unscrew 360deg, measure again (H1); the pitch is H1-H0 in mm)
rotations: the number of rotations required to remove a screw cap (round up if partial)
piercedepth: if piercing with a PierceArray, the needle will penetrate to this depth relative to the top of the cap

Copy and paste the following to create a new entry:

UNIQUEVIALNAME = {
'capdiameter': None,
'capheight': None,
'crimp': True/False,
'diameter': ###,
'height': ###,
'screw': True/False,
'piercediameter': ###,
'pitch': ###,
}

"""

HPLC_2mL_pierce = {  # 2 mL HPLC vial with a piercable cap
    'capdiameter': 11.51,
    'capheight': 5.82,
    'crimp': False,
    'diameter': 11.62,
    'height': 33.41,
    'screw': True,
    # 'piercediameter': 5.97,
    'screwpitch': 1.07,
    'rotations': 4,
    'piercedepth': 3.,
    'maximum_volume': 1.7,
}

twenty_mL = {
    'capdiameter': 27.05,
    'capheight': 11.86,
    'screw': True,
    'rotations': 2,
    # 'piercediameter': 14.29,
    'piercedepth': 3.,
    'maximum_volume': 20.,
    'height': 58.9,
    'diameter': 27.4
}

cuvette_vials = {  # plastic 1 mL cuvette vials
    'capdiameter': 12.8,
    'capheight': 9.2,
    'crimp': False,
    'diameter': 10.3,
    'height': 47.,
    'screw': True,
    'screwpitch': 1.07,
    'rotations': 3,
}

dram_1 = {  # 1 dram (~4 mL vials) with piercable caps
    'capdiameter': 15.5,
    'capheight': 9.5,
    'crimp': False,
    'screw': True,
    'diameter': 14.75,
    'height': 46.83,
    'screwpitch': 48.68-46.83,
    # 'piercediameter': 8.35,
    'rotations': 2,
    'piercedepth': 5.,
}

dram_2 = {  # 2 dram (~7 mL vials) with screw caps
    'capdiameter': 17.91,
    'capheight': 9.5,
    'crimp': False,
    'screw': True,
    'diameter': 16.7,
    'height': 62.15,
    'screwpitch': 63.79-62.15,
    # 'piercediameter': 8.35,
    'rotations': 2,
    'piercedepth': 3.,
    'maximum_volume': 7.,
}
