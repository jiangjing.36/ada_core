"""
Storage for components which are controlled by COM port communication

create new profiles by adding a new value

UNIQUE_NAME = {
    'global prefix': '',  # a globally required prefix required for all commands
    'global affix': '',  # a globally required affix required for all commands
    'commands': {  # valid complete commands
        'callname': '',
    },
    'packaged commands': {  # commands which enclose a value
        'callname': {
            'prefix': '',  # string to insert at the beginning of the command
            'affix': '',  # string to append at the end of the command
        },
    },
}
"""

tricontinent_pump = {  # command set for a Tricontinent Pump
    # 'global prefix': '/1',  # required to address the TC pump
    'global affix': 'R',  # run command
    'commands': {  # valid complete commands
        'velocity': 'V',  # change the pump's velocity
        'input': 'I',  # set to input position
        'output': 'O',  # set to output position
        'goto': 'A',  # go to the provided position in counts
        'zeroright': 'Z',  # zero the pump (valve right)
        'zeroleft': 'Y',  # zero the pump (valve left)
    },
}

VICI_2way = {  # VICI 2-way valve
    'commands': {
        1: 'GO1',  # go to position 1
        2: 'GO2',  # go to position 2
    }
}
