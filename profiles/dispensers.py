"""
Profiles for dispensers (held in the gripper)
"""

prototype_gen1 = {
    'x_offset': -18.75,
    # 'y': -19.25,
    'y_offset': -20.4,
    # 'length': 47.0 + 6.0,  # for appropriate calculation by inverse kinematics with the on-dispenser length
    'height': 95.5,  # height that the module sits at (gripper will grip here)
    'thickness': 6.,
    'clearheight': 24.,  # height required to go above dock dowel
    'dist_to_tubes': 41.,  # distance from center of gripper to tubes
    'index_degrees': 4,  # degrees of rotation separating each index
    'outputs': 5,  # number of tubes
    'output_depth': 7.,  # depth that the tubes protrude below the module
}
