"""
Trays of items (needle, pipette tip, vial, etc.)

UNIQUE_NAME = {
    'height': 0.,  # height where the items will be placed (the bottom of the holes)
    'spacing': 0.,  # center-center spacing
    'flipx': True,  # columns increase to robot's left (see locationarray function for details)
    'flipy': False,  # rows increase towards robot
    'arrayindicies': [  # define each hole array as a dictionary in this list
        {
            'x': 0.,  # x offset from index to center of hole A1
            'y': 0.,  # y offset from index to center of hole A1
            'z': 0.,  # z offset (will be added to height value above)
            'rows': 0,  # number of rows (y)
            'columns': 0,  # number of columns (x)
        },
    ]
}

"""
from ..assets import trays as tray_assets


needle_tray_gen1 = {
    # todo verify which one it is!
    'height': 121.,
    # 'height': 118.,
    'spacing': 8.5,  # hole to hole spacing
    'fliprow': True,  # rows increase towards robot
    'rows': 6,
    'columns': 16,
    'x_offset': -11.,
    'y_offset': 4.25,
    'itemclass': 'Needle',
    'additional_offsets': {
        'start': 'D1',
        'stop': 'F16',
        'y': -4.,
        'z': 24.,
    },
}

needle_tray_gen2 = {
    # base is 150 mm tall, block is 22. mm, and probe interacts with needle 8 mm above that
    # 'height': 150. + 22. + 5.75, #+5.0 can be too tight, +6.0 maybe too loose, +5.5 just a bit too tight (failed 1/12 to remove),
    'height': 148.20 + 22.10,   # the actual height of where the needle is sitting
    'spacing': 9.,
    'fliprow': True,  # rows increase towards robot
    'rows': 3,
    'columns': 16,
    'x_offset': 49.75,
    'y_offset': 12.25,
    'itemclass': 'Needle',
}

needle_tray_gen3 = {
    'height': 75.7,  # base is 75.7 mm tall
    'spacing': 9,  # hole to hole spacing
    'rows': 12,
    'columns': 22,
    'x_offset': -18,
    'y_offset': 2.25,
    # 'itemclass': 'Needle',
    'additional_offsets': [
        {
            'start': 'D1',
            'stop': 'F22',
            'y': 3.,
            'z': 25.,
        },
        {
            'start': 'G1',
            'stop': 'I22',
            'y': 6.,
            'z': 50.,
        },
        {
            'start': 'J1',
            'stop': 'L22',
            'y': 9.,
            'z': 75.,
        },
    ]
}

needle_tray_gen2_nadder = {
    'height': 148.3 + 22.,
    'spacing': 9.,
    'fliprow': True,  # rows increase towards robot
    'rows': 3,
    'columns': 16,
    'x_offset': 49.91,
    'y_offset': 12.53,
    'itemclass': 'Needle',
}

pipette_tip_tray_gen1 = {
    'spacing': 10.,  # hole to hole spacing
    'height': 106.,  # height from base to top
    'fliprow': True,  # rows increase towards robot
    'x_offset': 10.,
    'y_offset': 10.,
    #'z_offset': 16. - 9.,
    'rows': 3,
    'columns': 7,
    'additional_offsets': [
        {
            'start': 'B1',
            'stop': 'B7',
            'z': 3.,
        },
        {
            'start': 'C1',
            'stop': 'C7',
            'z': 1.,
        }
    ],
    'asset': tray_assets.pipette_tip_tray_gen1,
}

pipette_tip_tray_gen2_nadder = {  # for holding big 1250 uL pipette tips
    'spacing': 9.,  # hole to hole spacing
    'height': 148.3 + 22. - 0.5,  # height from base to top
    'fliprow': True,  # rows increase towards robot
    'x_offset': 49.91,
    'y_offset': 12.53,
    'rows': 3,
    'columns': 16,
}

vial_tray_gen1 = {  # tray of vials
    'height': 39.,  # height where the items will be placed
    'spacing': 20.,  # center-center spacing
    'x_offset': -2.5,
    'y_offset': -145.,
    'rows': 8,
    'columns': 12,
    'itemclass': 'Vial',
    'asset': tray_assets.vial_tray_gen1,
}

hplc_4x6 = {  # Allan's custom-made hplc-vial tray that fits in a 96-well plate format
    'height': 23. + 25.6 - 21.5,
    'spacing': 20.,
    'rows': 4,
    'columns': 6,
    # 'itemclass': 'Vial',
    'x_offset': 12.5,
    'y_offset': -30.,
}

reaction_tray_gen1 = {  # reaction tray generation 1 rotated 90 degrees CCW
    'height': 56.0,  # height where the items will be placed
    # 'height': 55.5,  # height where the items will be placed
    'spacing': 20.,  # center-center spacing
    'flipcol': True,  # columns increase to robot's left (see locationarray function for details)
    'x_offset': -5.,
    'y_offset': -37.5,
    'rows': 1,
    'columns': 8,
}

well_plate_96 = {  # SPL 96 well plate
    'height': 27.,  # height where the items will be placed
    'spacing': 9.,  # center-center spacing
    'x_offset': 12.,
    'y_offset': -32.5,
    'rows': 8,
    'columns': 12,
    'itemclass': 'LocationContainer',
    'itemkwargs': {
        'depth': 11.,
        'max_vol': 0.351,
    },
}

chemglass_optichem_2_dram = {  # 96-well plate format(ish) that holds 24 2 dram vials
    'height': 23. + 3.,
    'spacing': 21.,
    'x_offset': 15.,
    'y_offset': -31.5,
    'rows': 4,
    'columns': 6,
}

chilled_2_dram = {  # Custom chilled block for 2 dram vials with retaining lid
    'height': 41.,
    'spacing': 25.4,
    'x_offset': -24.20,
    'y_offset': -10.50,
    'rows': 3,
    'columns': 5,
    'additional_offsets': [
            {
                'start': 'B1',
                'stop': 'B5',
                'y': 1.6,  # I think...
                'z': 16.,
            },
            {
                'start': 'C1',
                'stop': 'C5',
                'y': 3.2,  # I think...
            },
    ],
    'asset': tray_assets.chilled_2_dram,
}

cuvette_vial_tray = {  # tray for plastic cuvette-style vials
    'height': 24.5,  # height where the items will be placed
    'spacing': 20.,  # center-center spacing
    'x_offset': -12.5,
    'y_offset': -30.,
    'rows': 4,
    'columns': 6,
}
