"""
Profile storage for dependencies.arm.ArmModule and dependencies.arm.DispenserProbe
See the documentation of those classes for more details.

The general format for the ArmModule profile is:

>>> UNIQUE_NAME = {
    'sh_el': 170.25,
    'el_point': 170.25,
    'hoffset': 0.,
    'voffset': 0.,
}

The base distance for the dispenser-probe mount is +47.0 mm above the gripper.

The general format for the DispenserProbe profile is:

>>> UNIQUE_NAME = {
    'sh_el': 170.25,
    'el_point': 170.25,
    'hoffset': 0.,
    'voffset': 0.,
    'length': 0.,
    'att_length': 0.,
    'top_width': 0.,
    'bot_width': 0.,
}

"""

toothless_2mL_gripper = {
    'el_point': 170.25,
    'length': 20.,
    'name': 'gripper',
    'module_type': 'ArmModule',
}

zippleback_2mL_gripper = {
    'el_point': 170.25,
    'length': 21.,  # apparently this is actually 21 mm, not 19
    'name': 'gripper',
    'module_type': 'ArmModule',
}

needle_gen1 = {
    'el_point': 211.75,
    'voffset': 14.,
    'attachment_length': 12.0,  # the length of the segment that will interact with the tips
    'top_width': 4.56,  # the width at the top of the segment
    'bot_width': 3.93,  # the width at the bottom of the segment
    'name': 'probe',
    'module_type': 'DispenserProbe',
}

needle_gen2 = {
    'el_point': 211.75,
    'voffset': 14.,
    'length': 33.,  # total length from the base of the threads to the tip
    'attachment_length': 12.,  # the length of the segment that will interact with the tips (used for tip removal calculations)
    'top_width': 4.5,  # the width of the attachment at the top of the attachment segment
    'bot_width': 3.95,  # the width of the attachment at the bottom of the attachment segment
    'name': 'probe',
    'module_type': 'DispenserProbe',
}

needle_gen3 = {  # very similar to Gen2, but in stainless steel
    'el_point': 211.75,
    'voffset': 14.,
    'length': 33.,
    'attachment_length': 11.5,
    'top_width': 4.35,
    'bot_width': 3.90,
    'name': 'probe',
    'module_type': 'DispenserProbe',
}

# todo figure out how to have all three types on one profile
pipette_gen1_5mm = {  # pipette probe at the 5mm attachment point
    'el_point': 211.75,
    'voffset': 14.,
    'length': 32.,
    'attachment_length': 10.,
    'top_width': 5.46,
    'bot_width': 5.2,
    'name': 'probe',
    'module_type': 'DispenserProbe',
}

pipette_gen1_8mm = {  # pipette probe at the 8mm attachment point
    'el_point': 211.75,
    'voffset': 14.,
    'length': 32.,
    'attachment_length': 23.,
    'top_width': 7.17,
    'bot_width': 7.12,
    'name': 'probe',
    'module_type': 'DispenserProbe',
}

combo_gen1 = {  # combination pipette and needle probe gen 1
    'el_point': 211.75,
    'voffset': 14.,
    'length': 33.,
    'attachment_length': 12.,
    'top_width': 4.39,
    'bot_width': 3.87,
    'name': 'probe',
    'module_type': 'DispenserProbe',
}
