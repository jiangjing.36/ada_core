"""
Grippers (pneumatic)

UNIQUE_NAME = {
    'height': 0.,  # the height of the bottom of the gripper from the robot deck
    'xoffset': 0.,  # the x offset from the index
    'yoffset': 0.,  # the y offset from the index
    'activationdelay': 0.25,  # delay time after activation to allow for movement of the holder
}

"""
vial_gripper_gen1 = {
    'height': 85.,  # the height of the bottom of the gripper from the robot deck
    'x_offset': 0.,  # the x offset from the index
    'y_offset': 7.,  # the y offset from the index
}

vial_gripper_gen2 = {
    'height': 102. - 24.,
    'x_offset': 37.5 / 2,
    'y_offset': -4.5,
    'approach_offset': 5.
}
