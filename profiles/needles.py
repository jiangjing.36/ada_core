"""
Store needle profiles here

Rather than individual profiles, it seemed more reasonable to profile gauges and lengths for disposable BD needles


gauges stores the measured outer diameter of a variety of needle gauges. These vaules are used to calculate pierce
arrays for vials.

BD_lengths stores the effective length (from the dispenser tip to the tip of the needle) for a variety of BD needle
lengths.
"""

BD_40 = {  # 40 mm BD needles
    'total_length': 56.,
    'height': 5.55,
    'interaction_depth': 9.52,
}

BD_50 = {  # 50 mm BD needles
    'total_length': 68.4,
    'height': 5.7,
    'interaction_depth': 9.5,
    'pierce_length': 47.0,
}

# dictionary relating needle gauges to outer diameter (units in mm)
gauges = {
    7: 4.572,
    8: 4.191,
    9: 3.759,
    10: 3.404,
    11: 3.048,
    12: 2.769,
    13: 2.413,
    14: 2.108,
    15: 1.829,
    16: 1.651,
    17: 1.473,
    18: 1.270,
    19: 1.067,
    20: 0.9081,
    21: 0.8192,
    22: 0.7176,
    23: 0.6414,
    24: 0.5652,
    25: 0.5144,
    26: 0.4636,
    27: 0.4128,
    28: 0.3620,
    29: 0.3366,
    30: 0.3112,
    31: 0.2604,
    32: 0.2350,
    33: 0.2096,
    34: 0.1842,
}

# lengths that each BD "length" corresponds to in terms of the robot, tuple (length from tip, overall length)
BD_lengths = {
    25.: (32., 42.75),
    40.: (45., 56.),
    50.: (58., 68.5)
}
