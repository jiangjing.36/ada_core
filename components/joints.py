"""
Classes required for interacting with actuators and joints. The behaviour and location of
the actuators and joints is described and stored within the classes.
"""
import warnings
import numpy as np
from ..dependencies.exceptions import AccelerationLimit, VelocityLimit, UnreachablePosition
from ..dependencies.general import intround, calculate_rpm, calculate_velocity
from ..dependencies.motion import trajectory


class Actuator(object):
    def __init__(self,
                 cpr=None,
                 ratio=1,
                 max_v=None,
                 max_a=None,
                 max_rpm=None,
                 axis_range=None,
                 v=20000,
                 a=100000,
                 initial=0,
                 defaulttraj='trapezoidal',
                 dirflag=1,
                 **kwargs):
        """
        Describes an actuator (motor) and generates appropriate trajectory profiles for the actuator.

        :param int ratio: gearhead ratio
        :param int cpr: counts per revolution
        :param int max_v: maximum velocity for the motor (counts/second)
        :param int max_a: maximum acceleration for the motor (counts/second^2)
        :param int max_rpm: maximum rpm for the motor (revolutions per minute)
        :param int initial: initial position for axis (default 0)
        :param list axis_range: axis allowable range (counts)
        :param int v: default velocity (counts/second)
        :param int a: default acceleration (counts/second^2)
        :param string defaulttraj: the default trajectory profile to use
        :param int dirflag: direction flag for positive axis movement
            (if the motor is moving in the opposite of the desired direction, set this to 0)
        :param kwargs: catch in case additional keyword arguments are accidentally handed in Parent/Child initialization
        """

        # set ratio
        self.ratio = ratio

        # set counts per revolution
        if cpr is None:
            raise ValueError('Counts per revolution is not defined for the Actuator')
        else:
            self.cpr = cpr * self.ratio

        # store maximums and limits
        self.max_v = max_v  # maximum velocity
        self.max_a = max_a  # maximum acceleration
        self.max_rpm = max_rpm  # maximum rpm
        self.axis_range = axis_range  # axis range
        if any(  # if there are defined maximums
                [val is not None for val in [self.max_v, self.max_rpm]]
        ) and any(  # and one is not defined
            [val is None for val in [self.max_v, self.max_rpm]]
        ):
            if self.max_rpm is not None:
                self.max_v = calculate_velocity(
                    self.max_rpm,
                    self.cpr,
                )
            if self.max_v is not None:
                self.max_rpm = calculate_rpm(
                    self.max_v,
                    self.cpr,
                )

        # initial state of the joint
        self._initial = initial
        self._pos = None
        self.position = self._initial
        # self._lastpos = int(self.position)  # storage for last position

        # default trajectory profile to use
        self._defaulttraj = defaulttraj

        # direction flag for axis movement (1 is positive, 0 is negative)
        self._dirflag = dirflag

        # set velocity and acceleration
        self._encoder_v = None  # velocity storage attribute
        self.encoder_v = v  # set velocity
        self._encoder_a = None  # acceleration storage attribute
        self.encoder_a = a  # set acceleration

    def __repr__(self):
        return f'{self.__class__.__name__}({self.position})'

    def __str__(self):
        return f"{self.cpr} counts per revolution Actuator at {self.position} counts"

    def _check_acceleration(self, acceleration):
        """checkes whether the specified acceleration is below the maximum"""
        if self.max_a is not None:
            return acceleration <= self.max_a
        return True

    def _check_velocity(self, velocity):
        """checks whether the specified velocity is below the maximum"""
        if self.max_v is not None:
            return velocity <= self.max_v
        return True

    def check_rpm(self, rpm):
        """checks whether the specified rpm is below the maximum"""
        if self.max_rpm is not None:
            return rpm <= self.max_rpm
        return True

    @property
    def encoder_v(self):
        """velocity of the encoder"""
        return self._encoder_v

    @encoder_v.setter
    def encoder_v(self, v):
        if self._check_velocity(v) is False:
            raise VelocityLimit(v, self.max_v, str(self))
        self._encoder_v = v

    @property
    def encoder_a(self):
        """acceleration of the encoder"""
        return self._encoder_a

    @encoder_a.setter
    def encoder_a(self, a):
        if self._check_acceleration(a) is False:
            raise AccelerationLimit(a, self.max_a, str(self))
        self._encoder_a = a

    def change_velocity(self, v):
        """
        Changes the encoder velocity of the Actuator.

        :param v: velocity (counts/second)
        """
        warnings.warn('the change_velocity method has been depreciated, modify the encoder_v attribute directly')
        self.encoder_v = v

    def change_acceleration(self, a):
        """
        Changes the encoder acceleration of the Actuator.

        :param a: acceleration (counts/second**2)
        """
        warnings.warn('the change_acceleration method has been depreciated, modify the encoder_a attribute directly')
        self.encoder_a = a
        if self._check_acceleration(a) is False:
            raise AccelerationLimit(a, self.max_a, str(self))
        self.encoder_a = a

    def _check_in_axis_range(self, abspos):
        """
        Verifies that the absolute position is within the accessible range of the axis. If there is no range specified
        for the axis, this will always return True.

        :param abspos: absolute position
        :return: boolean whether value is within range
        """
        if self.axis_range is not None:
            return self.axis_range[0] <= abspos <= self.axis_range[1]
        return True

    def home(self):
        """sets the axis position to the homed state"""
        self.position = int(self._initial)

    @property
    def position(self):
        """Returns the position of the axis in relative or absolute values"""
        return self._pos

    @position.setter
    def position(self, position):
        if self._check_in_axis_range(position) is False:
            raise UnreachablePosition(
                position,
                name=repr(self),
                limits=self.axis_range,
            )
        self._pos = position

    def position_delta(self, position):
        """returns the difference between the current joint position and the specified position"""
        if type(position) != int:
            position = intround(position)
        if self._check_in_axis_range(position) is False:  # if the provided position is out of range, raise error
            raise UnreachablePosition(
                position,
                name=repr(self),
                limits=self.axis_range,
            )
        return position - self.position

    def change_position(self, position, v=None, a=None, dt=None, bias=None):
        """
        Changes the position tracker of the actuator and generates a MOPY command trajectory for the actuator.
        If the keywords are not specified, the class instance defaults will be used.

        :param position: position to move to
        :param v: velocity (counts/second)
        :param a: acceleration (counts/second^2)
        :param dt: delta time (for making a trajectory last a specified amount of time)
        :param bias: trajectory profile
        The velocity and acceleration will be scaled proportionally to relation of the change in counts divided by
        the scalar.

        :return: appropriately formated list of variables to hand the MOPY function, estimated move duration (s)
        """
        # check in range and determine position delta
        delta = self.position_delta(position)
        if delta == 0:  # if there is no change, return an empty list
            return []

        if v is not None and dt is None:
            self.encoder_v = v  # check/set velocity
        if a is not None:
            self.encoder_a = a  # check/set acceleration
        self.position = position  # change position tracker

        # retrieve default if not specified
        if v is None and dt is None:  # allows passing of dt instead of v (works for some specific biases)
            v = self.encoder_v
        if a is None:
            a = self.encoder_a
        if bias is None:
            bias = self._defaulttraj

        # set direction flag based on position
        if delta >= 0.:
            dirflag = self._dirflag
        else:
            dirflag = 1 - self._dirflag

        return [  # return mopy list
            dirflag,  # insert direction flag
            *trajectory(  # determine trajectory
                abs(delta),
                v=v,
                a=a,
                dt=dt,
                bias=bias
            ),
        ]


class RevoluteJoint(Actuator):
    def __init__(self,
                 zero=0,
                 positive_rotation='cw',
                 axis_symmetry=None,
                 locked_symmetry=1,
                 **kwargs
                 ):
        """
        Defines a revolute joint (where a full rotation returns the axis to its original position)

        :param int zero: the counts at which the axis is zeroed (angles are calculated relative to this value)
        :param positive_rotation: positive count changes result in 'cw' or 'ccw' movement
        :param int, None axis_symmetry: if the axis has symmetry (e.g. the robot gripper which a half rotation is the
            same as a full rotation). If there is no symmetry (e.g. for a pump, specify None).
        :param int locked_symmetry: The symmetry of the axis to use if the axis symmetry is locked
        """
        # define the rotation sign for rotation calculation
        self._rot = 1 if positive_rotation.lower() == 'cw' else -1
        self._unlocked_symmetry = axis_symmetry
        self._locked_symmetry = locked_symmetry
        self.lock_symmetry = False  # toggle for locking the symmetry of the axis
        self.temp_lock = False  # temporary lock for the axis symmetry (used primarily by RobotArm)
        self.zero = zero
        self.radians = None
        self.degrees = None
        self.relative_pos = None
        Actuator.__init__(self, **kwargs)  # initialize Actuator

    def __repr__(self):
        return f'{self.__class__.__name__}({self.degrees:.1f}\u00b0, {self.position})'

    def __str__(self):
        return f'Revolute joint at {self.degrees:.1f}\u00b0, {self.position} counts'

    @property
    def axis_symmetry(self):
        """The symmetry of the axis. This value is dependent on the lock_symmetry attribute"""
        if self._unlocked_symmetry is None:  # unsymmetric axis
            return None
        if self.lock_symmetry is True or self.temp_lock is True:  # if lock is enabled, return locked symmetry
            return self._locked_symmetry
        return self._unlocked_symmetry  # otherwise return symmetry

    def angular_to_encoder_veloctiy(self, angvel):
        """
        Converts angular velocity to encoder velocity.

        :param float angvel: angular velocity (degrees/s)
        :return: encoder velocity (counts/second)
        :rtype: int
        """
        return intround(
            np.radians(angvel)  # convert to radians
            / (2 * np.pi)  # convert to fraction of a revolution
            * self.cpr  # multiply by counts per revolution
        )

    def encoder_to_angular_velocity(self, encvel):
        """
        Converts encoder velocity to angular velocity.

        :param int encvel: encoder velocity (counts/second)
        :return: angular velocity (degrees/second)
        :rtype: float
        """
        return np.degrees(
            float(encvel)  # float the value
            / self.cpr  # convert to revolutions
            * (2 * np.pi)  # convert to radians
        )

    def position_delta(self, position):
        """
        returns the difference between the current joint position and the specified position while considering
        allowances for full rotation

        :param position: position (counts)
        :return: difference in counts to the nearest option that satisfies the position argument
        :rtype: int
        """
        if type(position) != int:
            position = intround(position)

        # if there full rotation allowed, the nearest position may be a full-rotation option
        if self.axis_range is None and self.axis_symmetry is not None:
            return intround(min(
                [
                    position - self.cpr / self.axis_symmetry,  # target minus effective full rotation
                    position,  # target
                    position + self.cpr / self.axis_symmetry,  # target plus effective full rotation
                ],
                key=lambda x: abs(x - self.position),  # look for the closest position
            ) - self.position)

        # otherwise use standard position offset
        else:
            return position - self.position

    def angular_position_delta(self, position):
        """
        Returns the difference between the current position and the specified position in degrees.

        :param float position: position (degrees)
        :return: position delta (degrees)
        :rtype: float
        """
        return position - self.degrees

    def counts_to_degrees(self, counts):
        """converts counts to degrees relative to the zero position"""
        return np.degrees(self.counts_to_radians(counts))

    def counts_to_radians(self, counts):
        """converts counts to radians relative to the zero position"""
        counts = counts - self.zero
        return (
            (-1 if counts < 0 else 1)  # extract sign to avoid weird behaviour of operator with negative locations
            * (abs(counts) % self.cpr)  # find position relative to a full rotation
            * self._rot  # multiply by rotation sign
            / self.cpr  # convert to fraction of a circle
            * 2. * np.pi  # convert to radians
        )

    def degrees_to_counts(self, degrees):
        """converts degrees to counts relative to the zero position"""
        return self.radians_to_counts(np.radians(degrees))

    def radians_to_counts(self, radians):
        """converts radians to counts"""
        return intround(
            radians
            * self._rot
            / 2 / np.pi  # fraction of a complete circle
            * self.cpr  # convert to counts
        ) + self.zero  # relative to zero position

    def radians_to_counts_closest(self, radians):
        """converts radians to counts nearest to the current position"""
        return self.position_delta(  # calculate delta
                self.radians_to_counts(radians)  # calculate counts required to move to that location
                + self.cpr * (self.position // self.cpr)  # add current revolution offset
        ) + self.position  # plus current position

    def degrees_to_counts_closest(self, degrees):
        """converts degrees to counts nearest to the current position"""
        return self.radians_to_counts_closest(np.radians(degrees))

    def goto_degrees(self, degrees, **mopykwargs):
        """
        Determines the mopy set for going to the specified angle in radians relative to zero

        :param float degrees: angle in radians
        :param mopykwargs:  keyword arguments for Actuator.change_position
        :return: mopy set, estimated completion time for motion
        """
        return self.change_position(
            self.degrees_to_counts(degrees),
            **mopykwargs
        )

    def goto_counts(self, counts, **mopykwargs):
        """
        Determines the mopy set for going to the specified counts

        :param counts: target counts
        :param mopykwargs:  keyword arguments for Actuator.change_position
        :return: mopy set, estimated completion time for motion
        """
        warnings.warn(
            'The goto_counts method has been depreciated. Use change_position instead',
            DeprecationWarning
        )
        return self.change_position(
            counts,
            **mopykwargs
        )

    def change_position_locked_symmetry(self, position, v=None, a=None, dt=None, bias=None):
        """
        Locks the axis symmetry, then changes the position tracker of the actuator and generates a MOPY command
        trajectory for the actuator.
        If the keywords are not specified, the class instance defaults will be used.

        :param position: position to move to
        :param v: velocity (counts/second)
        :param a: acceleration (counts/second^2)
        :param dt: delta time (for making a trajectory last a specified amount of time)
        :param bias: trajectory profile
        The velocity and acceleration will be scaled proportionally to relation of the change in counts divided by
        the scalar.

        :return: appropriately formated list of variables to hand the MOPY function, estimated move duration (s)
        """
        self.temp_lock = True  # temporarily lock the symmetry of the axis
        out = self.change_position(  # generate the MOPY trajectory
            position,
            v=v,
            a=a,
            dt=dt,
            bias=bias,
        )
        self.temp_lock = False  # unlock temporary lock
        return out

    def goto_counts_relative(self, counts, **mopykwargs):
        """
        Determines the mopy set moving the axis by the relative amount specified. This function assumes that the
        relative position has already been determined.

        :param counts: target counts
        :param mopykwargs:  keyword arguments for Actuator.change_position
        :return: mopy set, estimated completion time for motion
        """
        warnings.warn(
            'The goto_counts_relative method has been depreciated. Use change_position_relative instead.',
            DeprecationWarning
        )
        return self.change_position_relative(counts, **mopykwargs)

    def change_position_relative(self, counts, **mopykwargs):
        """
        Determines the mopy set moving the axis by the relative amount specified. This function assumes that the
        relative position has already been determined.

        :param counts: target counts
        :param mopykwargs:  keyword arguments for Actuator.change_position
        :return: mopy set, estimated completion time for motion
        """
        return self.change_position(
            self.position + counts,
            **mopykwargs
        )

    def goto_radians(self, radians, **mopykwargs):
        """
        Determines the mopy set for going to the specified angle in radians relative to zero

        :param radians: angle in radians
        :param mopykwargs:  keyword arguments for Actuator.change_position
        :return: mopy set, estimated completion time for motion
        """
        return self.change_position(
            self.radians_to_counts(radians),
            **mopykwargs,
        )

    def spin(self, t=60, rpm=750, direction=1):
        """
        Spins the axis for the specified amount of time

        :param t: time to spin for (s)
        :param rpm: revolutions per minute
        :param direction: direction to spin (1 or -1)
        :return: mopy set, estimated completion time for motion
        """
        v = intround(rpm * self.cpr / 60.)
        target = self.position + direction * intround(t * v)
        mopy = self.change_position(
            target,  # delta
            v=v,  # velocity
        )
        return mopy

    @property
    def position(self):
        return self._pos

    @position.setter
    def position(self, position):
        self._pos = position
        self.relative_pos = self.position - self.zero
        self.radians = self.counts_to_radians(self.position)
        self.degrees = np.degrees(self.radians)

    def home(self):
        """sets the joint to the homed state"""
        self.position = int(self._initial)


class PrismaticJoint(Actuator):
    def __init__(self,
                 zero=0,
                 screwpitch=None,
                 cpmm=None,
                 distsign=1,
                 **kwargs,
                 ):
        """
        Defines a prismatic joint (where the joint drives a slider). Requires one of screwpitch or cpmm
        to be specified.

        :key zero: zero value for the axis (locations are calculated relative to this value)
        :key screwpitch: pitch of the screw installed in the prismatic joint
        :key cpmm: counts per mm
        :key distsign: sign for whether distance increases (1) or decreases (-1) with increasing counts
        """
        # relate counts to distance
        if cpmm is not None:
            self.cpmm = cpmm
        elif screwpitch is not None:
            self.cpmm = self.cpr / screwpitch
        else:
            raise ValueError('A value must be specified for either cpmm or screwpitch for a prismatic joint')
        self.zero = zero

        self.location = None
        self.distsign = distsign
        Actuator.__init__(self, **kwargs)  # initialize actuator
        self._linear_v = self.encoder_to_linear_velocity(self.encoder_v)

    def __repr__(self):
        return "%s(%g mm, %d counts)" % (
            self.__class__.__name__,
            self.location,
            self.position,
        )

    def __str__(self):
        return "Prismatic joint at %g mm (%d counts)" % (
            self.location,
            self.position,
        )

    def encoder_to_linear_velocity(self, encv):
        """
        Converts encoder velocity to linear velocity.

        :param encv: encoder velocity (counts/second)
        :return: linear velocity (mm/second)
        :rtype: float
        """
        return float(encv / self.cpmm)

    def linear_to_encoder_velocity(self, linv):
        """
        Converts linear velocity to encoder velocity.

        :param linv: linear velocity (mm/second)
        :return: encoder velocity (counts/second)
        :rtype: int
        """
        return int(linv * self.cpmm)

    def counts_to_mm(self, counts):
        """converts counts to distance relative to zero in mm"""
        return (
                       self.distsign * counts + self.zero
        ) / self.cpmm

    def mm_to_counts_relative(self, mm):
        """converts location in mm relative to zero to counts"""
        return intround(
            mm * self.cpmm
            - self.zero
        ) * self.distsign

    def mm_to_counts_absolute(self, mm):
        """converts absolute mm to counts"""
        return intround(
            mm * self.cpmm
        ) * self.distsign

    def goto_counts(self, counts, **mopykwargs):
        """
        Determines the mopy set for going to the specified counts

        :param counts: target counts
        :param mopykwargs:  keyword arguments for Actuator.change_position
        :return: mopy set, estimated completion time for motion
        """
        warnings.warn(
            'The goto_counts method has been depreciated. Use change_position instead. ',
            DeprecationWarning
        )
        return self.change_position(
            counts,
            **mopykwargs
        )

    def goto_position(self, mm, **mopykwargs):
        """
        Determines the mopy set for going to the specified distance in mm relative to zero

        :param mm: distance in mm
        :param mopykwargs:  keyword arguments for Actuator.change_position
        :return: mopy set, estimated completion time for motion
        """
        return self.change_position(
            self.mm_to_counts_relative(mm),
            **mopykwargs,
        )

    @property
    def position(self):
        return self._pos

    @position.setter
    def position(self, position):
        self._pos = position
        self.location = self.counts_to_mm(self.position)

    def home(self):
        """sets the joint to the homed state"""
        self.position = int(self._initial)
