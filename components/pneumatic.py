"""
Classes for defining pneumatically controlled components
"""

from ..components.basic import OutputComponent


class PneumaticHolder(OutputComponent):
    def __init__(self, positions=None, **kwargs):
        """
        Defines a pneumatic (pressure driven) gripper, such as the one on the end of the robot arm.

        :param positions: dictionary of positions other than default
        :param kwargs: OutputComponent kwargs
        """
        if positions is None:
            positions = {
                'on': 1,
                'off': 0,
            }

        OutputComponent.__init__(
            self,
            positions=positions,
            **kwargs,
        )

        self.state = 0  # initial state of the holder if off
        self.ingrip = None  # holder for instances that are in the gripper

    def __repr__(self):
        return f'{self.__class__.__name__}({self.output}, {self.state})'

    def __str__(self):
        return f'{self.__class__.__name__} on output {self.output} ({"ON" if self.state == 1 else "OFF"})'

    def on(self, item=None, delay=None):
        """
        Turns the holder on and places the provided item into the ingrip class attribute

        :param item: delay time after cycling the output
        :param delay: delay time after cycling the output
        """
        self.set_state('on', delay)
        self.ingrip = item  # store in attribute

    def off(self, delay=None):
        """
        Turns the pneumatic gripper off

        :param delay: delay time after cycling the output
        :return: item previously contained in the gripper
        """
        self.set_state('off', delay)
        out = self.ingrip
        self.ingrip = None
        return out


