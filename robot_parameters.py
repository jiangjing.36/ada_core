"""
A file for storing robot parameters. Each set of parameters will be unique to a given robot arm. The keyword arguments
are associated with the RobotArm class.
"""

# kwargs for Toothless
arm_parameters = {
    'shoulder': {
        'cpr': 101000,
        'axis_range': [0, 66400],
        'max_v': 65000,
        'max_a': 500000,
        'zero': 32898,
        'axis_symmetry': 1,
        'positive_rotation': 'CCW',
    },
    'elbow': {
        'cpr': 51000,
        # 'axis_range': [0, 42100],  # with no side-mounted components installed
        'axis_range': [0, 38000],  # with left-side mounted valve
        'max_v': 65000,
        'max_a': 500000,
        'zero': 20734,
        'axis_symmetry': 1,
    },
    'gripper': {
        'cpr': 4000,  # counts per revolution
        # 'zero': -150,  # aligned with elbow axis
        'initial': 150,
        'max_rpm': 750,
        'axis_symmetry': 2,
    },
    'z': {
        'cpr': 1000,
        'axis_range': [-800, 24500],
        'max_rpm': 6000,
        # 'dirflag': 0,
        'cpmm': 100,  # counts per mm is the most relevant value here
        # 'zero': 28480,  # the value where the gripper would touch the bed (calculated and outside of range)
        'zero': 30480,  # the value where the gripper would touch the bed (calculated and outside of range)
        'distsign': -1,
    },
    'kwargs': {
        'shax': 2,  # shoulder axis number
        'elax': 1,  # elbow axis number
        'grax': 0,  # gripper axis number
        'grop': 0,  # gripper output number (for engaging the gripper)
        'vertax': 3,  # vertical axis number
        'velocity': 45000,  # default velocity
        'acceleration': 150000,  # default acceleration
        'x_offset': -0.7,
        'y_offset': 0.6,
        'modules': [
            {  # needle generation 3
                'el_point': 211.75,
                'voffset': 14.,
                'length': 33.,
                'attachment_length': 11.5,
                'top_width': 4.35,
                'bot_width': 3.90,
                'name': 'probe',
                'module_type': 'DispenserProbe',
            },
            {  # Toothless 2 mL HPLC vial gripper
                'el_point': 170.25,
                'length': 20.,
                'name': 'gripper',
                'module_type': 'ArmModule',
            },
        ]
    }
}

# kwargs for Zippleback
# arm_parameters = {
#     'shoulder': {
#         'cpr': 101000,
#         'axis_range': [0, 66400],  # todo
#         'max_v': 65000,
#         'max_a': 500000,
#         'zero': 32910,
#         'axis_symmetry': 1,
#         'positive_rotation': 'CCW',
#     },
#     'elbow': {
#         'cpr': 51000,
#         'axis_range': [0, 42100],  # todo
#         'max_v': 65000,
#         'max_a': 500000,
#         'zero': 20856,
#         'axis_symmetry': 1,
#     },
#     'gripper': {
#         'cpr': 4000,  # counts per revolution
#         # 'zero': -150,  # aligned with elbow axis
#         'initial': 150,  # todo
#         'max_rpm': 750,
#         'axis_symmetry': 2,
#     },
#     'z': {
#         'cpr': 1000,
#         'axis_range': [-800, 24500],  # todo
#         'max_rpm': 6000,
#         # 'dirflag': 0,
#         'cpmm': 100,  # counts per mm is the most relevant value here
#         'zero': 30922,  # the value where the gripper would touch the bed (calculated and outside of range)
#         'distsign': -1,
#     },
#     'kwargs': {
#         'shax': 2,  # shoulder axis number
#         'elax': 1,  # elbow axis number
#         'grax': 0,  # gripper axis number
#         'grop': 0,  # gripper output number (for engaging the gripper)
#         'vertax': 3,  # vertical axis number
#         'velocity': 50000,  # default velocity
#         'acceleration': 150000,  # default acceleration
#         'x_offset': 0.3,
#         'y_offset': 0.197,
#         'modules': [
#             zippleback_2mL_gripper,
#             # todo pipette tip attachment
#         ]
#     }
# }
