ada\_core.sequences package
===========================

Submodules
----------

ada\_core.sequences.depreciated\_sequences module
-------------------------------------------------

.. automodule:: ada_core.sequences.depreciated_sequences
    :members:
    :undoc-members:
    :show-inheritance:

ada\_core.sequences.sampling module
-----------------------------------

.. automodule:: ada_core.sequences.sampling
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: ada_core.sequences
    :members:
    :undoc-members:
    :show-inheritance:
