ada\_core.profiles package
==========================

Submodules
----------

ada\_core.profiles.arm\_modules module
--------------------------------------

.. automodule:: ada_core.profiles.arm_modules
    :members:
    :undoc-members:
    :show-inheritance:

ada\_core.profiles.com\_components module
-----------------------------------------

.. automodule:: ada_core.profiles.com_components
    :members:
    :undoc-members:
    :show-inheritance:

ada\_core.profiles.dispensers module
------------------------------------

.. automodule:: ada_core.profiles.dispensers
    :members:
    :undoc-members:
    :show-inheritance:

ada\_core.profiles.grippers module
----------------------------------

.. automodule:: ada_core.profiles.grippers
    :members:
    :undoc-members:
    :show-inheritance:

ada\_core.profiles.needles module
---------------------------------

.. automodule:: ada_core.profiles.needles
    :members:
    :undoc-members:
    :show-inheritance:

ada\_core.profiles.outputs module
---------------------------------

.. automodule:: ada_core.profiles.outputs
    :members:
    :undoc-members:
    :show-inheritance:

ada\_core.profiles.pipette\_tips module
---------------------------------------

.. automodule:: ada_core.profiles.pipette_tips
    :members:
    :undoc-members:
    :show-inheritance:

ada\_core.profiles.pumps module
-------------------------------

.. automodule:: ada_core.profiles.pumps
    :members:
    :undoc-members:
    :show-inheritance:

ada\_core.profiles.syringes module
----------------------------------

.. automodule:: ada_core.profiles.syringes
    :members:
    :undoc-members:
    :show-inheritance:

ada\_core.profiles.tip\_removers module
---------------------------------------

.. automodule:: ada_core.profiles.tip_removers
    :members:
    :undoc-members:
    :show-inheritance:

ada\_core.profiles.trays module
-------------------------------

.. automodule:: ada_core.profiles.trays
    :members:
    :undoc-members:
    :show-inheritance:

ada\_core.profiles.ttl\_components module
-----------------------------------------

.. automodule:: ada_core.profiles.ttl_components
    :members:
    :undoc-members:
    :show-inheritance:

ada\_core.profiles.tubing module
--------------------------------

.. automodule:: ada_core.profiles.tubing
    :members:
    :undoc-members:
    :show-inheritance:

ada\_core.profiles.vials module
-------------------------------

.. automodule:: ada_core.profiles.vials
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: ada_core.profiles
    :members:
    :undoc-members:
    :show-inheritance:
