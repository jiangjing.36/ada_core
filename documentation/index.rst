.. ada documentation master file, created by
   sphinx-quickstart on Fri Jun 29 14:33:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Ada's documentation!
===============================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   intro
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
