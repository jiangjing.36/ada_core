ada\_core.GUI package
=====================

Submodules
----------

ada\_core.GUI.auto\_gui module
------------------------------

.. automodule:: ada_core.GUI.auto_gui
    :members:
    :undoc-members:
    :show-inheritance:

ada\_core.GUI.direct\_inject module
-----------------------------------

.. automodule:: ada_core.GUI.direct_inject
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: ada_core.GUI
    :members:
    :undoc-members:
    :show-inheritance:
